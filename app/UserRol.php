<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserRol extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $primaryKey = 'IDAlmacen';
    protected $table = 'user_rol';
    protected $fillable = [


    ];


   /** public function administradores(){
        return $this->belongsTo('App\Administradores','administrador_id');
    }**/
}
