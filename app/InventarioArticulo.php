<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class InventarioArticulo extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'inventario_articulo';
    protected $primaryKey = 'IDinventario_articulo';
    protected $fillable = [


    ];

    public function almacenes(){
        return $this->belongsTo('App\Almacen','IDalmacen');
    }

    public function inventarios(){
        return $this->belongsTo('App\Inventario','IDinventarios');
    }

    public function productos(){
        return $this->belongsTo('App\Producto','IDarticulo');
    }

    }
