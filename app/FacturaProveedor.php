<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class FacturaProveedor extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey ='IDfacturaproveedor';
    protected $table = 'facturaproveedor';
    protected $fillable = [


    ];

    public function almacenes(){
        return $this->belongsTo('App\Almacen','IDalmacen', 'IDalmacen');
    }


    public function proveedores(){
        return $this->belongsTo('App\Provider','IDproveedores', 'RUC');
    }

    }
