<?php

namespace App\Http\Controllers;

use App\CategoriaProducto;
use Illuminate\Http\Request;
use App\Movimiento;
use Carbon\Carbon;
use App\AccesoRol;
use Auth;

class KardexController extends Controller
{
    public function __construct(){
    	Carbon::setLocale('es');
        $this->middleware('auth');
    }

    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
    	$kardex = Movimiento::where('IDpref', Auth::User()->IDpref)->where('IDalmacen', Auth::User()->IDalmacen)->orderBy('created_at','DESC')
    			->get();
    	return view('layouts.listar.index',compact('rol','kardex','categorias'))
    				->with('kardex', $kardex);
    }
}
