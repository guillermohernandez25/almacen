<?php

namespace App\Http\Controllers;

use App\CategoriaProducto;
use Illuminate\Http\Request;
use App\Http\Requests\KardexoutRequest;
use Illuminate\Support\Facades\Auth;
use App\Movimiento;
use App\Producto;
use Carbon\Carbon;
use DB;
use Session;
use Toastr;
use App\AccesoRol;

class RegSalidaController extends Controller
{
	private $stock = 0;

    public function __construct(){
    	Carbon::setLocale('es');
    	$stock = 0;
    }

    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
	    	$articulo = Producto::where('estatus','!=','4')
                ->orderBy('Cod','DESC')
                ->select(DB::raw('CONCAT(Articulo," - ",Cod) AS Articulo'),'IDarticulo')
                ->pluck('Articulo','IDarticulo');
			return view('layouts.salida.create',compact('articulo','rol'));



    }

    public function store(KardexoutRequest $request){

    			if($this->validarStock($request->input('mov_salida'),$request->input('IDarticulo'))){
    			    $articulo = Producto::where('IDarticulo',$request->input('IDarticulo'))->first();
                    //$empresa_almacen  = Producto::where('IDarticulo',$request->input('IDarticulo'))->first();
					$salida = new Movimiento();
                    $salida->IDarticulo = $request->input('IDarticulo');
                    $salida->IDcategoria = $articulo->IDcategoria;
                    $salida->IDpref = Auth::User()->IDpref;
                    $salida->IDalmacen = Auth::User()->IDalmacen;
                    $salida->IDclientes = 1;
                    $salida->Mov_factura = 0;
                    $salida->Mov_entrada = 0;
                    $salida->Mov_salida = $request->input('mov_salida');
                    $salida->Mov_total = $this->stock;
                    $salida->IDregistra = Auth::user()->IDusuario;
                    $salida->IDactualiza = Auth::user()->IDusuario;
                    $salida->Mov_estado = 1;
                    $salida->Mov_obs = "";
                    $salida->save();
					$message=$salida ? 'Se ha adicionado a kardex la salida del articulo' : 'Registro Correcto';
				}else{
					$message ='El Stock del articulo no esta disponible, restante: '. $this->stock;
				}

    		return redirect()->route('kardexout.index', compact('message'));

    }
    
    public function validarStock($cantidad, $id){
		try{
			$mov = Movimiento::where('IDarticulo','=',$id)
							->orderBy('created_at','DESC')
							->first();
			if(!is_null($mov)){
				if($mov->Mov_total >= $cantidad){
					$this->stock = $mov->Mov_total - $cantidad;
					return true;
				}else{
					$this->stock = $mov->Mov_total;
					return false;
				}
			}
			else{
				return false;
			}
		}catch(\Exception $ex){
			Toastr::error('Ocurrio el siguiente error: '.$ex->getMessage(),'Error');
		}
	}
}
