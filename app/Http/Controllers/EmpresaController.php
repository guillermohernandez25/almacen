<?php

namespace App\Http\Controllers;

use App\Administradores;
use App\Empresa;
use App\Inventario;
use Illuminate\Http\Request;
use App\Almacen;
use App\Venta;
use App\Color;
use Auth;
use App\AccesoRol;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class EmpresaController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
 
    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(1,json_decode($rol),false)) {
            if(Auth::User()->rol_id != "1") {
                $color = Color::where('user_id', Auth::User()->id)->first();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
                return view('layouts.empresa.index', compact( 'empresa', 'color', 'rol'));
            }else{
                $color = Color::where('user_id', Auth::User()->id)->first();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $empresa = Empresa::where('estatus','!=','4')->get();
                return view('layouts.empresa.index', compact( 'empresa', 'color', 'rol'));
            }
        }else{
            return redirect()->route('home');
        }

    }




    public function create(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(1,json_decode($rol),false)) {
            $color = Color::where('user_id', Auth::User()->id)->first();
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');

            return view('layouts.empresa.create', compact('color',  'rol'));
            //->with('clientes', $clientes);
        }else{
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $existe_empresa = Empresa::where('RUC', '=', $request->RUC)->first();
        if (empty($existe_empresa)) {
            DB::enableQueryLog();
                $empresa = new Empresa();
                $empresa->NombreEstablecim = $request->NombreEstablecim;
                $empresa->Ciudad = $request->Ciudad;
                $empresa->Direcc = $request->Direcc;
                $empresa->RUC = $request->RUC;
                $empresa->Telf1 = $request->Telf1;
                $empresa->Telf2 = $request->Telf2;
                $empresa->save();
                //return view('welcome');
                $message = $empresa ? 'Se ha registrado la empresa' . $request->NombreEstablecim. 'de forma exitosa.' : 'Error al Registrar';
            $nombre  = 'SQLinstructions.txt';
            $cont = 0;
            $query = DB::getQueryLog();
            //dd($query);
            for ($j = 0; $j < sizeof($query); $j++) {
                $items = explode('?', $query[$j]['query']);
                //dd(sizeof($items));
                $querycompleta = '';
                for ($i = 1; $i < sizeof($items); $i++) {
                    $querycompleta = $querycompleta . $query['0']['bindings'][$i - 1] . $items[$i];
                }
                $file = Storage::append($nombre, $items['0'].$querycompleta.";\n");
            }


                //Session::flash("message', 'Te has registrado exitosamente ');
                return redirect()->route('empresa')->with('message', $message);
        } else {
            $message2 = 'Esta empresa ya se encuentra registrada';
            return redirect()->route('empresa')->with('message2', $message2);
        }
    }


    public function edit($id){
        $empresa = Empresa::where('IDpref','=', $id)->first();
        $color = Color::where('user_id', Auth::User()->id)->first();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        return view('layouts.empresa.edit',compact('empresa','rol','color'));
    }

    public function update(Request $request, $id){
        DB::enableQueryLog();
        $empresa= Empresa::where('IDpref','=', $id)->first();
        $empresa->NombreEstablecim = $request->NombreEstablecim;
        $empresa->Ciudad = $request->Ciudad;
        $empresa->Direcc = $request->Direcc;
        $empresa->RUC = $request->RUC;
        $empresa->Telf1 = $request->Telf1;
        $empresa->Telf2 = $request->Telf2;
        $empresa->save();
        $message = $empresa?'Se ha actualizado el registro'. $request->NombreEstablecim.'de forma exitosa.' : 'Error al actualizar';
        $nombre  = 'SQLinstructions.txt';

        $query = DB::getQueryLog();
        //dd($query);
        for ($j = 0; $j < sizeof($query); $j++) {
            $items = explode('?', $query[$j]['query']);
            //dd(sizeof($items));
            $querycompleta = '';
            for ($i = 1; $i < sizeof($items); $i++) {
                $querycompleta = $querycompleta . $query['0']['bindings'][$i - 1] . $items[$i];
            }
            $file = Storage::append($nombre, $items['0'].$querycompleta.";\n");
        }

        return redirect()->route('empresa')->with('message', $message);

    }


    public function destroy($id){
        DB::enableQueryLog();
        $empresa = Empresa::find($id);
        $empresa->delete($id);
        $message =  $empresa?'Registro eliminado correctamente' : 'Error al Eliminar';
        $nombre  = 'SQLinstructions.txt';
        $cont = 0;
        $query = DB::getQueryLog();
        //dd($query);
        for ($j = 0; $j < sizeof($query); $j++) {
            $items = explode('?', $query[$j]['query']);
            //dd(sizeof($items));
            $querycompleta = '';
            for ($i = 1; $i < sizeof($items); $i++) {
                $querycompleta = $querycompleta . $query['0']['bindings'][$i - 1] . $items[$i];
            }
            $file = Storage::append($nombre, $items['0'].$querycompleta.";\n");
        }


            return redirect()->route('empresa')->with('message', $message);

    }
}
