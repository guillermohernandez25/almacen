<?php

namespace App\Http\Controllers;

use App\Administradores;
use App\Empresa;
use App\Inventario;
use Illuminate\Http\Request;
use App\Almacen;
use App\Venta;
use App\Color;
use Auth;
use App\AccesoRol;
class AlmacenController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(1,json_decode($rol),false)) {
            if(Auth::User()->rol_id != "1") {
                $color = Color::where('user_id', Auth::User()->id)->first();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                $administradores = Administradores::where('estatus','!=','4')->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                return view('layouts.almacen.index', compact('administradores', 'almacen', 'color', 'rol'));
            }else{
                $color = Color::where('user_id', Auth::User()->id)->first();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $almacen = Almacen::where('estatus','!=','4')->get();
                $administradores = Administradores::where('estatus','!=','4')->get();
                return view('layouts.almacen.index', compact('administradores', 'almacen', 'color', 'rol'));
            }
        }else{
            return redirect()->route('home');
        }

    }


    public function almacen($id){
        $color = Color::where('user_id', Auth::User()->id)->first();
        $almacen = Almacen::where('IDalmacen',$id)->first();
        $inventarios = Inventario::where('IDalmacen',$id)->get();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        return view('layouts.almacen.inventario', compact('almacen','administradores','inventarios','color','rol'));

    }

    public function ventas(Request $request, $id = 1){
        //dd($request->fecha_inicial);
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(10,json_decode($rol),false)) {
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            $color = Color::where('user_id', Auth::User()->id)->first();

            $ventas = Venta::groupBy('Factura_Id')->where('IDalmacen',$id)->get();
            if(empty($request->fecha_inicial) && empty($request->fecha_final)) {
                $request->fecha_inicial = date('Y-m-d');
                $request->fecha_final = date('Y-m-d');
                $desde = $request->fecha_inicial = date('Y-m-d');
                $hasta = $request->fecha_final = date('Y-m-d');
                //dd($fecha_inicial);
                $ventas_total = Venta::groupBy('Factura_Id')->where('IDalmacen',$id)->whereBetween('Fecha', [$request->fecha_inicial, $request->fecha_final])->get();

                if(sizeof($ventas_total) >= 1){
                    for($i =0; $i < sizeof($ventas_total); $i++) {
                        $valor = 0;
                        $busqueda_items = Venta::where('Factura_Id', $ventas_total[$i]->Factura_Id)->where('IDalmacen',$id)->get();
                        for($j = 0 ; $j < sizeof($busqueda_items); $j++){
                            $valor = $valor + $busqueda_items[$j]->Valor_Total;
                        }

                        $ventas_suma[$i] = $valor;
                    }
                    //dd($ventas_total);
                    $ventas_final = 0;
                    for($k = 0 ; $k < sizeof($ventas_suma); $k++){
                        $ventas_final = $ventas_final + $ventas_suma[$k];
                    }
                    //dd($ventas_final,$ventas_total[0]->Factura_Id,$busqueda_items,$ventas_total, $ventas_suma);
                }
            }else{
                $desde = $request->fecha_inicial ;
                $hasta = $request->fecha_final ;
                $ventas_total = Venta::groupBy('Factura_Id')->where('IDalmacen',$id)->whereBetween('Fecha', [$request->fecha_inicial, $request->fecha_final])->get();
                //dd( $ventas_total, $request->fecha_inicial, $request->fecha_final );
                if(sizeof($ventas_total) >= 1){
                    for($i =0; $i < sizeof($ventas_total); $i++) {
                        $valor = 0;
                        $busqueda_items = Venta::where('Factura_Id', $ventas_total[$i]->Factura_Id)->where('IDalmacen',$id)->get();
                        for($j = 0 ; $j < sizeof($busqueda_items); $j++){
                            $valor = $valor + $busqueda_items[$j]->Valor_Total;
                        }

                        $ventas_suma[$i] = $valor;
                    }
                    //dd($ventas_total);
                    $ventas_final = 0;
                    for($k = 0 ; $k < sizeof($ventas_suma); $k++){
                        $ventas_final = $ventas_final + $ventas_suma[$k];
                    }
                    //dd($ventas_final,$ventas_total[0]->Factura_Id,$busqueda_items,$ventas_total, $ventas_suma);
                }

            }

            return view('layouts.ventas.index', compact('id','desde','hasta','ventas_total','ventas_suma','ventas_final','rol', 'ventas', 'color'));
        }else{
            return redirect()->route('home');
        }


    }

    public function administradores($id){

        $administradores = Administradores::where('IDadministradores',$id)->first();
        return $administradores;
        //->with('clientes', $clientes);
    }

    public function create(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(1,json_decode($rol),false)) {
            $color = Color::where('user_id', Auth::User()->id)->first();
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            $administradores = Administradores::all();
            if(Auth::User()->rol_id != "1") {
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->get();
            }else{
                $almacen = Almacen::where('estatus','!=','4')->get();
                $empresa = Empresa::all();
            }

            return view('layouts.almacen.create', compact('empresa','color', 'administradores', 'rol'));
            //->with('clientes', $clientes);
        }else{
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $existe_nombre = Almacen::where('nombre', '=', $request->nombre)->first();
        if (empty($existe_nombre)) {


                $almacen = new Almacen();
                $almacen->nombre = $request->nombre;
            $almacen->estatus = "2";
                $almacen->direccion = $request->direccion;
                $almacen->telefono = $request->telefono;
                $almacen->descripcion = $request->descripcion;
                $almacen->IDpref = $request->IDpref;
                $almacen->IDadministradores = $request->IDadministradores;
                $almacen->save();
                //return view('welcome');
                $message = $almacen ? 'Se ha registrado el almacen' . $request->nombre. 'de forma exitosa.' : 'Error al Registrar';
                $administradores = Administradores::where('IDadministradores',$request->IDadministradores)->first();
                $administradores->estatus = "3";
                $administradores->IDalmacen = $almacen->IDalmacen;
                $administradores->IDpref = $request->IDpref;
                $administradores->save();


                //Session::flash('message', 'Te has registrado exitosamente ');
                return redirect()->route('almacenes')->with('message', $message);
        } else {
            $message2 = 'Este almacen ya se encuentra registrado';
            return redirect()->route('almacenes')->with('message2', $message2);
        }
    }


    public function edit($id){
        $almacen = Almacen::where('IDalmacen','=', $id)->first();
        $administrador = Administradores::where('IDadministradores',$almacen->IDadministradores)->first();
        $administradores = Administradores::all();
        $color = Color::where('user_id', Auth::User()->id)->first();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        $empresa = Empresa::all();
        return view('layouts.almacen.edit',compact('empresa','rol','administrador','almacen','color','administradores'));
    }
    public function update(Request $request, $id){

        $almacen= Almacen::where('IDalmacen','=', $id)->first();
        $almacen->nombre = $request->nombre;
        $almacen->estatus = "2";
        $almacen->direccion = $request->direccion;
        $almacen->telefono = $request->telefono;
        $almacen->descripcion = $request->descripcion;
        $almacen->IDpref = $request->IDpref;
        $almacen->IDadministradores = $request->IDadministradores;
        $almacen->save();

        $administradores = Administradores::where('IDadministradores',$request->IDadministradores)->first();
        $administradores->estatus = "3";
        $administradores->IDalmacen = $almacen->IDalmacen;
        $administradores->IDpref = $request->IDpref;
        $administradores->save();

        $message = $almacen?'Se ha actualizado el registro'. $request->almacen.'de forma exitosa.' : 'Error al actualizar';
        return redirect()->route('almacenes')->with('message', $message);


    }


    public function destroy($id){

        $almacen = almacen::find($id);
        $almacen->estatus = "4";
        $almacen->save();
        $message =  $almacen?'Registro eliminado correctamente' : 'Error al Eliminar';
            return redirect()->route('almacenes')->with('message', $message);

    }
}
