<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Administradores;
use App\Color;
use App\AccesoRol;
use Auth;
class AdministradoresController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');

        if(in_array(12,json_decode($rol),false)){
            if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2"){
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $color = Color::where('user_id', Auth::User()->id)->first();

                $administradores = Administradores::where('estatus','!=','4')->where('IDalmacen',Auth::User()->IDalmacen)->get();
                return view('layouts.administradores.index', compact('rol', 'administradores', 'color'));
            }elseif(Auth::User()->rol_id == "2"){
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            $color = Color::where('user_id', Auth::User()->id)->first();

            $administradores = Administradores::where('estatus','!=','4')->where('IDpref', Auth::User()->IDpref)->get();

            return view('layouts.administradores.index', compact('rol', 'administradores', 'color'));
            }elseif(Auth::User()->rol_id == "1"){
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            $color = Color::where('user_id', Auth::User()->id)->first();

            $administradores = Administradores::where('estatus','!=','4')->get();

            return view('layouts.administradores.index', compact('rol', 'administradores', 'color'));
        }
        }else{
            return redirect()->route('home');
        }
    }
    public function create(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(13,json_decode($rol),false)) {
            $color = Color::where('user_id', Auth::User()->id)->first();
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');

            return view('layouts.administradores.create', compact('color', 'rol'));
            //->with('clientes', $clientes);
        }else{
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $existe_dni = Administradores::where('dni', '=', $request->dni)->first();
        if (empty($existe_dni)) {

            DB::enableQueryLog();
            $administradores = new administradores();

            $administradores->dni = $request->dni;
            $administradores->estatus = "2";
            $administradores->nombre = $request->nombre;
            $administradores->apellido = $request->apellido;
            $administradores->direccion = $request->direccion;
            $administradores->telefono = $request->telefono;

            $administradores->save();
            //return view('welcome');
            $message = $administradores ? 'Se ha registrado el administrador ' . $request->dni. ' de forma exitosa.' : 'Error al Registrar';
            $nombre  = 'SQLinstructions.txt';

            $query = DB::getQueryLog();
            //dd($query);
            for ($j = 0; $j < sizeof($query); $j++) {
                $items = explode('?', $query[$j]['query']);
                //dd(sizeof($items));
                $querycompleta = '';
                for ($i = 1; $i < sizeof($items); $i++) {
                    $querycompleta = $querycompleta . $query['0']['bindings'][$i - 1] . $items[$i];
                }
                $file = Storage::append($nombre, $items['0'].$querycompleta.";\n");
            }
            //Session::flash('message', 'Te has registrado exitosamente ');
            return redirect()->route('administradores')->with('message', $message);
        } else {
            $message2 = 'Este administrador ya se encuentra registrado';
            return redirect()->route('administradores')->with('message2', $message2);
        }
    }


    public function edit($id){
        $administradores = Administradores::where('IDadministradores','=', $id)->first();
        $color = Color::where('user_id', Auth::User()->id)->first();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        return view('layouts.administradores.edit',compact('administradores','color','rol'));
    }
    public function update(Request $request, $id){
        DB::enableQueryLog();
        $administradores= Administradores::where('IDadministradores','=', $id)->first();
        $administradores->dni = $request->dni;
        $administradores->estatus = "3";
        $administradores->nombre = $request->nombre;
        $administradores->apellido = $request->apellido;
        $administradores->direccion = $request->direccion;
        $administradores->telefono = $request->telefono;
        $administradores->save();
        $nombre  = 'SQLinstructions.txt';

        $query = DB::getQueryLog();
        //dd($query);
        for ($j = 0; $j < sizeof($query); $j++) {
            $items = explode('?', $query[$j]['query']);
            //dd(sizeof($items));
            $querycompleta = '';
            for ($i = 1; $i < sizeof($items); $i++) {
                $querycompleta = $querycompleta . $query['0']['bindings'][$i - 1] . $items[$i];
            }
            $file = Storage::append($nombre, $items['0'].$querycompleta.";\n");
        }
        $message = $administradores?'Se ha actualizado el registro'. $request->administradores.'de forma exitosa.' : 'Error al actualizar';
        return redirect()->route('administradores')->with('message', $message);


    }


    public function destroy($id){
        DB::enableQueryLog();
        $administradores = Administradores::find($id);
        //$administradores->estatus = "4";
        $administradores->delete();
        $nombre  = 'SQLinstructions.txt';

        $query = DB::getQueryLog();
        //dd($query);
        for ($j = 0; $j < sizeof($query); $j++) {
            $items = explode('?', $query[$j]['query']);
            //dd(sizeof($items));
            $querycompleta = '';
            for ($i = 1; $i < sizeof($items); $i++) {
                $querycompleta = $querycompleta . $query['0']['bindings'][$i - 1] . $items[$i];
            }
            $file = Storage::append($nombre, $items['0'].$querycompleta.";\n");
        }
        $message =  $administradores?'Registro eliminado correctamente' : 'Error al Eliminar';
        return redirect()->route('administradores')->with('message', $message);

    }
}
