<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FacturaProveedor;
use App\Color;
use Auth;
use App\AccesoRol;
class CompraController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(11,json_decode($rol),false)) {
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            $color = Color::where('user_id', Auth::User()->id)->first();
            $compras = FacturaProveedor::all();
            return view('layouts.compras.index', compact('compras', 'color', 'rol'));
        }else{
            return redirect()->route('home');
        }
    }
    public function create(){
        $color = Color::where('user_id', Auth::User()->id)->first();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        return view('layouts.compras.create', compact('color','rol'));
        //->with('clientes', $clientes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $existe_ruc = Compra::where('ruc', '=', $request->ruc)->first();
        if (empty($existe_ruc)) {
                $proveedor = new Compra();
                $proveedor->ruc = $request->ruc;
                $proveedor->razonsocial = $request->razonsocial;
                $proveedor->direccion = $request->direccion;
                $proveedor->telfprincipal = $request->telfprincipal;
                $proveedor->notas = $request->notas;
                $proveedor->email = $request->email;
                $proveedor->vendedorcontacto = $request->vendedorcontacto;
                $proveedor->tlfvendedorcontacto = $request->tlfvendedorcontacto;
                $proveedor->comentario = $request->comentario;
                $proveedor->ctacontable = $request->ctacontable;
                $proveedor->nombrebanco1 = $request->nombrebanco1;
                $proveedor->numctacorriente1 = $request->numctacorriente1;
                $proveedor->numctaahorros1 = $request->numctaahorros1;
                $proveedor->nombrebanco2 = $request->nombrebanco2;
                $proveedor->numctacorriente2 = $request->numctacorriente2;
                $proveedor->numctaahorros2 = $request->numctaahorros2;
                $proveedor->save();
                //return view('welcome');
                $message = $proveedor ? 'Se ha registrado el Proveedor' . $request->proveedor. 'de forma exitosa.' : 'Error al Registrar';

                //Session::flash('message', 'Te has registrado exitosamente ');
                return redirect()->route('proveedor')->with('message', $message);
        } else {
            $message2 = 'Este Proveedor ya se encuentra registrado';
            return redirect()->route('proveedor')->with('message2', $message2);
        }
    }


    public function edit($id){
        $compras = Compra::where('IDFacturacion','=', $id)->first();
        $color = Color::where('user_id', Auth::User()->id)->first();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        return view('layouts.compras.edit',compact('compras','color','rol'));
    }
    public function update(Request $request, $id){

        $proveedor= Compra::where('id','=', $id)->first();
        $proveedor->ruc = $request->ruc;
        $proveedor->razonsocial = $request->razonsocial;
        $proveedor->direccion = $request->direccion;
        $proveedor->telfprincipal = $request->telfprincipal;
        $proveedor->notas = $request->notas;
        $proveedor->email = $request->email;
        $proveedor->tlfvendedorcontacto = $request->tlfvendedorcontacto;
        $proveedor->comentario = $request->comentario;
        $proveedor->ctacontable = $request->ctacontable;
        $proveedor->nombrebanco1 = $request->nombrebanco1;
        $proveedor->numctacorriente1 = $request->numctacorriente1;
        $proveedor->numctaahorros2 = $request->numctaahorros2;
        $proveedor->nombrebanco2 = $request->nombrebanco2;
        $proveedor->numctacorriente2 = $request->numctacorriente2;
        $proveedor->numctaahorros2 = $request->numctaahorros2;
        $proveedor->vendedorcontacto = $request->vendedorcontacto;
        $proveedor->save();

        $message = $proveedor?'Se ha actualizado el registro'. $request->proveedor.'de forma exitosa.' : 'Error al actualizar';
        return redirect()->route('proveedor')->with('message', $message);


    }


    public function destroy($id){
        $proveedor = Compra::find($id);
        $proveedor->delete($id);
        $message =  $proveedor?'Registro eliminado correctamente' : 'Error al Eliminar';
            return redirect()->route('proveedor')->with('message', $message);

    }
}
