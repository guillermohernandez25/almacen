<?php

namespace App\Http\Controllers;

use App\Accion;
use App\Almacen;
use App\CategoriaProducto;
use App\Cliente;
use App\Empresa;
use App\Inventario;
use App\Producto;
use App\Provider;
use App\Administradores;
use Illuminate\Http\Request;
use App\Venta;
use App\Color;
use App\AccesoRol;
use DB;
class UploadController extends Controller
{


    public function consulta_almacen(Request $request, $empresa){
        $json =array();
        if(!empty($empresa)){
            $emp = Empresa::where('RUC', $empresa)->first();

        }
       // dd($emp);
        if(!empty($emp)){
            $json['IDpref'] = $emp->IDpref;
            $alma = Almacen::where('IDpref',$emp->IDpref)->get();
            if(isset($alma)) {
                for ($i = 0; $i < sizeof($alma); $i++) {
                    $json['consulta_almacen'][$i]['IDalmacen'] = $alma[$i]->IDalmacen;
                    $json['consulta_almacen'][$i]['nombre'] = $alma[$i]->nombre;
                }
            }else{
                $json['consulta_almacen'] = "No hay almacen para esta empresa!";
            }
            return response()->json($json);
        }else{
            $json['IDpref'] = "Esta empresa no existe en el sistema";
            return response()->json($json);
        }



    }

    public function download(Request $request, $empresa, $almacen = 1){
        $empresaruc = $empresa;
        $almacenruc = $almacen;
        $empresa = Empresa::where('RUC', $empresaruc)->pluck('IDpref');
        $almacen = Almacen::where('RUC', $almacenruc)->pluck('IDalmacen');
//dd($empresa, $almacen);
        if(empty($almacen)){
            $down_almacen = Almacen::where('estatus','!=','1')->where('IDpref',$empresa)->get();
            //se añadio a la db empresa para almacenes
        }else{
            $down_almacen = Almacen::where('estatus','!=','1')->where('IDpref',$empresa)->where('IDalmacen',$almacen)->get();
        }
        if(empty($almacen)){
            $down_proveedores = Provider::where('estatus','!=','1')->where('IDpref',$empresa)->get();
            //se añadio a la DB empresa para proveedores
        }else{
            $down_proveedores = Provider::where('estatus','!=','1')->where('IDpref',$empresa)->where('IDalmacen',$almacen)->get();
        }
        if(empty($almacen)){
            $down_inventarios = Inventario::where('estatus','!=','1')->where('IDpref',$empresa)->get();
            //se añadio a db empresa para inventarios
        }else{
            $down_inventarios = Inventario::where('estatus','!=','1')->where('IDpref',$empresa)->where('IDalmacen',$almacen)->get();
        }
        if(empty($almacen)){
            $down_productos = Producto::where('estatus','!=','1')->where('IDpref',$empresa)->get();
            //se añadio a db empresa para productos
        }else{
            $down_productos = Producto::where('estatus','!=','1')->where('IDpref',$empresa)->where('IDalmacen',$almacen)->get();
        }

        if(empty($almacen)){
            $down_administradores = Administradores::where('estatus','!=','1')->where('IDpref',$empresa)->get();
            //se añadio a db empresa para productos
        }else{
            $down_administradores = Administradores::where('estatus','!=','1')->where('IDpref',$empresa)->where('IDalmacen',$almacen)->get();
        }

        if(empty($almacen)){
            $down_categorias = CategoriaProducto::where('estatus','!=','1')->where('IDpref',$empresa)->get();
            //se añadio a db empresa para productos
        }else{
            $down_categorias = CategoriaProducto::where('estatus','!=','1')->where('IDpref',$empresa)->where('IDalmacen',$almacen)->get();
        }

        if(empty($almacen)){
            $down_clientes = Cliente::where('estatus','!=','1')->where('IDpref',$empresa)->get();
            //se añadio a db empresa para productos
        }else{
            $down_clientes = Cliente::where('estatus','!=','1')->where('IDpref',$empresa)->where('IDalmacen',$almacen)->get();
        }
        //dd($down_almacen,$down_proveedores,$down_inventarios,$down_inventarios,$down_productos, $down_administradores,$down_categorias,$down_clientes);

        $pos = 0;
        if(sizeof($down_almacen) >= 1){
            $json['download'][$pos]['table'] = "almacen";

            for($i = 0; $i < sizeof($down_almacen); $i++){
                $json['download'][$pos]['registos'][$i] = $down_almacen[$i];
            }
            $pos = $pos + 1;
        }
        if(sizeof($down_proveedores) >= 1){
            $json['download'][$pos]['table'] = "proveedores";

            for($i = 0; $i < sizeof($down_proveedores); $i++){
                $json['download'][$pos]['registos'][$i] = $down_proveedores[$i];
            }
            $pos = $pos + 1;
        }
        if(sizeof($down_inventarios) >= 1){
            $json['download'][$pos]['table'] = "inventarios";

            for($i = 0; $i < sizeof($down_inventarios); $i++){
                $json['download'][$pos]['registos'][$i] = $down_inventarios[$i];
            }
            $pos = $pos + 1;
        }
        if(sizeof($down_productos) >= 1){
            $json['download'][$pos]['table'] = "articulos";

            for($i = 0; $i < sizeof($down_productos); $i++){
                $json['download'][$pos]['registos'][$i] = $down_productos[$i];
            }
            $pos = $pos + 1;
        }
        if(sizeof($down_administradores) >= 1){
            $json['download'][$pos]['table'] = "administradores";

            for($i = 0; $i < sizeof($down_administradores); $i++){
                $json['download'][$pos]['registos'][$i] = $down_administradores[$i];
            }
            $pos = $pos + 1;
        }
        if(sizeof($down_categorias) >= 1){
            $json['download'][$pos]['table'] = "categorias";

            for($i = 0; $i < sizeof($down_categorias); $i++){
                $json['download'][$pos]['registos'][$i] = $down_categorias[$i];
            }
            $pos = $pos + 1;
        }
        if(sizeof($down_clientes) >= 1){
            $json['download'][$pos]['table'] = "clientes";

            for($i = 0; $i < sizeof($down_clientes); $i++){
                $json['download'][$pos]['registos'][$i] = $down_clientes[$i];
            }
            $pos = $pos + 1;
        }
        //$message= "true";

        return response()->json($json);
    }

    public function upload(Request $request){

        //dd(sizeof($request->upload));
        for($i = 0; $i < sizeof($request->upload); $i++){
            $tabla = $request->upload[$i]['table'];
            //dd($tabla);
            for($j = 0; $j < sizeof($request->upload[$i]['registros']); $j++){
                if($request->upload[$i]['registros'][$j]['estatus'] == 2){
                    $id = DB::table($request->upload[$i]['table'])->insertGetId(
                        $request->upload[$i]['registros'][$j]
                    );
                    DB::table($request->upload[$i]['table'])->where("ID".$request->upload[$i]['table']."",$id)->update(
                        ["estatus" => 1]
                    );
                }
                if($request->upload[$i]['registros'][$j]['estatus'] == 3){
                    //dd($request->upload[$i]['registros'][$j]["ID".$request->upload[$i]['table'].""]);
                    //dd($request->upload[$i]['registros'][$j]);
                    //dd($request->upload[$i]['registros'][$j]);
                    //for($k = 0; $k < sizeof($request->upload[$i]['registros'][$j]); $k++){
                    $otro = $request->upload[$i]['registros'][$j];
                    $otro['estatus'] = 1;
                    //dd($otro);
                    //$request->upload[$i]['registros'][$j]['estatus'] = "1";
                        DB::table($request->upload[$i]['table'])->where("ID".$request->upload[$i]['table']."",$request->upload[$i]['registros'][$j]["ID".$request->upload[$i]['table'].""])->update(
                            $otro
                        );
                        /*DB::table($request->upload[$i]['table'])->where("ID".$request->upload[$i]['table']."",$request->upload[$i]['registros'][$j]["ID".$request->upload[$i]['table'].""])->update(
                        ["estatus" => 1]
                    );*/
                    //}
                }
                if($request->upload[$i]['registros'][$j]['estatus'] == 4){
                    //dd($request->upload[$i]['registros'][$j]["ID".$request->upload[$i]['table'].""]);
                    //dd($request->upload[$i]['registros'][$j]);
                    //dd($request->upload[$i]['registros'][$j]);
                    //for($k = 0; $k < sizeof($request->upload[$i]['registros'][$j]); $k++){
                    DB::table($request->upload[$i]['table'])->where("ID".$request->upload[$i]['table']."",$request->upload[$i]['registros'][$j]["ID".$request->upload[$i]['table'].""])->delete();
                    //}
                }
            }
        }


        $message= "true";

        return response()->json($message);
    }
    public function uploadespecifico(Request $request, $empresa, $almacen){

        //dd(sizeof($request->upload));
        for($i = 0; $i < sizeof($request->upload); $i++){
            $tabla = $request->upload[$i]['table'];
            //dd($tabla);
            for($j = 0; $j < sizeof($request->upload[$i]['registros']); $j++){
                if($request->upload[$i]['registros'][$j]['estatus'] == 2){
                    $id = DB::table($request->upload[$i]['table'])->insertGetId(
                        $request->upload[$i]['registros'][$j]
                    );
                    DB::table($request->upload[$i]['table'])->where("ID".$request->upload[$i]['table']."",$id)->update(
                        ["estatus" => 1]
                    );
                }
                if($request->upload[$i]['registros'][$j]['estatus'] == 3){
                    //dd($request->upload[$i]['registros'][$j]["ID".$request->upload[$i]['table'].""]);
                    //dd($request->upload[$i]['registros'][$j]);
                    //dd($request->upload[$i]['registros'][$j]);
                    //for($k = 0; $k < sizeof($request->upload[$i]['registros'][$j]); $k++){
                    $otro = $request->upload[$i]['registros'][$j];
                    $otro['estatus'] = 1;
                    //dd($otro);
                    //$request->upload[$i]['registros'][$j]['estatus'] = "1";
                    DB::table($request->upload[$i]['table'])->where("ID".$request->upload[$i]['table']."",$request->upload[$i]['registros'][$j]["ID".$request->upload[$i]['table'].""])->update(
                        $otro
                    );
                    /*DB::table($request->upload[$i]['table'])->where("ID".$request->upload[$i]['table']."",$request->upload[$i]['registros'][$j]["ID".$request->upload[$i]['table'].""])->update(
                    ["estatus" => 1]
                );*/
                    //}
                }
                if($request->upload[$i]['registros'][$j]['estatus'] == 4){
                    //dd($request->upload[$i]['registros'][$j]["ID".$request->upload[$i]['table'].""]);
                    //dd($request->upload[$i]['registros'][$j]);
                    //dd($request->upload[$i]['registros'][$j]);
                    //for($k = 0; $k < sizeof($request->upload[$i]['registros'][$j]); $k++){
                    DB::table($request->upload[$i]['table'])->where("ID".$request->upload[$i]['table']."",$request->upload[$i]['registros'][$j]["ID".$request->upload[$i]['table'].""])->delete();
                    //}
                }
            }
        }


        $message= "true";

        return response()->json($message);
    }

}
