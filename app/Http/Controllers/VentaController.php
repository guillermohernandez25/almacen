<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venta;
use App\Color;

use Auth;
use App\AccesoRol;
class VentaController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

    }
 
    public function index(Request $request ){

        //dd($request->fecha_inicial);
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(10,json_decode($rol),false)) {
            if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2") {
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $color = Color::where('user_id', Auth::User()->id)->first();

                $ventas = Venta::groupBy('Factura_Id')->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                if (empty($request->fecha_inicial) && empty($request->fecha_final)) {
                    $request->fecha_inicial = date('Y-m-d');
                    $request->fecha_final = date('Y-m-d');
                    $desde = $request->fecha_inicial = date('Y-m-d');
                    $hasta = $request->fecha_final = date('Y-m-d');
                    //dd($fecha_inicial);
                    $ventas_total = Venta::groupBy('Factura_Id')->whereBetween('Fecha', [$request->fecha_inicial, $request->fecha_final])->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();

                    if (sizeof($ventas_total) >= 1) {
                        for ($i = 0; $i < sizeof($ventas_total); $i++) {
                            $valor = 0;
                            $busqueda_items = Venta::where('Factura_Id', $ventas_total[$i]->Factura_Id)->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                            for ($j = 0; $j < sizeof($busqueda_items); $j++) {
                                $valor = $valor + $busqueda_items[$j]->Valor_Total;
                            }

                            $ventas_suma[$i] = $valor;
                        }
                        //dd($ventas_total);
                        $ventas_final = 0;
                        for ($k = 0; $k < sizeof($ventas_suma); $k++) {
                            $ventas_final = $ventas_final + $ventas_suma[$k];
                        }
                        //dd($ventas_final,$ventas_total[0]->Factura_Id,$busqueda_items,$ventas_total, $ventas_suma);
                    }
                } else {
                    $desde = $request->fecha_inicial;
                    $hasta = $request->fecha_final;
                    $ventas_total = Venta::groupBy('Factura_Id')->whereBetween('Fecha', [$request->fecha_inicial, $request->fecha_final])->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                    //dd( $ventas_total, $request->fecha_inicial, $request->fecha_final );
                    if (sizeof($ventas_total) >= 1) {
                        for ($i = 0; $i < sizeof($ventas_total); $i++) {
                            $valor = 0;
                            $busqueda_items = Venta::where('Factura_Id', $ventas_total[$i]->Factura_Id)->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                            for ($j = 0; $j < sizeof($busqueda_items); $j++) {
                                $valor = $valor + $busqueda_items[$j]->Valor_Total;
                            }

                            $ventas_suma[$i] = $valor;
                        }
                        //dd($ventas_total);
                        $ventas_final = 0;
                        for ($k = 0; $k < sizeof($ventas_suma); $k++) {
                            $ventas_final = $ventas_final + $ventas_suma[$k];
                        }
                        //dd($ventas_final,$ventas_total[0]->Factura_Id,$busqueda_items,$ventas_total, $ventas_suma);
                    }

                }

                return view('layouts.ventas.index', compact('desde', 'hasta', 'ventas_total', 'ventas_suma', 'ventas_final', 'rol', 'ventas', 'color'));
            }elseif(Auth::User()->rol_id == "2"){
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $color = Color::where('user_id', Auth::User()->id)->first();

                $ventas = Venta::groupBy('Factura_Id')->where('IDpref',Auth::User()->IDpref)->get();
                if (empty($request->fecha_inicial) && empty($request->fecha_final)) {
                    $request->fecha_inicial = date('Y-m-d');
                    $request->fecha_final = date('Y-m-d');
                    $desde = $request->fecha_inicial = date('Y-m-d');
                    $hasta = $request->fecha_final = date('Y-m-d');
                    //dd($fecha_inicial);
                    $ventas_total = Venta::groupBy('Factura_Id')->whereBetween('Fecha', [$request->fecha_inicial, $request->fecha_final])->where('IDpref',Auth::User()->IDpref)->get();

                    if (sizeof($ventas_total) >= 1) {
                        for ($i = 0; $i < sizeof($ventas_total); $i++) {
                            $valor = 0;
                            $busqueda_items = Venta::where('Factura_Id', $ventas_total[$i]->Factura_Id)->where('IDpref',Auth::User()->IDpref)->get();
                            for ($j = 0; $j < sizeof($busqueda_items); $j++) {
                                $valor = $valor + $busqueda_items[$j]->Valor_Total;
                            }

                            $ventas_suma[$i] = $valor;
                        }
                        //dd($ventas_total);
                        $ventas_final = 0;
                        for ($k = 0; $k < sizeof($ventas_suma); $k++) {
                            $ventas_final = $ventas_final + $ventas_suma[$k];
                        }
                        //dd($ventas_final,$ventas_total[0]->Factura_Id,$busqueda_items,$ventas_total, $ventas_suma);
                    }
                } else {
                    $desde = $request->fecha_inicial;
                    $hasta = $request->fecha_final;
                    $ventas_total = Venta::groupBy('Factura_Id')->whereBetween('Fecha', [$request->fecha_inicial, $request->fecha_final])->where('IDpref',Auth::User()->IDpref)->get();
                    //dd( $ventas_total, $request->fecha_inicial, $request->fecha_final );
                    if (sizeof($ventas_total) >= 1) {
                        for ($i = 0; $i < sizeof($ventas_total); $i++) {
                            $valor = 0;
                            $busqueda_items = Venta::where('Factura_Id', $ventas_total[$i]->Factura_Id)->where('IDpref',Auth::User()->IDpref)->get();
                            for ($j = 0; $j < sizeof($busqueda_items); $j++) {
                                $valor = $valor + $busqueda_items[$j]->Valor_Total;
                            }

                            $ventas_suma[$i] = $valor;
                        }
                        //dd($ventas_total);
                        $ventas_final = 0;
                        for ($k = 0; $k < sizeof($ventas_suma); $k++) {
                            $ventas_final = $ventas_final + $ventas_suma[$k];
                        }
                        //dd($ventas_final,$ventas_total[0]->Factura_Id,$busqueda_items,$ventas_total, $ventas_suma);
                    }

                }

                return view('layouts.ventas.index', compact('desde', 'hasta', 'ventas_total', 'ventas_suma', 'ventas_final', 'rol', 'ventas', 'color'));
            }elseif(Auth::User()->rol_id == "1"){
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $color = Color::where('user_id', Auth::User()->id)->first();

                $ventas = Venta::groupBy('Factura_Id')->get();
                if (empty($request->fecha_inicial) && empty($request->fecha_final)) {
                    $request->fecha_inicial = date('Y-m-d');
                    $request->fecha_final = date('Y-m-d');
                    $desde = $request->fecha_inicial = date('Y-m-d');
                    $hasta = $request->fecha_final = date('Y-m-d');
                    //dd($fecha_inicial);
                    $ventas_total = Venta::groupBy('Factura_Id')->whereBetween('Fecha', [$request->fecha_inicial, $request->fecha_final])->get();

                    if (sizeof($ventas_total) >= 1) {
                        for ($i = 0; $i < sizeof($ventas_total); $i++) {
                            $valor = 0;
                            $busqueda_items = Venta::where('Factura_Id', $ventas_total[$i]->Factura_Id)->get();
                            for ($j = 0; $j < sizeof($busqueda_items); $j++) {
                                $valor = $valor + $busqueda_items[$j]->Valor_Total;
                            }

                            $ventas_suma[$i] = $valor;
                        }
                        //dd($ventas_total);
                        $ventas_final = 0;
                        for ($k = 0; $k < sizeof($ventas_suma); $k++) {
                            $ventas_final = $ventas_final + $ventas_suma[$k];
                        }
                        //dd($ventas_final,$ventas_total[0]->Factura_Id,$busqueda_items,$ventas_total, $ventas_suma);
                    }
                } else {
                    $desde = $request->fecha_inicial;
                    $hasta = $request->fecha_final;
                    $ventas_total = Venta::groupBy('Factura_Id')->whereBetween('Fecha', [$request->fecha_inicial, $request->fecha_final])->get();
                    //dd( $ventas_total, $request->fecha_inicial, $request->fecha_final );
                    if (sizeof($ventas_total) >= 1) {
                        for ($i = 0; $i < sizeof($ventas_total); $i++) {
                            $valor = 0;
                            $busqueda_items = Venta::where('Factura_Id', $ventas_total[$i]->Factura_Id)->get();
                            for ($j = 0; $j < sizeof($busqueda_items); $j++) {
                                $valor = $valor + $busqueda_items[$j]->Valor_Total;
                            }

                            $ventas_suma[$i] = $valor;
                        }
                        //dd($ventas_total);
                        $ventas_final = 0;
                        for ($k = 0; $k < sizeof($ventas_suma); $k++) {
                            $ventas_final = $ventas_final + $ventas_suma[$k];
                        }
                        //dd($ventas_final,$ventas_total[0]->Factura_Id,$busqueda_items,$ventas_total, $ventas_suma);
                    }

                }

                return view('layouts.ventas.index', compact('desde', 'hasta', 'ventas_total', 'ventas_suma', 'ventas_final', 'rol', 'ventas', 'color'));
            }

        }else{
            return redirect()->route('home');
        }
    }
    public function create(){
        $color = Color::where('user_id', Auth::User()->id)->first();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');

        return view('layouts.ventas.create', compact('color','rol'));
        //->with('clientes', $clientes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $existe_ruc = Venta::where('ruc', '=', $request->ruc)->first();
        if (empty($existe_ruc)) {
                $venta = new Venta();
                $venta->ruc = $request->ruc;
                $venta->razonsocial = $request->razonsocial;
                $venta->direccion = $request->direccion;
                $venta->telfprincipal = $request->telfprincipal;
                $venta->notas = $request->notas;
                $venta->email = $request->email;
                $venta->vendedorcontacto = $request->vendedorcontacto;
                $venta->tlfvendedorcontacto = $request->tlfvendedorcontacto;
                $venta->comentario = $request->comentario;
                $venta->ctacontable = $request->ctacontable;
                $venta->nombrebanco1 = $request->nombrebanco1;
                $venta->numctacorriente1 = $request->numctacorriente1;
                $venta->numctaahorros1 = $request->numctaahorros1;
                $venta->nombrebanco2 = $request->nombrebanco2;
                $venta->numctacorriente2 = $request->numctacorriente2;
                $venta->numctaahorros2 = $request->numctaahorros2;
                $venta->save();
                //return view('welcome');
                $message = $venta ? 'Se ha registrado el venta' . $request->venta. 'de forma exitosa.' : 'Error al Registrar';

                //Session::flash('message', 'Te has registrado exitosamente ');
                return redirect()->route('venta')->with('message', $message);
        } else {
            $message2 = 'Este venta ya se encuentra registrado';
            return redirect()->route('venta')->with('message2', $message2);
        }
    }


    public function show($id){
        $ventas = Venta::where('Factura_Id','=', $id)->get;
        $color = Color::where('user_id', Auth::User()->id)->first();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        return view('layouts.ventas.show',compact('ventas','color','rol'));
    }
    public function update(Request $request, $id){

        $venta= Venta::where('id','=', $id)->first();
        $venta->ruc = $request->ruc;
        $venta->razonsocial = $request->razonsocial;
        $venta->direccion = $request->direccion;
        $venta->telfprincipal = $request->telfprincipal;
        $venta->notas = $request->notas;
        $venta->email = $request->email;
        $venta->tlfvendedorcontacto = $request->tlfvendedorcontacto;
        $venta->comentario = $request->comentario;
        $venta->ctacontable = $request->ctacontable;
        $venta->nombrebanco1 = $request->nombrebanco1;
        $venta->numctacorriente1 = $request->numctacorriente1;
        $venta->numctaahorros2 = $request->numctaahorros2;
        $venta->nombrebanco2 = $request->nombrebanco2;
        $venta->numctacorriente2 = $request->numctacorriente2;
        $venta->numctaahorros2 = $request->numctaahorros2;
        $venta->vendedorcontacto = $request->vendedorcontacto;
        $venta->save();

        $message = $venta?'Se ha actualizado el registro'. $request->venta.'de forma exitosa.' : 'Error al actualizar';
        return redirect()->route('venta')->with('message', $message);


    }


    public function destroy($id){

        $venta = Venta::find($id);
        $venta->delete($id);
        $message =  $venta?'Registro eliminado correctamente' : 'Error al Eliminar';
            return redirect()->route('venta')->with('message', $message);

    }
}
