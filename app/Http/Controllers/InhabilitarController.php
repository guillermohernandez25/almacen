<?php

namespace App\Http\Controllers;

use App\Salarios;
use App\UserRol;
use Illuminate\Http\Request;
use App\User;
use App\Color;
use App\Almacen;
use App\Empresa;
use App\Horarios;
use App\Cargos;
use Auth;
use App\AccesoRol;
class InhabilitarController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(19,json_decode($rol),false)) {
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        $color = Color::where('user_id', Auth::User()->id)->first();
            if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->get();
                $usuarios = User::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
            }elseif(Auth::User()->rol_id == "2") {
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
                $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->get();
                $usuarios = User::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
            }elseif(Auth::User()->rol_id == "1") {
                $almacen = Almacen::where('estatus','!=','4')->get();
                $empresa = Empresa::all();
                $usuarios = User::where('estatus','!=','4')->get();
            }



        return view('layouts.inhabilitar.index', compact('usuarios','color','rol'));
        }else {
            return redirect()->route('home');
        }

    }

    public function status($id){

        $color = Color::where('user_id', Auth::User()->id)->first();

        $usuarios = User::where('IDusuario',$id)->first();
        if($usuarios->active == "1"){
        $usuarios->active = "0";
        }else{
            $usuarios->active = "1";
        }
        $usuarios->save();

        return redirect()->route('inhabilitar');

    }
    public function user_create(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(19,json_decode($rol),false)) {
            $roles = UserRol::all();
            $color = Color::where('user_id', Auth::User()->id)->first();
            if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
                $almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();

            }elseif(Auth::User()->rol_id == "2") {
                $almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
                $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();

            }elseif(Auth::User()->rol_id == "1") {
                $almacenes = Almacen::where('estatus','!=','4')->get();
                $empresas = Empresa::all();

            }
            $horarios = Horarios::all();
            $cargos = Cargos::all();
            $salarios = Salarios::all();
            return view('layouts.usuario.create', compact('almacenes','roles','rol', 'salarios', 'cargos', 'horarios', 'empresas', 'color'));
        }else{
            return redirect()->route('home');
        }

    }

    public function user_edit($id){
        $color = Color::where('user_id', Auth::User()->id)->first();
$roles = UserRol::all();
        $usuario = User::where('IDusuario',$id)->first();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
            $almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
            $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();

        }elseif(Auth::User()->rol_id == "2") {
            $almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
            $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();

        }elseif(Auth::User()->rol_id == "1") {
            $almacenes = Almacen::where('estatus','!=','4')->get();
            $empresas = Empresa::all();

        }
        $horarios = Horarios::all();
        $cargos = Cargos::all();
        $salarios = Salarios::all();
        return view('layouts.usuario.edit', compact('rol','usuario','color','roles','almacenes','empresas',
            'horarios','cargos','salarios'));

    }


    public function user_update(Request $request,$id){
        $color = Color::where('user_id', Auth::User()->id)->first();

        $usuario = User::where('IDusuario',$id)->first();
        $usuario->estatus = "3";
        $usuario->Nombres = $request->Nombres;
        $usuario->Apellidos = $request->Apellidos;
        $usuario->CedulaRUC = $request->CedulaRUC;
        $usuario->FechaNacimiento = $request->FechaNacimiento;
        $usuario->Edad = $request->Edad;
        $usuario->Genero = $request->Genero;
        $usuario->EstadoCivil = $request->EstadoCivil;
        $usuario->TelfCasa = $request->TelfCasa;
        $usuario->TelfCelular = $request->TelfCelular;
        $usuario->Conyuge = $request->Conyuge;
        $usuario->Direccion = $request->Direccion;
        $usuario->ProfesionOficio = $request->ProfesionOficio;
        $usuario->NumCargasFamiliares = $request->NumCargasFamiliares;
        $usuario->NumHijos = $request->NumHijos;
        $usuario->Comentarios = $request->Comentarios;
        $usuario->IDpref = $request->IDpref;
        $usuario->IDalmacen = $request->IDalmacen;
        $usuario->FechaContratacion = $request->FechaContratacion;
        $usuario->Cargos_IDCargo = $request->Cargos_IDCargo;
        $usuario->Salarios_IDSalario = $request->Salarios_IDSalario;
        $usuario->email = $request->email;
        $usuario->EmailPersonal = $request->email;
        $usuario->EmailEmpresarial = $request->EmailEmpresarial;
        $usuario->rol_id = $request->roles;

        if(!empty($request->password)){
            if($request->password == $request->confpassword) {
                $usuario->password = bcrypt($request->password);
                $usuario->save();
                $message = $usuario?'Se ha actualizado el registro'. $request->name.'de forma exitosa.' : 'Error al actualizar';
                return redirect()->route('inhabilitar')->with('message', $message);
            }else{
                $message2 = 'El Password no es igual en los dos campos';
                return redirect()->back()->with('message2', $message2);
            }

        }else{
            $usuario->save();
            $message = $usuario?'Se ha actualizado el registro'. $request->name.'de forma exitosa.' : 'Error al actualizar';
            return redirect()->route('inhabilitar')->with('message', $message);
        }

    }
    public function user_store(Request $request){
        $color = Color::where('user_id', Auth::User()->id)->first();

        $usuario = new User();
        $usuario->estatus = "2";
        $usuario->Nombres = $request->Nombres;
        $usuario->Apellidos = $request->Apellidos;
        $usuario->CedulaRUC = $request->CedulaRUC;
        $usuario->FechaNacimiento = $request->FechaNacimiento;
        $usuario->Edad = $request->Edad;
        $usuario->Genero = $request->Genero;
        $usuario->EstadoCivil = $request->EstadoCivil;
        $usuario->TelfCasa = $request->TelfCasa;
        $usuario->TelfCelular = $request->TelfCelular;
        $usuario->Conyuge = $request->Conyuge;
        $usuario->Direccion = $request->Direccion;
        $usuario->ProfesionOficio = $request->ProfesionOficio;
        $usuario->NumCargasFamiliares = $request->NumCargasFamiliares;
        $usuario->NumHijos = $request->NumHijos;
        $usuario->Comentarios = $request->Comentarios;
        $usuario->IDpref = $request->IDpref;
        $usuario->IDalmacen = $request->IDalmacen;
        $usuario->FechaContratacion = $request->FechaContratacion;
        $usuario->Cargos_IDCargo = $request->Cargos_IDCargo;
        $usuario->Salarios_IDSalario = $request->Salarios_IDSalario;
        $usuario->email = $request->email;
        $usuario->EmailPersonal = $request->email;
        $usuario->EmailEmpresarial = $request->EmailEmpresarial;
        $usuario->rol_id = $request->roles;

        if(!empty($request->password)){
            if($request->password == $request->confpassword) {
                $usuario->password = bcrypt($request->password);
                $usuario->save();
                $message = $usuario?'Se ha actualizado el registro'. $request->name.'de forma exitosa.' : 'Error al actualizar';
                return redirect()->route('inhabilitar')->with('message', $message);
            }else{
                $message2 = 'El Password no es igual en los dos campos';
                return redirect()->back()->with('message2', $message2);
            }

        }else{
            $usuario->save();
            $message = $usuario?'Se ha actualizado el registro'. $request->name.'de forma exitosa.' : 'Error al actualizar';
            return redirect()->route('inhabilitar')->with('message', $message);
        }

    }


}
