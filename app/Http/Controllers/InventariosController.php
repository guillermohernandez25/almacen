<?php

namespace App\Http\Controllers;

use App\Almacen;
use App\Inventario;
use App\InventarioArticulo;
use App\Producto;
use Illuminate\Http\Request;
use App\Empresa;
use App\Color;
use Auth;
use App\AccesoRol;
use Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use App\User;
use Importer;
class InventariosController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(){

        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(5,json_decode($rol),false)) {
            if(Auth::User()->rol_id != "1") {
                $color = Color::where('user_id', Auth::User()->id)->first();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $inventarios = Inventario::where('estatus','!=','4')->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                return view('layouts.inventarios.index', compact('rol', 'inventarios', 'color'));
            }else{
                $color = Color::where('user_id', Auth::User()->id)->first();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $inventarios = Inventario::where('estatus','!=','4')->get();
                return view('layouts.inventarios.index', compact('rol', 'inventarios', 'color'));
            }
        }else{
            return redirect()->route('home');
        }
    }
    public function ver($id){
        $color = Color::where('user_id', Auth::User()->id)->first();
        $relacion = InventarioArticulo::where('IDinventarios',$id)->get();
        $inventario = Inventario::where('IDinventarios',$id)->first();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(!empty($relacion)){
            for($i =0 ; $i < sizeof($relacion); $i++){
                $articulos[$i] = Producto::where('IDarticulo',$relacion[$i]->IDarticulo)->first();
            }
        }
        //dd($articulos, $relacion);
        return view('layouts.inventarios.articuloxinventario', compact('rol','color', 'articulos', 'inventario'));

    }
    public function create(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(6,json_decode($rol),false)) {
            $color = Color::where('user_id', Auth::User()->id)->first();
            $articulo = Producto::all();
            if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->get();
                $usuarios = User::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
            }elseif(Auth::User()->rol_id == "2") {
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->get();
                $usuarios = User::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
            }elseif(Auth::User()->rol_id == "1") {
                $almacen = Almacen::where('estatus','!=','4')->get();
                $empresa = Empresa::all();
                $usuarios = User::where('estatus','!=','4')->get();
            }

            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            return view('layouts.inventarios.create', compact('rol', 'color', 'articulo', 'almacen'));
            //->with('clientes', $clientes);
        }else{
            return redirect()->route('home');
        }
    }

    public function asignacion(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(7,json_decode($rol),false)) {
            $color = Color::where('user_id', Auth::User()->id)->first();
            if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
                $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->get();

            }elseif(Auth::User()->rol_id == "2") {
                $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->get();
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
                $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->get();

            }elseif(Auth::User()->rol_id == "1") {
                $articulo = Producto::where('estatus','!=','4')->get();
                $almacen = Almacen::where('estatus','!=','4')->get();
                $empresa = Empresa::all();

            }

            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            return view('layouts.inventarios.asignar', compact('rol', 'color', 'articulo', 'almacen'));
        }else{
            return redirect()->route('home');
        }
    }

    public function asignacion_excel(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(7,json_decode($rol),false)) {
            $color = Color::where('user_id', Auth::User()->id)->first();
            if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
                $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->get();

            }elseif(Auth::User()->rol_id == "2") {
                $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->get();
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
                $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->get();

            }elseif(Auth::User()->rol_id == "1") {
                $articulo = Producto::where('estatus','!=','4')->get();
                $almacen = Almacen::where('estatus','!=','4')->get();
                $empresa = Empresa::all();

            }
            return view('layouts.inventarios.asignar_excel', compact('rol', 'color', 'articulo', 'almacen'));
        }else{
            return redirect()->route('home');
        }
    }

    public function consulinventario($id){
        $inventario1 = Inventario::where('IDalmacen',$id)->get();
        $inventario['resultado'] = "";
        $inventario['registro'] = "";
        if(sizeof($inventario1) >= 1){
            $inventario['resultado'] = $inventario1;
            $inventario['registro'] = 'true';
            return $inventario;
        }else{
            $inventario['registro'] = 'false';
            return $inventario;
        }
    }

    public function consularticulo($id){
        $articulo = Producto::where('IDarticulo',$id)->first();
        return $articulo;
    }

    public function asignacionstore(Request $request){
        $existe_articulo = InventarioArticulo::where('IDarticulo', '=', $request->IDarticulo)->first();
        if (empty($existe_articulo)) {
            $inventarios = InventarioArticulo::where('IDarticulo',$request->IDarticulo)->first();
            if(!empty($inventarios)){
                $inventarios->IDinventarios = $request->IDinventarios;
                $inventarios->IDarticulo = $request->IDarticulo;
                $inventarios->estatus = "2";
                $inventarios->save();
            }else{
                $inventarios = new InventarioArticulo();
                $inventarios->IDinventarios = $request->IDinventarios;
                $inventarios->IDarticulo = $request->IDarticulo;
                $inventarios->estatus = "2";
                $inventarios->save();
            }

            $inventario = Inventario::where('IDinventarios', $request->IDinventarios)->first();
            //return view('welcome');
            $articulo = Producto::where('IDarticulo',$request->IDarticulo)->first();
            $articulo->AlmacenCentral = "0";
            $articulo->IDinventarios = $request->IDinventarios;
            $articulo->IDalmacen = $inventario->IDalmacen;
            $articulo->save();
            $message = $inventarios ? 'Se ha registrado el articulo de forma exitosa.' : 'Error al Registrar';

            //Session::flash('message', 'Te has registrado exitosamente ');
            return redirect()->route('inventario')->with('message', $message);
        } else {
            $message2 = 'Este articulo ya se encuentra registrado en un Inventario';
            return redirect()->back()->with('message2', $message2);
        }
    }

    public function asignacionstore_excel(Request $request){
        $collection = (new FastExcel)->import($request->file('excel'));
        //dd($collection[0]['Codigo']);
        for($i = 0 ; $i < sizeof($collection); $i++){
            $productos = Producto::where('Cod',$collection[$i]['Código'])->first();
            //dd($productos);
            if(!empty($productos)){
                $productos->estatus = "2";
                $productos->IDalmacen = $request->IDalmacen;
                $productos->IDinventarios = $request->IDinventarios;
                $productos->IDpref = Auth::User()->IDpref;
                $productos->Cod = $collection[$i]['Código'];
                $productos->Articulo = $collection[$i]['Artículo'];
                $productos->Costo = $collection[$i]['Costo'];
                $productos->PVP = $collection[$i]['PVP'];
                $productos->Proveedor = $collection[$i]['Proveedor'];
                $productos->Cantidad = $collection[$i]['Cantidad'];
                $productos->FechaDeCompra = $collection[$i]['FechaCompra'];
                $productos->FechaCaducidad = $collection[$i]['FechaCaducidad'];
                $productos->Minimo_Para_Reposicion = $collection[$i]['MinimoRep'];
                $productos->AplicaIva = $collection[$i]['AplicaIVA'];
                $productos->AdvertirReposicion = $collection[$i]['AdvertirRep'];
                $productos->AdvertirCaducidad = $collection[$i]['AdvertirCad'];
                //$productos->Margen = $collection[$i]['Margen'];
                //$productos->MinIndisp = $collection[$i]['MinIndisp'];
                //$productos->RUC = $collection[$i]['RUC'];
                $productos->Presentacion = $collection[$i]['Presentación'];
                $productos->Contenido = $collection[$i]['Contenido'];
                $productos->PDMa = $collection[$i]['PDMa'];
                $productos->PDMi = $collection[$i]['PDMi'];
                $productos->PE1 = $collection[$i]['PE1'];
                $productos->PE2 = $collection[$i]['PE2'];
                $productos->ubicacion = $collection[$i]['UBICACIÓN'];
                $productos->save();

                $inventarios = InventarioArticulo::where('IDarticulo',$productos->IDarticulo)->first();
                if(!empty($inventarios)){
                    $inventarios->IDinventarios = $request->IDinventarios;
                    $inventarios->IDarticulo = $productos->IDarticulo;
                    $inventarios->estatus = "2";
                    $inventarios->save();
                }else{
                    $inventarios = new InventarioArticulo();
                    $inventarios->IDinventarios = $request->IDinventarios;
                    $inventarios->IDarticulo = $productos->IDarticulo;
                    $inventarios->estatus = "2";
                    $inventarios->save();
                }
                $inventario = Inventario::where('IDinventarios', $request->IDinventarios)->first();
                //return view('welcome');
                $articulo = Producto::where('IDarticulo', $productos->IDarticulo)->first();
                $articulo->AlmacenCentral = "0";
                $articulo->IDinventarios = $request->IDinventarios;
                $articulo->IDalmacen = $inventario->IDalmacen;
                $articulo->save();
            }
            if(empty($productos) ){

                $productos = new Producto();
                $productos->estatus = "2";
                $productos->IDalmacen = $request->IDalmacen;
                $productos->IDinventarios = $request->IDinventarios;
                $productos->IDpref = Auth::User()->IDpref;
                $productos->Cod = $collection[$i]['Código'];
                $productos->Articulo = $collection[$i]['Artículo'];
                $productos->Costo = $collection[$i]['Costo'];
                $productos->PVP = $collection[$i]['PVP'];
                $productos->Proveedor = $collection[$i]['Proveedor'];
                $productos->Cantidad = $collection[$i]['Cantidad'];
                $productos->FechaDeCompra = $collection[$i]['FechaCompra'];
                $productos->FechaCaducidad = $collection[$i]['FechaCaducidad'];
                $productos->Minimo_Para_Reposicion = $collection[$i]['MinimoRep'];
                $productos->AplicaIva = $collection[$i]['AplicaIVA'];
                $productos->AdvertirReposicion = $collection[$i]['AdvertirRep'];
                $productos->AdvertirCaducidad = $collection[$i]['AdvertirCad'];
                //$productos->Margen = $collection[$i]['Margen'];
                //$productos->MinIndisp = $collection[$i]['MinIndisp'];
                //$productos->RUC = $collection[$i]['RUC'];
                $productos->Presentacion = $collection[$i]['Presentación'];
                $productos->Contenido = $collection[$i]['Contenido'];
                $productos->PDMa = $collection[$i]['PDMa'];
                $productos->PDMi = $collection[$i]['PDMi'];
                $productos->PE1 = $collection[$i]['PE1'];
                $productos->PE2 = $collection[$i]['PE2'];
                $productos->ubicacion = $collection[$i]['UBICACIÓN'];
                $productos->save();

                $inventarios = InventarioArticulo::where('IDarticulo',$productos->IDarticulo)->first();
                if(!empty($inventarios)){
                    $inventarios->IDinventarios = $request->IDinventarios;
                    $inventarios->IDarticulo = $productos->IDarticulo;
                    $inventarios->estatus = "2";
                    $inventarios->save();
                }else{
                    $inventarios = new InventarioArticulo();
                    $inventarios->IDinventarios = $request->IDinventarios;
                    $inventarios->IDarticulo = $productos->IDarticulo;
                    $inventarios->estatus = "2";
                    $inventarios->save();
                }

                $inventario = Inventario::where('IDinventarios', $request->IDinventarios)->first();
                //return view('welcome');
                $articulo = Producto::where('IDarticulo', $productos->IDarticulo)->first();
                $articulo->AlmacenCentral = "0";
                $articulo->IDinventarios = $request->IDinventarios;
                $articulo->IDalmacen = $inventario->IDalmacen;
                $articulo->save();

            }
        }

        $message =  'Se ha registrado/actualizado los articulos y asigandos inventario de forma exitosa.';
        //Session::flash('message', 'Te has registrado exitosamente ');
        return redirect()->route('inventario')->with('message', $message);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $existe_inventario = Inventario::where('nombre', '=', $request->nombre)->where('IDalmacen','=',$request->IDalmacen)->first();
        if (empty($existe_inventario)) {
            $inventarios = new Inventario();
            $inventarios->IDalmacen = $request->IDalmacen;
            $inventarios->nombre = $request->nombre;
            $inventarios->descripcion = $request->descripcion;
            $inventarios->estatus = "2";
            $inventarios->save();
            //return view('welcome');
            $message = $inventarios ? 'Se ha registrado el inventario de forma exitosa.' : 'Error al Registrar';

            //Session::flash('message', 'Te has registrado exitosamente ');
            return redirect()->route('inventario')->with('message', $message);
        } else {
            $message2 = 'Este inventario ya se encuentra registrado en un almacen';
            return redirect()->back()->with('message2', $message2);
        }
    }


    public function edit($id){
        $inventarios = Inventario::where('IDinventarios','=', $id)->first();
        $color = Color::where('user_id', Auth::User()->id)->first();
        $articulo = Producto::all();
        $almacen = Almacen::all();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        return view('layouts.inventarios.edit',compact('rol','inventarios','color','articulo','almacen'));
    }

    public function update(Request $request, $id){
        $inventarios= Inventario::where('IDinventarios','=', $id)->first();
        $inventarios->IDalmacen = $request->IDalmacen;
        $inventarios->nombre = $request->nombre;
        $inventarios->descripcion = $request->descripcion;
        $inventarios->estatus = "3";
        $inventarios->save();
        $message = $inventarios?'Se ha actualizado el registro de forma exitosa.' : 'Error al actualizar';
        return redirect()->route('inventario')->with('message', $message);
    }

    public function destroy($id){
        $inventarios = Inventario::find($id);
        $inventarios->estatus = "4";
        $inventarios->save();
        $message =  $inventarios?'Registro eliminado correctamente' : 'Error al Eliminar';
        return redirect()->route('inventario')->with('message', $message);
    }
}
