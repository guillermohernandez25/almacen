<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Auth;
use App\Color;
use App\Color_type;
use App\AccesoRol;
class ColorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(18,json_decode($rol),false)) {
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            $color = Color::where('user_id', Auth::User()->id)->first();
            $color_list = Color_type::all();

            return view('layouts.color.color', compact('color', 'color_list', 'rol'));
        }else{
            return redirect()->route('home');
        }
    }

    public function store(Request $request)
    {
        //dd($request->color);

        $color = Color::where('user_id', Auth::User()->id)->first();
        if(!empty($color)){
            $color->color = $request->color;
            $color->save();
        }else{
            $color = new Color();
            $color->user_id = Auth::User()->id;
            $color->color = $request->color;
            $color->save();

        }

        return redirect()->route('home');
    }
}
