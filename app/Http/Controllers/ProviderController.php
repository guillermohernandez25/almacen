<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Provider;
use App\Color;
use App\Almacen;
use App\Empresa;
use Auth;
use App\Producto;
use App\AccesoRol;
class ProviderController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
 
    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(3,json_decode($rol),false)) {
            if(Auth::User()->rol_id != "1") {
                $color = Color::where('user_id', Auth::User()->id)->first();
                $proveedor = Provider::where('estatus','!=','4')->where('IDPref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                return view('layouts.provider.index', compact('rol', 'proveedor', 'color'));
            }else{
                $color = Color::where('user_id', Auth::User()->id)->first();
                $proveedor = Provider::where('estatus','!=','4')->get();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                return view('layouts.provider.index', compact('rol', 'proveedor', 'color'));
            }
        }else{
        return redirect()->route('home');
    }
    }
    public function create(){
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            if(in_array(4,json_decode($rol),false)) {

                $color = Color::where('user_id', Auth::User()->id)->first();
                if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
                    $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                    $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                    $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->get();

                }elseif(Auth::User()->rol_id == "2") {
                    $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->get();
                    $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
                    $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->get();

                }elseif(Auth::User()->rol_id == "1") {
                    $articulo = Producto::where('estatus','!=','4')->get();
                    $almacen = Almacen::where('estatus','!=','4')->get();
                    $empresa = Empresa::all();

                }
                //dd($almacen);
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                return view('layouts.provider.create', compact('rol', 'color', 'almacen'));
                //->with('clientes', $clientes);
            }else{
                return redirect()->route('home');
            }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $existe_ruc = Provider::where('ruc', '=', $request->ruc)->first();
        if (empty($existe_ruc)) {
                $proveedor = new Provider();
                $proveedor->IDalmacen = Auth::User()->IDalmacen;
                $proveedor->IDpref = Auth::User()->IDpref;
                $proveedor->estatus = "2";
                $proveedor->RUC = $request->RUC;
                $proveedor->RazonSocial = $request->RazonSocial;
                $proveedor->Direccion = $request->Direccion;
                $proveedor->TelfPrincipal = $request->TelfPrincipal;
                $proveedor->Notas = $request->Notas;
                $proveedor->IDalmacen = $request->IDalmacen;
                $proveedor->Email = $request->Email;
                $proveedor->Vendedor = $request->Vendedor;
                $proveedor->TelfVendedor = $request->TelfVendedor;
                $proveedor->Comentarios = $request->Comentarios;
                $proveedor->CtaContable = $request->CtaContable;
                $proveedor->Banco1 = $request->Banco1;
                $proveedor->NumCtaCorriente1 = $request->NumCtaCorriente1;
                $proveedor->NumCtaAhorros1 = $request->NumCtaAhorros1;
                $proveedor->Banco2 = $request->Banco2;
                $proveedor->NumCtaCorriente2 = $request->NumCtaCorriente2;
                $proveedor->NumCtaAhorros2 = $request->NumCtaAhorros2;

                $proveedor->save();
                //return view('welcome');
                $message = $proveedor ? 'Se ha registrado el Proveedor' . $request->proveedor. 'de forma exitosa.' : 'Error al Registrar';

                //Session::flash('message', 'Te has registrado exitosamente ');
                return redirect()->route('proveedor')->with('message', $message);
        } else {
            $message2 = 'Este Proveedor ya se encuentra registrado';
            return redirect()->route('proveedor')->with('message2', $message2);
        }
    }


    public function edit($id){
        $proveedor = Provider::where('IDproveedores','=', $id)->first();
        $color = Color::where('user_id', Auth::User()->id)->first();
        if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
            $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
            $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
            $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->get();

        }elseif(Auth::User()->rol_id == "2") {
            $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->get();
            $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
            $empresa = Empresa::where('IDpref',Auth::User()->IDpref)->get();

        }elseif(Auth::User()->rol_id == "1") {
            $articulo = Producto::where('estatus','!=','4')->get();
            $almacen = Almacen::where('estatus','!=','4')->get();
            $empresa = Empresa::all();

        }
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        return view('layouts.provider.edit',compact('rol','proveedor','color','almacen'));
    }
    public function update(Request $request, $id){

        $proveedor= Provider::where('IDproveedores','=', $id)->first();
        $proveedor->estatus = "3";
        $proveedor->RUC = $request->RUC;
        $proveedor->RazonSocial = $request->RazonSocial;
        $proveedor->Direccion = $request->Direccion;
        $proveedor->TelfPrincipal = $request->TelfPrincipal;
        $proveedor->Notas = $request->Notas;
        $proveedor->IDalmacen = $request->IDalmacen;
        $proveedor->Email = $request->Email;
        $proveedor->Vendedor = $request->Vendedor;
        $proveedor->TelfVendedor = $request->TelfVendedor;
        $proveedor->Comentarios = $request->Comentarios;
        $proveedor->CtaContable = $request->CtaContable;
        $proveedor->Banco1 = $request->Banco1;
        $proveedor->NumCtaCorriente1 = $request->NumCtaCorriente1;
        $proveedor->NumCtaAhorros1 = $request->NumCtaAhorros1;
        $proveedor->Banco2 = $request->Banco2;
        $proveedor->NumCtaCorriente2 = $request->NumCtaCorriente2;
        $proveedor->NumCtaAhorros2 = $request->NumCtaAhorros2;
        $proveedor->save();

        $message = $proveedor?'Se ha actualizado el registro'. $request->proveedor.'de forma exitosa.' : 'Error al actualizar';
        return redirect()->route('proveedor')->with('message', $message);


    }


    public function destroy($id){

        $proveedor = Provider::find($id);
        $proveedor->estatus = "4";
        $proveedor->save();
        $message =  $proveedor?'Registro eliminado correctamente' : 'Error al Eliminar';
            return redirect()->route('proveedor')->with('message', $message);

    }
}
