<?php

namespace App\Http\Controllers;


use App\Empresa;
use App\Almacen;
use Illuminate\Http\Request;
use App\User;
use App\Color;
use App\UserRol;
use App\Modulos;
use App\AccesoRol;
use App\Producto;
use Auth;
class RolesController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->pluck('panel_id');
        if(in_array(20,json_decode($rol),false)) {
            $color = Color::where('user_id', Auth::User()->id)->first();

            $roles = UserRol::all();
            if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
                $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();

            }elseif(Auth::User()->rol_id == "2") {
                $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->get();
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
                $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();

            }elseif(Auth::User()->rol_id == "1") {
                $articulo = Producto::where('estatus','!=','4')->get();
                $almacen = Almacen::where('estatus','!=','4')->get();
                $empresas = Empresa::all();

            }
            return view('layouts.roles.index', compact('empresas','rol','roles', 'color'));
        }else{
            return redirect()->route('home');
        }

    }
    public function edit(Request $request, $id){
        //dd($request,$id);
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->pluck('panel_id');
        if(in_array(20,json_decode($rol),false)) {
        $color = Color::where('user_id', Auth::User()->id)->first();
            if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
                $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                $empresa = Empresa::where('IDpref',$request->IDpref)->first();

            }elseif(Auth::User()->rol_id == "2") {
                $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->get();
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
                $empresa = Empresa::where('IDpref',$request->IDpref)->first();

            }elseif(Auth::User()->rol_id == "1") {
                $articulo = Producto::where('estatus','!=','4')->get();
                $almacen = Almacen::where('estatus','!=','4')->get();
                $empresa = Empresa::where('IDpref',$request->IDpref)->first();

            }
        $roles = UserRol::where('id', $id)->first();
        $roles_actual = AccesoRol::where('rol_id',$id)->where('IDpref',$request->IDpref)->get();
        $modulos = Modulos::all();
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->pluck('panel_id');
        return view('layouts.roles.edit', compact('empresa','roles_actual','rol','modulos','roles','color'));
        }else{
            return redirect()->route('home');
        }

    }



    public function update(Request $request, $id){
        $acceso = AccesoRol::where('rol_id',$id)->where('IDpref',$request->IDpref)->get();
        for($i = 0; $i < sizeof($acceso); $i++){
            $acceso[$i]->delete();
        }
        for($j = 0; $j < sizeof($request->roles); $j++) {
            $acceso = new AccesoRol();
            $acceso->rol_id = $id;
            $acceso->panel_id = $request->roles[$j];
            $acceso->IDpref = $request->IDpref;
            $acceso->save();
        }


        return redirect()->route('roles');
    }





}
