<?php

namespace App\Http\Controllers;

use App\CategoriaProducto;
use App\Producto;
use Illuminate\Http\Request;
use App\Provider;
use App\Color;
use Auth;
use App\AccesoRol;
class AlmacenGeneralController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
 
    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(21,json_decode($rol),false)) {
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            $color = Color::where('user_id', Auth::User()->id)->first();
            $productos = Producto::where('AlmacenCentral', '1')->where('estatus','!=','4')->paginate();
            return view('layouts.productos.index', compact('productos', 'color', 'rol'));
        }else{
            return redirect()->route('home');
        }

    }
    public function create(){
        $color = Color::where('user_id', Auth::User()->id)->first();
        $catego = CategoriaProducto::all();


        return view('layouts.productos.create', compact('color','catego','rol'));
        //->with('clientes', $clientes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $existe_cod = Producto::where('Cod', '=', $request->Cod)->first();
        if (empty($existe_cod)) {


            $productos = new Producto();

            $productos->IDCategoria = $request->IDCategoria;
                                                    $productos->Articulo = $request->Articulo;
                                                    $productos->Costo = $request->Costo;
                                                    $productos->Cantidad = $request->Cantidad;
                                                    $productos->TipoTrans = $request->TipoTrans;
                                                    $productos->Consulta_Espec_Otro = $request->Consulta_Espec_Otro;
                                                    $productos->AplicaIva = $request->AplicaIva;
                                                    $productos->CtaContable = $request->CtaContable;
                                                    $productos->Codigo_Impuestos = $request->Codigo_Impuestos;
                                                    $productos->FechaDeCompra = $request->FechaDeCompra;
                                                    $productos->Margen = $request->Margen;
                                                    $productos->MinIndisp = $request->MinIndisp;
                                                    $productos->RUC = $request->RUC;
                                                    $productos->Presentacion = $request->Presentacion;
                                                    $productos->Contenido = $request->Contenido;
                                                    $productos->PVP = $request->PVP;
                                                    $productos->Cod = $request->Cod;
                                                    $productos->IDEstante = $request->IDEstante;
                                                    $productos->AdvertirReposicion = $request->AdvertirReposicion;
                                                    $productos->FechaCaducidad = $request->FechaCaducidad;
                                                    $productos->AdvertirCaducidad = $request->AdvertirCaducidad;
                                                    $productos->IDUser = $request->IDUser;
                                                    $productos->FechaCierreTransaccion = $request->FechaCierreTransaccion;
                                                    $productos->TransaccionCerrada = $request->TransaccionCerrada;
                                                    $productos->IDCaja = $request->IDCaja;
                                                    $productos->PDMa = $request->PDMa;
                                                    $productos->PDMi = $request->PDMi;
                                                    $productos->PE1 = $request->PE1;
                                                    $productos->PE2 = $request->PE2;
                                                    $productos->IDDescuento = $request->IDDescuento;
                                                    $productos->ProductoElaborado = $request->ProductoElaborado;
                                                    $productos->CodComponenteProductoElaborado= $request->CodComponenteProductoElaborado;
                                                    $productos->Antelacion = $request->Antelacion;
                                                    $productos->AlfaNumericoPosEstante = $request->AlfaNumericoPosEstante;

                $productos->save();
                //return view('welcome');
                $message = $productos ? 'Se ha registrado la Categoria correctamente' . $request->Categoria. 'de forma exitosa.' : 'Error al Registrar';

                //Session::flash('message', 'Te has registrado exitosamente ');
                return redirect()->route('producto')->with('message', $message);
        } else {
            $message2 = 'Esta Categoria ya se encuentra registrado';
            return redirect()->route('producto')->with('message2', $message2);
        }
    }


    public function edit($id){
        $productos = Producto::where('IDProducto','=', $id)->first();
        $color = Color::where('user_id', Auth::User()->id)->first();
        $catego = CategoriaProducto::all();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        return view('layouts.productos.edit',compact('productos','color','catego','rol'));
    }
    public function update(Request $request, $id){

        $productos= Producto::where('IDProducto','=', $id)->first();
        $productos->IDCategoria = $request->IDCategoria;
        $productos->Articulo = $request->Articulo;
        $productos->Costo = $request->Costo;
        $productos->Cantidad = $request->Cantidad;
        $productos->TipoTrans = $request->TipoTrans;
        $productos->Consulta_Espec_Otro = $request->Consulta_Espec_Otro;
        $productos->AplicaIva = $request->AplicaIva;
        $productos->CtaContable = $request->CtaContable;
        $productos->Codigo_Impuestos = $request->Codigo_Impuestos;
        $productos->FechaDeCompra = $request->FechaDeCompra;
        $productos->Margen = $request->Margen;
        $productos->MinIndisp = $request->MinIndisp;
        $productos->RUC = $request->RUC;
        $productos->Presentacion = $request->Presentacion;
        $productos->Contenido = $request->Contenido;
        $productos->PVP = $request->PVP;
        $productos->Cod = $request->Cod;
        $productos->IDEstante = $request->IDEstante;
        $productos->AdvertirReposicion = $request->AdvertirReposicion;
        $productos->FechaCaducidad = $request->FechaCaducidad;
        $productos->AdvertirCaducidad = $request->AdvertirCaducidad;
        $productos->IDUser = $request->IDUser;
        $productos->FechaCierreTransaccion = $request->FechaCierreTransaccion;
        $productos->TransaccionCerrada = $request->TransaccionCerrada;
        $productos->IDCaja = $request->IDCaja;
        $productos->PDMa = $request->PDMa;
        $productos->PDMi = $request->PDMi;
        $productos->PE1 = $request->PE1;
        $productos->PE2 = $request->PE2;
        $productos->IDDescuento = $request->IDDescuento;
        $productos->ProductoElaborado = $request->ProductoElaborado;
        $productos->CodComponenteProductoElaborado= $request->CodComponenteProductoElaborado;
        $productos->Antelacion = $request->Antelacion;
        $productos->AlfaNumericoPosEstante = $request->AlfaNumericoPosEstante;
        $productos->save();

        $message = $productos?'Se ha actualizado el registro'. $request->Categoria.'de forma exitosa.' : 'Error al actualizar';
        return redirect()->route('producto')->with('message', $message);

    }
    public function destroy($id){

        $productos = Producto::where('IDProducto',$id);
        $productos->delete($id);
        $message =  $productos?'Registro eliminado correctamente' : 'Error al Eliminar';
            return redirect()->route('prodcuto')->with('message', $message);

    }
}
