<?php

namespace App\Http\Controllers;

use App\FacturaProveedor;
use App\Inventario;
use App\Producto;
use App\Venta;
use Illuminate\Http\Request;

use App\User;
use Auth;
use App\Color;
use App\Almacen;
use App\InventarioArticulo;
use App\Cliente;
class HomeControllerback extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $color = Color::where('user_id', Auth::User()->id)->first();
        $almacenes = Almacen::all()->first();
        //dd($almacenes);


            $clientes = count(Cliente::where('IDAlmacen', $almacenes->IDAlmacen)->get());

//dd($clientes);

            $inventarios = Inventario::where('almacen_id', $almacenes->IDAlmacen)->get();
            for ($b = 0; $b < sizeof($inventarios); $b++) {
                //dd($inventarios[$a]);
                    //dd($inventarios[$a][$b]);
                        $articulos[$b] = count(InventarioArticulo::where('inventario_id', $inventarios[$b]->IDInventario)->get());
            }

//dd($articulos,$almacenes);

            $articulos_almacen = 0;
            if(isset($articulos)){
            for ($e = 0; $e < sizeof($articulos); $e++) {
                //dd($articulos[$a]);
                $articulos_almacen = $articulos_almacen + $articulos[$e];
            }
            }else{
                $articulos_almacen = 0;
            }




            $ventas_almacen = Venta::where('IDAlmacen', $almacenes->IDAlmacen)->latest()->take(5)->get();



            $total_ventas_almacen = count(Venta::where('IDAlmacen', $almacenes->IDAlmacen)->get());



         $meses = ['01','02','03','04','05','06','07','08','09','10','11','12'];


            for($g = 0 ; $g < sizeof($meses); $g++){
                $mensual_ventas_almacen[$g] = count(Venta::where('IDAlmacen', $almacenes->IDAlmacen)->whereMonth('Fecha',$meses[$g])->whereYear('Fecha',date('Y'))->get());
            }



            $factura_proveedor = FacturaProveedor::where('IDAlmacen', $almacenes->IDAlmacen)->latest()->take(5)->get();


            $count_factura_proveedor = count(FacturaProveedor::where('IDAlmacen', $almacenes->IDAlmacen)->get());



            $producto_stock = Producto::where('IDAlmacen', $almacenes->IDAlmacen)->where('AdvertirReposicion','1')->latest()->take(5)->get();

//dd($producto_stock);

        return view('home',compact('producto_stock','count_factura_proveedor','factura_proveedor','color','ventas_almacen','total_ventas_almacen','almacenes','articulos_almacen','clientes','mensual_ventas_almacen'));
    }

    public function nuevapag()
    {
        $color = Color::where('user_id', Auth::User()->id)->first();
        $almacenes = Almacen::all();
        for ($f = 0; $f < sizeof($almacenes); $f ++) {
            $clientes[$f] = count(Cliente::where('IDAlmacen', $almacenes[$f]->IDAlmacen)->get());
        }
//dd($clientes);
        for ($a = 0; $a < sizeof($almacenes); $a ++) {
            $inventarios[$a] = Inventario::where('almacen_id', $almacenes[$a]->IDAlmacen)->get();
            for ($b = 0; $b < sizeof($inventarios[$a]); $b++) {
                //dd($inventarios[$a]);
                //dd($inventarios[$a][$b]);
                $articulos[$a][$b] = count(InventarioArticulo::where('inventario_id', $inventarios[$a][$b]->IDInventario)->get());
            }
        };
//dd($articulos,$almacenes);
        for ($d = 0; $d < sizeof($almacenes); $d ++) {
            $articulos_almacen[$d] = 0;
            if(isset($articulos[$d])){
                for ($e = 0; $e < sizeof($articulos[$d]); $e++) {
                    //dd($articulos[$a]);
                    $articulos_almacen[$d] = $articulos_almacen[$d] + $articulos[$d][$e];
                }
            }else{
                $articulos_almacen[$d] = 0;
            }

        }

        for ($f = 0; $f < sizeof($almacenes); $f ++) {
            $ventas_almacen[$f] = Venta::where('IDAlmacen', $almacenes[$f]->IDAlmacen)->latest()->take(5)->get();
        }

        for ($f = 0; $f < sizeof($almacenes); $f ++) {
            $total_ventas_almacen[$f] = count(Venta::where('IDAlmacen', $almacenes[$f]->IDAlmacen)->get());

        }

        $meses = ['01','02','03','04','05','06','07','08','09','10','11','12'];

        for ($f = 0; $f < sizeof($almacenes); $f ++) {
            for($g = 0 ; $g < sizeof($meses); $g++){
                $mensual_ventas_almacen[$f][$g] = count(Venta::where('IDAlmacen', $almacenes[$f]->IDAlmacen)->whereMonth('Fecha',$meses[$g])->whereYear('Fecha',date('Y'))->get());
            }
        }

        for ($f = 0; $f < sizeof($almacenes); $f ++) {
            $factura_proveedor[$f] = FacturaProveedor::where('IDAlmacen', $almacenes[$f]->IDAlmacen)->latest()->take(5)->get();
        }

        for ($f = 0; $f < sizeof($almacenes); $f ++) {
            $count_factura_proveedor[$f] = count(FacturaProveedor::where('IDAlmacen', $almacenes[$f]->IDAlmacen)->get());
        }

        for ($f = 0; $f < sizeof($almacenes); $f ++) {
            $producto_stock[$f] = Producto::where('IDAlmacen', $almacenes[$f]->IDAlmacen)->where('AdvertirReposicion','1')->latest()->take(5)->get();
        }
//dd($producto_stock);

        return view('home',compact('producto_stock','count_factura_proveedor','factura_proveedor','color','ventas_almacen','total_ventas_almacen','almacenes','articulos_almacen','clientes','mensual_ventas_almacen'));
    }

}
