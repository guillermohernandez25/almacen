<?php

namespace App\Http\Controllers;

use App\AccesoRol;
use App\FacturaProveedor;
use App\Inventario;
use App\Producto;
use App\Venta;
use Illuminate\Http\Request;

use App\User;
use Auth;
use App\Color;
use App\Almacen;
use App\InventarioArticulo;
use App\Cliente;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::User()->active == 1){
            if(Auth::User()->rol_id != "1") {
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                //dd($rol);

                $color = Color::where('user_id', Auth::User()->id)->first();
                $almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->first();
                $list_almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                //dd($almacenes);
                if(!empty($almacenes)){
                $clientes = count(Cliente::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('created_at', date('m'))->get());
                }else{
                    $clientes = 0;
                }
//dd($clientes);
                $inventarios = Inventario::where('IDalmacen', $almacenes->IDalmacen)->get();
                for ($b = 0; $b < sizeof($inventarios); $b++) {
                    //dd($inventarios[$a]);
                    //dd($inventarios[$a][$b]);
                    $articulos[$b] = count(InventarioArticulo::where('IDinventarios', $inventarios[$b]->IDinventario)->whereMonth('created_at', date('m'))->get());
                }

//dd($articulos,$almacenes);

                $articulos_almacen = 0;
                if (isset($articulos)) {
                    for ($e = 0; $e < sizeof($articulos); $e++) {
                        //dd($articulos[$a]);
                        $articulos_almacen = $articulos_almacen + $articulos[$e];
                    }
                } else {
                    $articulos_almacen = 0;
                }


                $ventas_almacen = Venta::where('IDalmacen', $almacenes->IDalmacen)->latest()->take(5)->groupBy('Factura_Id')->get();


                $total_ventas_almacen = count(Venta::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('created_at', date('m'))->groupBy('Factura_Id')->get());


                $meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];


                for ($g = 0; $g < sizeof($meses); $g++) {

                    $recolecta = Venta::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('Fecha', $meses[$g])->whereYear('Fecha', date('Y'))->get();
                    $sumatoria = 0;
                    for ($i = 0; $i < sizeof($recolecta); $i++) {
                        $sumatoria = $sumatoria + $recolecta[$i]->Valor_Total;
                    }
                    $mensual_ventas_almacen[$g] = number_format($sumatoria, 2, '.', '');;
                }


                $factura_proveedor = FacturaProveedor::where('IDalmacen', $almacenes->IDalmacen)->latest()->take(5)->get();


                $count_factura_proveedor = count(FacturaProveedor::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('created_at', date('m'))->get());


                $producto_stock = Producto::where('IDalmacen', $almacenes->IDalmacen)->where('AdvertirReposicion', '1')->latest()->take(5)->get();

//dd($producto_stock);

                return view('home', compact('rol', 'list_almacenes', 'producto_stock', 'count_factura_proveedor', 'factura_proveedor', 'color', 'ventas_almacen', 'total_ventas_almacen', 'almacenes', 'articulos_almacen', 'clientes', 'mensual_ventas_almacen'));
            }else{
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                //dd($rol);

                $color = Color::where('user_id', Auth::User()->id)->first();
                $almacenes = Almacen::all()->first();
                $list_almacenes = Almacen::all();
                //dd($almacenes);
                $clientes = count(Cliente::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('created_at', date('m'))->get());
//dd($clientes);
                $inventarios = Inventario::where('IDalmacen', $almacenes->IDalmacen)->get();
                for ($b = 0; $b < sizeof($inventarios); $b++) {
                    //dd($inventarios[$a]);
                    //dd($inventarios[$a][$b]);
                    $articulos[$b] = count(InventarioArticulo::where('IDinventarios', $inventarios[$b]->IDinventarios)->whereMonth('created_at', date('m'))->get());
                }

//dd($articulos,$almacenes);

                $articulos_almacen = 0;
                if (isset($articulos)) {
                    for ($e = 0; $e < sizeof($articulos); $e++) {
                        //dd($articulos[$a]);
                        $articulos_almacen = $articulos_almacen + $articulos[$e];
                    }
                } else {
                    $articulos_almacen = 0;
                }


                $ventas_almacen = Venta::where('IDalmacen', $almacenes->IDalmacen)->latest()->take(5)->groupBy('Factura_Id')->get();


                $total_ventas_almacen = count(Venta::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('created_at', date('m'))->groupBy('Factura_Id')->get());


                $meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];


                for ($g = 0; $g < sizeof($meses); $g++) {

                    $recolecta = Venta::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('Fecha', $meses[$g])->whereYear('Fecha', date('Y'))->get();
                    $sumatoria = 0;
                    for ($i = 0; $i < sizeof($recolecta); $i++) {
                        $sumatoria = $sumatoria + $recolecta[$i]->Valor_Total;
                    }
                    $mensual_ventas_almacen[$g] = number_format($sumatoria, 2, '.', '');;
                }


                $factura_proveedor = FacturaProveedor::where('IDalmacen', $almacenes->IDalmacen)->latest()->take(5)->get();


                $count_factura_proveedor = count(FacturaProveedor::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('created_at', date('m'))->get());


                $producto_stock = Producto::where('IDalmacen', $almacenes->IDalmacen)->where('AdvertirReposicion', '1')->latest()->take(5)->get();

//dd($producto_stock);

                return view('home', compact('rol', 'list_almacenes', 'producto_stock', 'count_factura_proveedor', 'factura_proveedor', 'color', 'ventas_almacen', 'total_ventas_almacen', 'almacenes', 'articulos_almacen', 'clientes', 'mensual_ventas_almacen'));
            }
        }else{
            Auth::logout();
            return redirect()->route('home');
        }
    }

    public function nuevapag($id)
    {
        if(Auth::User()->rol_id != "1") {
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            $color = Color::where('user_id', Auth::User()->id)->first();
            $almacenes = Almacen::where('IDalmacen', $id)->first();
            $list_almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
            //dd($almacenes);
            $clientes = count(Cliente::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('created_at', date('m'))->get());
//dd($clientes);
            $inventarios = Inventario::where('IDalmacen', $almacenes->IDalmacen)->get();
            for ($b = 0; $b < sizeof($inventarios); $b++) {
                //dd($inventarios[$a]);
                //dd($inventarios[$a][$b]);
                $articulos[$b] = count(InventarioArticulo::where('IDinventarios', $inventarios[$b]->IDinventarios)->whereMonth('created_at', date('m'))->get());
            }

//dd($articulos,$almacenes);

            $articulos_almacen = 0;
            if (isset($articulos)) {
                for ($e = 0; $e < sizeof($articulos); $e++) {
                    //dd($articulos[$a]);
                    $articulos_almacen = $articulos_almacen + $articulos[$e];
                }
            } else {
                $articulos_almacen = 0;
            }


            $ventas_almacen = Venta::where('IDalmacen', $almacenes->IDalmacen)->latest()->take(5)->groupBy('Factura_Id')->get();


            $total_ventas_almacen = count(Venta::where('IDalmacen', $almacenes->IDAlmacen)->whereMonth('created_at', date('m'))->groupBy('Factura_Id')->get());


            $meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];


            for ($g = 0; $g < sizeof($meses); $g++) {

                $recolecta = Venta::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('Fecha', $meses[$g])->whereYear('Fecha', date('Y'))->get();
                $sumatoria = 0;
                for ($i = 0; $i < sizeof($recolecta); $i++) {
                    $sumatoria = $sumatoria + $recolecta[$i]->Valor_Total;
                }
                $mensual_ventas_almacen[$g] = number_format($sumatoria, 2, '.', '');
            }


            $factura_proveedor = FacturaProveedor::where('IDalmacen', $almacenes->IDalmacen)->latest()->take(5)->get();


            $count_factura_proveedor = count(FacturaProveedor::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('created_at', date('m'))->get());


            $producto_stock = Producto::where('IDalmacen', $almacenes->IDalmacen)->where('AdvertirReposicion', '1')->latest()->take(5)->get();

            return view('home', compact('rol', 'list_almacenes', 'producto_stock', 'count_factura_proveedor', 'factura_proveedor', 'color', 'ventas_almacen', 'total_ventas_almacen', 'almacenes', 'articulos_almacen', 'clientes', 'mensual_ventas_almacen'));
        }else{
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            $color = Color::where('user_id', Auth::User()->id)->first();
            $almacenes = Almacen::where('IDalmacen', $id)->first();
            $list_almacenes = Almacen::all();
            //dd($almacenes);
            $clientes = count(Cliente::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('created_at', date('m'))->get());
//dd($clientes);
            $inventarios = Inventario::where('IDalmacen', $almacenes->IDalmacen)->get();
            for ($b = 0; $b < sizeof($inventarios); $b++) {
                //dd($inventarios[$a]);
                //dd($inventarios[$a][$b]);
                $articulos[$b] = count(InventarioArticulo::where('IDinventarios', $inventarios[$b]->IDinventario)->whereMonth('created_at', date('m'))->get());
            }

//dd($articulos,$almacenes);

            $articulos_almacen = 0;
            if (isset($articulos)) {
                for ($e = 0; $e < sizeof($articulos); $e++) {
                    //dd($articulos[$a]);
                    $articulos_almacen = $articulos_almacen + $articulos[$e];
                }
            } else {
                $articulos_almacen = 0;
            }


            $ventas_almacen = Venta::where('IDalmacen', $almacenes->IDalmacen)->latest()->take(5)->groupBy('Factura_Id')->get();


            $total_ventas_almacen = count(Venta::where('IDalmacen', $almacenes->IDAlmacen)->whereMonth('created_at', date('m'))->groupBy('Factura_Id')->get());


            $meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];


            for ($g = 0; $g < sizeof($meses); $g++) {

                $recolecta = Venta::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('Fecha', $meses[$g])->whereYear('Fecha', date('Y'))->get();
                $sumatoria = 0;
                for ($i = 0; $i < sizeof($recolecta); $i++) {
                    $sumatoria = $sumatoria + $recolecta[$i]->Valor_Total;
                }
                $mensual_ventas_almacen[$g] = number_format($sumatoria, 2, '.', '');
            }


            $factura_proveedor = FacturaProveedor::where('IDalmacen', $almacenes->IDalmacen)->latest()->take(5)->get();


            $count_factura_proveedor = count(FacturaProveedor::where('IDalmacen', $almacenes->IDalmacen)->whereMonth('created_at', date('m'))->get());


            $producto_stock = Producto::where('IDalmacen', $almacenes->IDalmacen)->where('AdvertirReposicion', '1')->latest()->take(5)->get();

            return view('home', compact('rol', 'list_almacenes', 'producto_stock', 'count_factura_proveedor', 'factura_proveedor', 'color', 'ventas_almacen', 'total_ventas_almacen', 'almacenes', 'articulos_almacen', 'clientes', 'mensual_ventas_almacen'));
        }
    }

}
