<?php

namespace App\Http\Controllers;

use App\Almacen;
use App\CategoriaProducto;
use App\Empresa;
use App\Inventario;
use App\InventarioArticulo;
use App\Producto;
use Illuminate\Http\Request;
use App\Provider;
use App\Color;
use Auth;
use App\AccesoRol;
class ProductosController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    
    
    public function c_almacen($id){
        $resultado = array();
     $almacen = Almacen::where('IDpref',$id)->get();
     if(sizeof($almacen) >= 1){
         $resultado['registro'] = 'true';
         for($i = 0; $i < sizeof($almacen); $i++){
         $resultado['resultado'][$i] = $almacen[$i];
             }
             
     }else{
         $resultado['registro'] = 'false';
     }
     return $resultado;
    }
    public function c_inventario($id){
    $inventario = Inventario::where('IDalmacen',$id)->get();
        if(sizeof($inventario) >= 1){
            $resultado['registro'] = 'true';
            for($i = 0; $i < sizeof($inventario); $i++){
                $resultado['resultado'][$i] = $inventario[$i];
            }

        }else{
            $resultado['registro'] = 'false';
        }
        return $resultado;
    }

    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(8,json_decode($rol),false)) {
            if(Auth::User()->rol_id != "1") {
                $color = Color::where('user_id', Auth::User()->id)->first();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $productos = Producto::where('estatus','!=','4')->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
//dd($productos);
                return view('layouts.productos.indexslow', compact('rol', 'productos', 'color'));
            }else{
                $color = Color::where('user_id', Auth::User()->id)->first();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $productos = Producto::where('estatus','!=','4')->get();
//dd($productos);
                return view('layouts.productos.indexslow', compact('rol', 'productos', 'color'));
            }
        }else{
            return redirect()->route('home');
        }

    }
    public function indexslow(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(8,json_decode($rol),false)) {
            if(Auth::User()->rol_id != "1") {
                $color = Color::where('user_id', Auth::User()->id)->first();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $productos = Producto::where('estatus','!=','4')->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
//dd($productos);
                return view('layouts.productos.indexslow', compact('rol', 'productos', 'color'));
            }else{
                $color = Color::where('user_id', Auth::User()->id)->first();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                $productos = Producto::where('estatus','!=','4')->get();
//dd($productos);
                return view('layouts.productos.indexslow', compact('rol', 'productos', 'color'));
            }
        }else{
            return redirect()->route('home');
        }

    }
    public function create(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(9,json_decode($rol),false)) {
            $color = Color::where('user_id', Auth::User()->id)->first();
            $catego = CategoriaProducto::all();
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
                $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                $almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();

            }elseif(Auth::User()->rol_id == "2") {
                $articulo = Producto::where('estatus','!=','4')->where('AlmacenCentral', 1)->where('IDpref',Auth::User()->IDpref)->get();
                $almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
                $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();

            }elseif(Auth::User()->rol_id == "1") {
                $articulo = Producto::where('estatus','!=','4')->get();
                $almacenes = Almacen::where('estatus','!=','4')->get();
                $empresas = Empresa::all();

            }
            return view('layouts.productos.create', compact('almacenes','empresas','rol', 'color', 'catego'));
            //->with('clientes', $clientes);
        }else{
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $existe_cod = Producto::where('Cod', '=', $request->Cod)->first();
        if(Auth::User()->rol_id != "1") {
        if (empty($existe_cod)) {
            $productos = new Producto();
            $productos->IDpref = $request->empresa;
            $productos->IDalmacen = $request->almacen;
            $productos->estatus = "2";
            $productos->IDcategoria = $request->IDcategoria;
            $productos->AlmacenCentral = "1";
            $productos->Articulo = $request->Articulo;
            $productos->Costo = $request->Costo;
            $productos->Cantidad = $request->Cantidad;
            $productos->TipoTrans = $request->TipoTrans;
            $productos->Consulta_Espec_Otro = $request->Consulta_Espec_Otro;
            $productos->AplicaIva = $request->AplicaIva;
            $productos->CtaContable = $request->CtaContable;
            $productos->Codigo_Impuestos = $request->Codigo_Impuestos;
            $productos->FechaDeCompra = $request->FechaDeCompra;
            $productos->Margen = $request->Margen;
            $productos->MinIndisp = $request->MinIndisp;
            $productos->RUC = $request->RUC;
            $productos->Presentacion = $request->Presentacion;
            $productos->Contenido = $request->Contenido;
            $productos->PVP = $request->PVP;
            $productos->Cod = $request->Cod;
            $productos->IDEstante = $request->IDEstante;
            $productos->AdvertirReposicion = $request->AdvertirReposicion;
            $productos->FechaCaducidad = $request->FechaCaducidad;
            $productos->AdvertirCaducidad = $request->AdvertirCaducidad;
            $productos->IDusuarios = $request->IDUser;
            $productos->FechaCierreTransaccion = $request->FechaCierreTransaccion;
            $productos->TransaccionCerrada = $request->TransaccionCerrada;
            $productos->IDCaja = $request->IDCaja;
            $productos->PDMa = $request->PDMa;
            $productos->PDMi = $request->PDMi;
            $productos->PE1 = $request->PE1;
            $productos->PE2 = $request->PE2;
            $productos->IDDescuento = $request->IDDescuento;
            $productos->ProductoElaborado = $request->ProductoElaborado;
            $productos->CodComponenteProductoElaborado= $request->CodComponenteProductoElaborado;
            $productos->Antelacion = $request->Antelacion;
            $productos->AlfaNumericoPosEstante = $request->AlfaNumericoPosEstante;
            $productos->save();


            $inventarios = InventarioArticulo::where('IDarticulo',$productos->IDarticulo)->first();
            if(!empty($inventarios)){
                $inventarios->IDinventarios = $request->inventario;
                $inventarios->IDarticulo =$productos->IDarticulo;
                $inventarios->estatus = "2";
                $inventarios->save();
            }else{
                $inventarios = new InventarioArticulo();
                $inventarios->IDinventarios = $request->inventario;
                $inventarios->IDarticulo = $productos->IDarticulo;
                $inventarios->estatus = "2";
                $inventarios->save();
            }

            //return view('welcome');
            $message = $productos ? 'Se ha registrado el producto correctamente' . $request->Categoria. 'de forma exitosa.' : 'Error al Registrar';

            //Session::flash('message', 'Te has registrado exitosamente ');
            return redirect()->route('producto')->with('message', $message);
        } else {
            $message2 = 'Este producto ya se encuentra registrado';
            return redirect()->route('producto')->with('message2', $message2);
        }
            }else{
            if (empty($existe_cod)) {
                $productos = new Producto();
                $productos->IDpref = Auth::User()->IDpref;
                $productos->IDalmacen = $request->almacen;
                $productos->estatus = "2";
                $productos->IDcategoria = $request->IDcategoria;
                $productos->AlmacenCentral = "1";
                $productos->Articulo = $request->Articulo;
                $productos->Costo = $request->Costo;
                $productos->Cantidad = $request->Cantidad;
                $productos->TipoTrans = $request->TipoTrans;
                $productos->Consulta_Espec_Otro = $request->Consulta_Espec_Otro;
                $productos->AplicaIva = $request->AplicaIva;
                $productos->CtaContable = $request->CtaContable;
                $productos->Codigo_Impuestos = $request->Codigo_Impuestos;
                $productos->FechaDeCompra = $request->FechaDeCompra;
                $productos->Margen = $request->Margen;
                $productos->MinIndisp = $request->MinIndisp;
                $productos->RUC = $request->RUC;
                $productos->Presentacion = $request->Presentacion;
                $productos->Contenido = $request->Contenido;
                $productos->PVP = $request->PVP;
                $productos->Cod = $request->Cod;
                $productos->IDEstante = $request->IDEstante;
                $productos->AdvertirReposicion = $request->AdvertirReposicion;
                $productos->FechaCaducidad = $request->FechaCaducidad;
                $productos->AdvertirCaducidad = $request->AdvertirCaducidad;
                $productos->IDusuarios = $request->IDUser;
                $productos->FechaCierreTransaccion = $request->FechaCierreTransaccion;
                $productos->TransaccionCerrada = $request->TransaccionCerrada;
                $productos->IDCaja = $request->IDCaja;
                $productos->PDMa = $request->PDMa;
                $productos->PDMi = $request->PDMi;
                $productos->PE1 = $request->PE1;
                $productos->PE2 = $request->PE2;
                $productos->IDDescuento = $request->IDDescuento;
                $productos->ProductoElaborado = $request->ProductoElaborado;
                $productos->CodComponenteProductoElaborado= $request->CodComponenteProductoElaborado;
                $productos->Antelacion = $request->Antelacion;
                $productos->AlfaNumericoPosEstante = $request->AlfaNumericoPosEstante;
                $productos->save();
                $inventario = new InventarioArticulo();
                $inventario->IDarticulo = $productos->IDarticulo;
                $inventario->IDinventarios = $request->inventario;
                $inventario->estatus = "2";
                $inventario->save();

                $inventarios = InventarioArticulo::where('IDarticulo',$productos->IDarticulo)->first();
                if(!empty($inventarios)){
                    $inventarios->IDinventarios = $request->inventario;
                    $inventarios->IDarticulo =$productos->IDarticulo;
                    $inventarios->estatus = "2";
                    $inventarios->save();
                }else{
                    $inventarios = new InventarioArticulo();
                    $inventarios->IDinventarios = $request->inventario;
                    $inventarios->IDarticulo = $productos->IDarticulo;
                    $inventarios->estatus = "2";
                    $inventarios->save();
                }
                //return view('welcome');
                $message = $productos ? 'Se ha registrado el prodcuto correctamente' . $request->Categoria. 'de forma exitosa.' : 'Error al Registrar';

                //Session::flash('message', 'Te has registrado exitosamente ');
                return redirect()->route('producto')->with('message', $message);
            } else {
                $message2 = 'Este producto ya se encuentra registrado';
                return redirect()->route('producto')->with('message2', $message2);
            }

        }
    }


    public function edit($id){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        $productos = Producto::where('IDarticulo','=', $id)->first();
        $color = Color::where('user_id', Auth::User()->id)->first();
        $catego = CategoriaProducto::all();
        $empresas =  Empresa::all();
        $almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->get();
        $inventarioss = InventarioArticulo::where('IDarticulo',$id)->first();
        //dd($inventarioss);
        return view('layouts.productos.edit',compact('inventarioss','almacenes','empresas','rol','productos','color','catego'));
    }
    public function update(Request $request, $id){
        if(Auth::User()->rol_id != "1") {
            $productos = Producto::where('IDarticulo', '=', $id)->first();
            $productos->IDpref = $request->empresa;
            $productos->IDalmacen = $request->almacen;
            $productos->estatus = "3";
            $productos->IDcategoria = $request->IDcategoria;
            $productos->Articulo = $request->Articulo;
            $productos->Costo = $request->Costo;
            $productos->Cantidad = $request->Cantidad;
            $productos->TipoTrans = $request->TipoTrans;
            $productos->Consulta_Espec_Otro = $request->Consulta_Espec_Otro;
            $productos->AplicaIva = $request->AplicaIva;
            $productos->CtaContable = $request->CtaContable;
            $productos->Codigo_Impuestos = $request->Codigo_Impuestos;
            $productos->FechaDeCompra = $request->FechaDeCompra;
            $productos->Margen = $request->Margen;
            $productos->MinIndisp = $request->MinIndisp;
            $productos->RUC = $request->RUC;
            $productos->Presentacion = $request->Presentacion;
            $productos->Contenido = $request->Contenido;
            $productos->PVP = $request->PVP;
            $productos->Cod = $request->Cod;
            $productos->IDEstante = $request->IDEstante;
            $productos->AdvertirReposicion = $request->AdvertirReposicion;
            $productos->FechaCaducidad = $request->FechaCaducidad;
            $productos->AdvertirCaducidad = $request->AdvertirCaducidad;
            $productos->IDusuarios = $request->IDUser;
            $productos->FechaCierreTransaccion = $request->FechaCierreTransaccion;
            $productos->TransaccionCerrada = $request->TransaccionCerrada;
            $productos->IDCaja = $request->IDCaja;
            $productos->PDMa = $request->PDMa;
            $productos->PDMi = $request->PDMi;
            $productos->PE1 = $request->PE1;
            $productos->PE2 = $request->PE2;
            $productos->IDDescuento = $request->IDDescuento;
            $productos->ProductoElaborado = $request->ProductoElaborado;
            $productos->CodComponenteProductoElaborado = $request->CodComponenteProductoElaborado;
            $productos->Antelacion = $request->Antelacion;
            $productos->AlfaNumericoPosEstante = $request->AlfaNumericoPosEstante;
            $productos->save();
            $inventarios = InventarioArticulo::where('IDarticulo',$productos->IDarticulo)->first();
            if(!empty($inventarios)){
                $inventarios->IDinventarios = $request->inventario;
                $inventarios->IDarticulo =$productos->IDarticulo;
                $inventarios->estatus = "3";
                $inventarios->save();
            }else{
                $inventarios = new InventarioArticulo();
                $inventarios->IDinventarios = $request->inventario;
                $inventarios->IDarticulo = $productos->IDarticulo;
                $inventarios->estatus = "3";
                $inventarios->save();
            }

            $message = $productos ? 'Se ha actualizado el registro' . $request->Categoria . 'de forma exitosa.' : 'Error al actualizar';
            return redirect()->route('producto')->with('message', $message);
        }else{
            $productos = Producto::where('IDarticulo', '=', $id)->first();
            $productos->IDpref = Auth::User()->IDpref;
            $productos->IDalmacen = $request->almacen;
            $productos->estatus = "3";
            $productos->IDcategoria = $request->IDcategoria;
            $productos->Articulo = $request->Articulo;
            $productos->Costo = $request->Costo;
            $productos->Cantidad = $request->Cantidad;
            $productos->TipoTrans = $request->TipoTrans;
            $productos->Consulta_Espec_Otro = $request->Consulta_Espec_Otro;
            $productos->AplicaIva = $request->AplicaIva;
            $productos->CtaContable = $request->CtaContable;
            $productos->Codigo_Impuestos = $request->Codigo_Impuestos;
            $productos->FechaDeCompra = $request->FechaDeCompra;
            $productos->Margen = $request->Margen;
            $productos->MinIndisp = $request->MinIndisp;
            $productos->RUC = $request->RUC;
            $productos->Presentacion = $request->Presentacion;
            $productos->Contenido = $request->Contenido;
            $productos->PVP = $request->PVP;
            $productos->Cod = $request->Cod;
            $productos->IDEstante = $request->IDEstante;
            $productos->AdvertirReposicion = $request->AdvertirReposicion;
            $productos->FechaCaducidad = $request->FechaCaducidad;
            $productos->AdvertirCaducidad = $request->AdvertirCaducidad;
            $productos->IDusuarios = $request->IDUser;
            $productos->FechaCierreTransaccion = $request->FechaCierreTransaccion;
            $productos->TransaccionCerrada = $request->TransaccionCerrada;
            $productos->IDCaja = $request->IDCaja;
            $productos->PDMa = $request->PDMa;
            $productos->PDMi = $request->PDMi;
            $productos->PE1 = $request->PE1;
            $productos->PE2 = $request->PE2;
            $productos->IDDescuento = $request->IDDescuento;
            $productos->ProductoElaborado = $request->ProductoElaborado;
            $productos->CodComponenteProductoElaborado = $request->CodComponenteProductoElaborado;
            $productos->Antelacion = $request->Antelacion;
            $productos->AlfaNumericoPosEstante = $request->AlfaNumericoPosEstante;
            $productos->save();
            $inventarios = InventarioArticulo::where('IDarticulo',$productos->IDarticulo)->first();
            if(!empty($inventarios)){
                $inventarios->IDinventarios = $request->inventario;
                $inventarios->IDarticulo =$productos->IDarticulo;
                $inventarios->estatus = "3";
                $inventarios->save();
            }else{
                $inventarios = new InventarioArticulo();
                $inventarios->IDinventarios = $request->inventario;
                $inventarios->IDarticulo = $productos->IDarticulo;
                $inventarios->estatus = "3";
                $inventarios->save();
            }

            $message = $productos ? 'Se ha actualizado el registro' . $request->Categoria . 'de forma exitosa.' : 'Error al actualizar';
            return redirect()->route('producto')->with('message', $message);
        }
    }


    public function destroy($id){

        $productos = Producto::where('IDarticulo',$id);
        $productos->estatus = "4";
        $productos->save();
        $message =  $productos?'Registro eliminado correctamente' : 'Error al Eliminar';
        return redirect()->route('prodcuto')->with('message', $message);

    }
}
