<?php

namespace App\Http\Controllers;

use App\Administradores;
use App\Empresa;
use App\Inventario;
use App\User;
use Illuminate\Http\Request;
use App\Almacen;
use App\Mensajes;
use App\Color;
use Auth;
use App\AccesoRol;
class MensajesController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    public function contador()
    {
        $mensajes = count(Mensajes::where('IDreceptor', Auth::User()->IDusuario)->where('Estatus','1')->get());
        return $mensajes;
    }
 
    public function recibidos(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        $color = Color::where('user_id', Auth::User()->id)->first();
        $mensajes = Mensajes::where('IDreceptor', Auth::User()->IDusuario)->get();
        return view('layouts.mensajes.recibidos',compact('rol','mensajes','color'));
    }

    public function enviados(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        $color = Color::where('user_id', Auth::User()->id)->first();
        $mensajes = Mensajes::where('IDemisor', Auth::User()->IDusuario)->get();
        return view('layouts.mensajes.enviados',compact('rol','mensajes','color'));
    }

    public function leer_recibido($id){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        $color = Color::where('user_id', Auth::User()->id)->first();
        $mensajes = Mensajes::where('IDmensajes', $id)->where('IDreceptor', Auth::User()->IDusuario)->first();
        //dd($mensajes);
        if(!empty($mensajes)){
        $leer = Mensajes::where('IDmensajes', $id)->where('IDreceptor', Auth::User()->IDusuario)->first();
        $leer->Estatus = 2;
        $leer->save();
            return view('layouts.mensajes.leer',compact('rol','mensajes','color'));
        }else {
            return redirect()->back();
        }
    }
    public function leer_enviado($id){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        $color = Color::where('user_id', Auth::User()->id)->first();
        $mensajes = Mensajes::where('IDmensajes', $id)->where('IDemisor', Auth::User()->IDusuario)->first();
        return view('layouts.mensajes.leer',compact('rol','mensajes','color'));
    }




    public function create(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        $color = Color::where('user_id', Auth::User()->id)->first();
        if(Auth::User()->rol_id == 1){
            $usuarios = User::all();
        }else{
        $usuarios = User::where('IDpref', Auth::User()->IDpref)->get();
        }
        return view('layouts.mensajes.create',compact('rol', 'usuarios','color'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        for($i = 0; $i < sizeof($request->IDreceptor); $i++){
        $mensajes = new Mensajes();
        $mensajes->Asunto = $request->Asunto;
        $mensajes->Mensaje = $request->Mensaje;
        $mensajes->IDemisor = Auth::User()->IDusuario;
        $mensajes->IDreceptor = $request->IDreceptor[$i];
        $mensajes->Estatus = 1;
        $mensajes->save();
        }

        return redirect()->route('mensajes-enviados');
    }


    public function edit($id){

    }

    public function update(Request $request, $id){


    }


    public function destroy($id){


    }
}
