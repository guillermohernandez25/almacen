<?php

namespace App\Http\Controllers;

use App\Almacen;
use App\CategoriaProducto;
use App\Empresa;
use Illuminate\Http\Request;
use App\Provider;
use App\Color;
use Auth;
use App\AccesoRol;
class CategoriaProductosController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
 
    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(14,json_decode($rol),false)) {
            $color = Color::where('user_id', Auth::User()->id)->first();
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            $categoria = CategoriaProducto::all();


            return view('layouts.categoriaproductos.index', compact('empresas','almacenes','categoria', 'color', 'rol'));
        }else{
            return redirect()->route('home');
        }

    }
    public function create(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(15,json_decode($rol),false)) {
            $color = Color::where('user_id', Auth::User()->id)->first();
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
                $almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();
            }elseif(Auth::User()->rol_id == "2") {
                $almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
                $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();
            }elseif(Auth::User()->rol_id == "1") {
                $almacenes = Almacen::where('estatus','!=','4')->get();
                $empresas = Empresa::all();
            }
            return view('layouts.categoriaproductos.create', compact('empresas','almacenes','color', 'rol'));
            //->with('clientes', $clientes);
        }else{
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $existe_categoria = CategoriaProducto::where('Categoria', '=', $request->Categoria)->first();
        if (empty($existe_categoria)) {


                $categoria = new CategoriaProducto();

                $categoria->Categoria = $request->Categoria;
                $categoria->estatus = "2";
                /*$categoria->TipoCuenta = $request->TipoCuenta;
                $categoria->TipoTrans = $request->TipoTrans;*/
                $categoria->CtaContable = $request->CtaContable;
                $categoria->IDpref = $request->IDpref;
            $categoria->IDalmacen = $request->IDalmacen;
                $categoria->estatus = "2";
                /*$categoria->CategoriaFacturable = $request->CategoriaFacturable;
                $categoria->IDDescuento = $request->IDDescuento;
                $categoria->ProductoElaborado = $request->ProductoElaborado;
                $categoria->DebitarDelTotal = $request->DebitarDelTotal;*/

                $categoria->save();
                //return view('welcome');
                $message = $categoria ? 'Se ha registrado la Categoria correctamente' . $request->Categoria. 'de forma exitosa.' : 'Error al Registrar';

                //Session::flash('message', 'Te has registrado exitosamente ');
                return redirect()->route('categoriaproducto')->with('message', $message);
        } else {
            $message2 = 'Esta Categoria ya se encuentra registrado';
            return redirect()->route('categoriaproducto')->with('message2', $message2);
        }
    }


    public function edit($id){
        $categoria = CategoriaProducto::where('IDCategoria','=', $id)->first();
        $color = Color::where('user_id', Auth::User()->id)->first();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
            $almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
            $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();
        }elseif(Auth::User()->rol_id == "2") {
            $almacenes = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
            $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();
        }elseif(Auth::User()->rol_id == "1") {
            $almacenes = Almacen::where('estatus','!=','4')->get();
            $empresas = Empresa::all();
        }
        return view('layouts.categoriaproductos.edit',compact('empresas','almacenes','categoria','color','rol'));
    }
    public function update(Request $request, $id){

        $categoria= CategoriaProducto::where('IDCategoria','=', $id)->first();
        $categoria->Categoria = $request->Categoria;
        $categoria->estatus = "3";
        /*$categoria->TipoCuenta = $request->TipoCuenta;
        $categoria->TipoTrans = $request->TipoTrans;*/
        $categoria->CtaContable = $request->CtaContable;
        $categoria->IDpref = $request->IDpref;
        $categoria->IDalmacen = $request->IDalmacen;
        $categoria->estatus = "3";
        /*$categoria->CategoriaFacturable = $request->CategoriaFacturable;
        $categoria->IDDescuento = $request->IDDescuento;
        $categoria->ProductoElaborado = $request->ProductoElaborado;
        $categoria->DebitarDelTotal = $request->DebitarDelTotal;*/
        $categoria->save();

        $message = $categoria?'Se ha actualizado el registro'. $request->Categoria.'de forma exitosa.' : 'Error al actualizar';
        return redirect()->route('categoriaproducto')->with('message', $message);

    }

    public function destroy($id){

        $categoria = CategoriaProducto::where('IDCategoria',$id);
        $categoria->estatus = "4";
        $categoria->save();
        $message =  $categoria?'Registro eliminado correctamente' : 'Error al Eliminar';
            return redirect()->route('categoriaproducto')->with('message', $message);

    }
}
