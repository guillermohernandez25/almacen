<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\perfil;
use App\Color;
use Auth;
use App\AccesoRol;
class PerfilController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
 
    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        $color = Color::where('user_id', Auth::User()->IDusuario)->first();
        $usuario = User::where('estatus','!=','4')->where('IDusuario' ,Auth::User()->IDusuario)->first();
        return view('layouts.perfil.index', compact('rol','usuario','color'));
    }

    public function create(){
        $color = Color::where('user_id', Auth::User()->IDusuario)->first();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');

        return view('layouts.perfil.create', compact('rol','color'));
        //->with('clientes', $clientes);
    }
    public function updateimage(Request $request, $id){
//dd($request);
        $user = User::where('IDusuario','=', Auth::User()->IDusuario)->first();
        $destination =  public_path() .'/uploads/usuarios/';
        $image = $request->file('foto_cargada');

        $random = str_random(6);
        if (!empty($image)) {
            $filename = $random.$image->getClientOriginalName();
            $image->move($destination, $filename);
            $user->foto = $filename;
        }
        $user->save();

        $message = $user?'Se ha actualizado el registro de forma exitosa.' : 'Error al actualizar';
        return redirect()->route('perfil')->with('message', $message);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $existe_ruc = perfil::where('ruc', '=', $request->ruc)->first();
        if (empty($existe_ruc)) {


                $proveedor = new perfil();

                $proveedor->ruc = $request->ruc;
                $proveedor->razonsocial = $request->razonsocial;
                $proveedor->direccion = $request->direccion;
                $proveedor->telfprincipal = $request->telfprincipal;
                $proveedor->notas = $request->notas;
                $proveedor->email = $request->email;
                $proveedor->vendedorcontacto = $request->vendedorcontacto;
                $proveedor->tlfvendedorcontacto = $request->tlfvendedorcontacto;
                $proveedor->comentario = $request->comentario;
                $proveedor->ctacontable = $request->ctacontable;
                $proveedor->nombrebanco1 = $request->nombrebanco1;
                $proveedor->numctacorriente1 = $request->numctacorriente1;
                $proveedor->numctaahorros1 = $request->numctaahorros1;
                $proveedor->nombrebanco2 = $request->nombrebanco2;
                $proveedor->numctacorriente2 = $request->numctacorriente2;
                $proveedor->numctaahorros2 = $request->numctaahorros2;

                $proveedor->save();
                //return view('welcome');
                $message = $proveedor ? 'Se ha registrado el Proveedor' . $request->proveedor. 'de forma exitosa.' : 'Error al Registrar';

                //Session::flash('message', 'Te has registrado exitosamente ');
                return redirect()->route('proveedor')->with('message', $message);
        } else {
            $message2 = 'Este Proveedor ya se encuentra registrado';
            return redirect()->route('proveedor')->with('message2', $message2);
        }
    }


    public function edit(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        $usuario = User::where('IDusuario','=', Auth::User()->IDusuario)->first();
        $color = Color::where('user_id', Auth::User()->IDusuario)->first();
        return view('layouts.perfil.edit',compact('rol','usuario','color'));
    }
    public function update(Request $request){

        //dd($request);
        $user = User::where('IDusuario','=', Auth::User()->IDusuario)->first();

        $user->Nombres = $request->Nombres;
        $user->Apellidos = $request->Apellidos;
       $user->CedulaRUC = $request->CedulaRUC;
       $user->FechaNacimiento = $request->FechaNacimiento;
       $user->Edad = $request->Edad;
       $user->Genero = $request->Genero;
       $user->EstadoCivil = $request->EstadoCivil;
       $user->TelfCasa = $request->TelfCasa;
       $user->TelfCelular = $request->TelfCelular;
       $user->Conyuge = $request->Conyuge;
       $user->Direccion = $request->Direccion;
       $user->ProfesionOficio = $request->ProfesionOficio;
       $user->NumCargasFamiliares = $request->NumCargasFamiliares;
       $user->NumHijos = $request->NumHijos;
       $user->Comentarios = $request->Comentarios;
       $user->IDEmpresa = $request->IDEmpresa;
       $user->FechaContratacion = $request->FechaContratacion;
       $user->Cargos_IDCargo = $request->Cargos_IDCargo;
       $user->Salarios_IDSalario = $request->Salarios_IDSalario;
       $user->email = $request->email;
        $user->EmailPersonal = $request->email;
       $user->EmailEmpresarial = $request->EmailEmpresarial;
        
        if(!empty($request->password)){
            if($request->password == $request->confpassword) {
                $user->password = bcrypt($request->password);
                $user->save();
                $message = $user?'Se ha actualizado el registro'. $request->name.'de forma exitosa.' : 'Error al actualizar';
                return redirect()->route('perfil')->with('message', $message);
            }else{
                $message2 = 'El Password no es igual en los dos campos';
                return redirect()->back()->with('message2', $message2);
            }

        }else{
            $user->save();
            $message = $user?'Se ha actualizado el registro'. $request->name.'de forma exitosa.' : 'Error al actualizar';
            return redirect()->route('perfil')->with('message', $message);
        }

    }


    public function destroy($id){

        $proveedor = perfil::find($id);
        $proveedor->delete($id);
        $message =  $proveedor?'Registro eliminado correctamente' : 'Error al Eliminar';
            return redirect()->route('proveedor')->with('message', $message);

    }
}
