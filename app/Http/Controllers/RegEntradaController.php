<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KardexinRequest;
use Illuminate\Support\Facades\Auth;
use App\Movimiento;
use App\Producto;
use Carbon\Carbon;
use DB;
use Session;
use Toastr;

use App\AccesoRol;

class RegEntradaController extends Controller
{
    private $stock = 0;

    public function __construct(){
        $this->middleware('auth');
    	Carbon::setLocale('es');
    	$stock = 0;
    }

    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
    	$articulo = Producto::where('estatus','!=','4')
				->orderBy('Cod','DESC')
				->select(DB::raw('CONCAT(Articulo," - ",Cod) AS Articulo'),'IDarticulo')
				->pluck('Articulo','IDarticulo');
		return view('layouts.entrada.create',compact('rol','articulo'));
    }

    public function store(KardexinRequest $request){
		try{
			$this->validarStock($request->input('mov_entrada'),$request->input('IDarticulo'));
			$articulo = Producto::where('IDarticulo',$request->input('IDarticulo'))->first();
			//$empresa_almacen  = Producto::where('IDarticulo',$request->input('IDarticulo'))->first();

			$salida = new Movimiento();
			$salida->IDarticulo = $request->input('IDarticulo');
			$salida->IDpref = Auth::User()->IDpref;
            $salida->IDcategoria = $articulo->IDcategoria;
			$salida->IDalmacen = Auth::User()->IDalmacen;
			$salida->IDclientes = 1;
			$salida->Mov_factura = 0;
			$salida->Mov_entrada = $request->input('mov_entrada');
			$salida->Mov_salida = 0;
			$salida->Mov_total = $this->stock;
			$salida->IDregistra = Auth::user()->IDusuario;
			$salida->IDactualiza = Auth::user()->IDusuario;
			$salida->Mov_estado = 1;
			$salida->Mov_obs = $request->input('mov_obs');
			$salida->save();
			$message= $salida ? 'Se ha adicionado a kardex la entrada del articulo':'Registro Correcto';
		}catch(\Exception $ex){
            $message= $salida ? 'Ocurrio el siguiente error: ':'Error';

		}
		return redirect()->route('kardexin.index', compact('message'));
    }
    
    public function validarStock($cantidad, $id){
		try{
			$mov = Movimiento::where('IDarticulo','=',$id)
							->orderBy('created_at','DESC')
							->first();
			if(!is_null($mov)){
				$this->stock = $cantidad + $mov->Mov_total;
			}
			else{
				$this->stock = $cantidad;
			}
		}catch(\Exception $ex){
			Toastr::error('Ocurrio el siguiente error: '.$ex->getMessage(),'Error');
		}
	}
}
