<?php

namespace App\Http\Controllers;

use App\Almacen;
use App\Empresa;
use Illuminate\Http\Request;
use App\Cliente;
use App\Color;
use Auth;
use App\AccesoRol;
class ClientesController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(){
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if(in_array(16,json_decode($rol),false)) {
            if(Auth::User()->rol_id != "1") {
            $color = Color::where('user_id', Auth::User()->id)->first();
            $clientes = Cliente::where('estatus','!=','4')->where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->get();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            return view('layouts.clientes.indexcompact', compact('clientes', 'color', 'rol'));
                }else{
                $color = Color::where('user_id', Auth::User()->id)->first();
                $clientes = Cliente::where('estatus','!=','4')->get();
                $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
                return view('layouts.clientes.index', compact('clientes', 'color', 'rol'));
            }
        }else{
            return redirect()->route('home');
        }

    }

    public function clientes_almacen($id){

        $color = Color::where('user_id', Auth::User()->id)->first();
        $clientes = Cliente::where('IDalmacen',$id)->where('estatus','!=','4')->get();
        $almacen = Almacen::where('estatus','!=','4')->where('IDalmacen',$id)->first();
        $empresas = Empresa::where('estatus','!=','4')->get();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        return view('layouts.clientes.indexcompact', compact('empresas','clientes','color','almacen','rol'));

    }
    public function create()
    {
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        if (in_array(17, json_decode($rol), false)) {

            $color = Color::where('user_id', Auth::User()->id)->first();
            if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
                $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();
            }elseif(Auth::User()->rol_id == "2") {
                $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
                $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();
            }elseif(Auth::User()->rol_id == "1") {
                $almacen = Almacen::where('estatus','!=','4')->get();
                $empresas = Empresa::all();
            }
            $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
            return view('layouts.clientes.create', compact('empresas','color', 'almacen', 'rol'));
            //->with('clientes', $clientes);
        }else{
            return redirect()->route('home');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $last_id = Cliente::orderBy('IDclientes','desc')->first();
        //dd($last_id);
        $existe_CedulaRUC = Cliente::where('CedulaRUC', '=', $request->CedulaRUC)->first();
        if (empty($existe_CedulaRUC)) {


            $clientes = new Cliente();
            $clientes->IDclientes = ($last_id->IDclientes + 1);
            $clientes->IDalmacen = $request->IDalmacen;
            $clientes->IDpref = $request->IDpref;
            $clientes->estatus = "2";
            $clientes->Apellidos = $request->Apellidos;
            $clientes->Nombres = $request->Nombres;
            $clientes->CedulaRUC = $request->CedulaRUC;
            $clientes->TelefonoDom = $request->TelefonoDom;
            $clientes->TelefonoTrabajo = $request->TelefonoTrabajo;
            $clientes->TelefonoMovil = $request->TelefonoMovil;
            $clientes->NumFamiliares = $request->NumFamiliares;
            $clientes->NumHijos = $request->NumHijos;
            $clientes->LugarTrabajo = $request->LugarTrabajo;
            $clientes->email = $request->email;
            $clientes->Precio = $request->Precio;
            $clientes->Descuento = $request->Descuento;
            $clientes->Referencia = $request->Referencia;
            $clientes->FechaInscripcion = $request->FechaInscripcion;
            $clientes->FechaNacimiento = $request->FechaNacimiento;
            $clientes->Ocupacion = $request->Ocupacion;
            $clientes->EstadoCivil = $request->EstadoCivil;
            $clientes->Genero = $request->Genero;
            $clientes->Edad = $request->Edad;
            $clientes->ValorLimiteCredito = $request->ValorLimiteCredito;
            $clientes->DireccionOficina = $request->DireccionOficina;
            $clientes->CtaContable = $request->CtaContable;
            $clientes->Notas = $request->Notas;


            $destination =  public_path() .'/uploads/clientes/';
            $image = $request->file('Foto');

            $random = str_random(6);
            if (!empty($image)) {
                $filename = $random.$image->getClientOriginalName();
                $image->move($destination, $filename);
                $clientes->Foto = $filename;
            }else{
                $clientes->Foto = "user.jpg";
            }

            $clientes->save();
            //return view('welcome');
            $message = $clientes ? 'Se ha registrado el administrador ' . $request->dni. ' de forma exitosa.' : 'Error al Registrar';
//dd($clientes->IDclientes);
            //Session::flash('message', 'Te has registrado exitosamente ');
            return redirect()->route('clientes')->with('message', $message);

        } else {
            $message2 = 'Este cliente ya se encuentra registrado';
            return redirect()->route('clientes')->with('message2', $message2);
        }
    }


    public function edit($id){
        $clientes = Cliente::where('IDclientes','=', $id)->first();
        //dd($clientes);
        if(Auth::User()->rol_id != "1" and Auth::User()->rol_id != "2" ) {
            $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('IDalmacen',Auth::User()->IDalmacen)->where('estatus','!=','4')->get();
            $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();
        }elseif(Auth::User()->rol_id == "2") {
            $almacen = Almacen::where('IDpref',Auth::User()->IDpref)->where('estatus','!=','4')->get();
            $empresas = Empresa::where('IDpref',Auth::User()->IDpref)->get();
        }elseif(Auth::User()->rol_id == "1") {
            $almacen = Almacen::where('estatus','!=','4')->get();
            $empresas = Empresa::all();
        }
        $color = Color::where('user_id', Auth::User()->id)->first();
        $rol = AccesoRol::where('rol_id', Auth::User()->rol_id)->where('IDpref', Auth::User()->IDpref)->pluck('panel_id');
        return view('layouts.clientes.edit',compact('empresas','clientes','color','almacen','rol'));
    }
    public function update(Request $request, $id){

        $clientes= Cliente::where('IDclientes','=', $id)->first();
        $clientes->IDalmacen = $request->IDalmacen;
        $clientes->IDpref = $request->IDpref;
        $clientes->estatus = "3";
        $clientes->Apellidos = $request->Apellidos;
        $clientes->Nombres = $request->Nombres;
        $clientes->CedulaRUC = $request->CedulaRUC;
        $clientes->TelefonoDom = $request->TelefonoDom;
        $clientes->TelefonoTrabajo = $request->TelefonoTrabajo;
        $clientes->TelefonoMovil = $request->TelefonoMovil;
        $clientes->NumFamiliares = $request->NumFamiliares;
        $clientes->NumHijos = $request->NumHijos;
        $clientes->LugarTrabajo = $request->LugarTrabajo;
        $clientes->email = $request->email;
        $clientes->Precio = $request->Precio;
        $clientes->Descuento = $request->Descuento;
        $clientes->Referencia = $request->Referencia;
        $clientes->FechaInscripcion = $request->FechaInscripcion;
        $clientes->FechaNacimiento = $request->FechaNacimiento;
        $clientes->Ocupacion = $request->Ocupacion;
        $clientes->EstadoCivil = $request->EstadoCivil;
        $clientes->Genero = $request->Genero;
        $clientes->Edad = $request->Edad;
        $clientes->ValorLimiteCredito = $request->ValorLimiteCredito;
        $clientes->DireccionOficina = $request->DireccionOficina;
        $clientes->CtaContable = $request->CtaContable;
        $clientes->Notas = $request->Notas;

        $destination =  public_path() .'/uploads/clientes/';
        $image = $request->file('Foto');

        $random = str_random(6);
        if (!empty($image)) {
            $filename = $random.$image->getClientOriginalName();
            $image->move($destination, $filename);
            $clientes->Foto = $filename;
        }else{

        }
        $clientes->save();

        $message = $clientes?'Se ha actualizado el registro'. $request->clientes.'de forma exitosa.' : 'Error al actualizar';
        return redirect()->route('clientes')->with('message', $message);


    }


    public function destroy($id){

        $clientes = Cliente::find($id);
        $clientes->estatus = "4";
        $clientes->save();
        $message =  $clientes?'Registro eliminado correctamente' : 'Error al Eliminar';
        return redirect()->route('clientes')->with('message', $message);

    }
}
