<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Modulos extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $primaryKey = 'IDAlmacen';
    protected $table = 'modulos_sistema';
    protected $fillable = [
    ];


   /** public function administradores(){
        return $this->belongsTo('App\Administradores','administrador_id');
    }**/
}
