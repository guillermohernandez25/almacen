<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Venta extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'facturacion';
    protected $primaryKey = 'IDfacturacion';
    protected $fillable = [
        'IDAlamcen',
        'Factura_Id',
        'Cantidad',
        'Descripcion',
        'Valor_Unitario',
        'Valor_Total',
        'Anulada',
        'Fecha',
        'Hora',
        'Cod',
        'NumCaja',
        'FactAbierta',
        'Terminos',
        'IDCliente',
        'Referencia',
        'IVA',
        'TipoTrans',
        'TipoFactura',
        'IVA_Luego_Descuento',
        'IDPrecio',
        'Distribuidor',
        'VTotal_luego_descuento',
        'TipoDeComprobante',
        'FormaDePago',
        'NotaVenta',

    ];

    public function almacen(){
        return $this->belongsTo('App\Almacen','IDalmacen');
    }
    public function clientes(){
        return $this->belongsTo('App\Cliente','IDcliente');
    }


}
