<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Producto extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'articulo';
    protected $primaryKey = 'IDarticulo';
    protected $fillable = [


    ];

    public function empresas(){
        return $this->belongsTo('App\Empresa','IDpref');
    }

    public function categorias(){
        return $this->belongsTo('App\CategoriaProducto','IDcategoria');
    }


    public function almacenes(){
        return $this->belongsTo('App\Almacen','IDalmacen');
    }


    }
