<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Provider extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'IDproveedores';
    protected $table = 'proveedores';
    protected $fillable = [
        

    ];
    public function almacenes(){
        return $this->belongsTo('App\Almacen','IDalmacen');
    }


    }
