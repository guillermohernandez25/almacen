<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'IDusuario';

    protected $fillable = [
        'name' ,'lastname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function empresas(){
        return $this->belongsTo('App\Empresa','IDpref', 'IDpref');
    }

    public function almacenes(){
        return $this->belongsTo('App\Almacen','IDalmacen');
    }

    public function horarios(){
        return $this->belongsTo('App\Horarios','IDhorario');
    }

    public function cargos(){
        return $this->belongsTo('App\Cargos','Cargos_IDCargo');
    }

    public function salarios(){
        return $this->belongsTo('App\Salarios','Salarios_IDSalario');
    }
    public function roles(){
        return $this->belongsTo('App\UserRol','rol_id');
    }


}
