<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{
    protected $table = 'movimientos';
    protected $primaryKey = 'IDmovimientos';
    protected $fillable = ['id','IDarticulo','IDcliente','Mov_factura','Mov_entrada','Mov_salida','Mov_total','IDregistra','IDactualiza','Mov_estado','Mov_obs'];

    public function userRegistra(){
    	return $this->belongsTo('App\User','IDRegistra','IDusuario');
    }

    public function userActualiza(){
    	return $this->belongsTo('App\User','IDActualiza','IDusuario');
    }

    public function articulos(){
    	return $this->belongsTo('App\Producto','IDarticulo','IDarticulo');
    }

    public function categorias(){
        return $this->belongsTo('App\CategoriaProducto','IDcategoria','IDcategoria');
    }
    public function hclientes(){
    	return $this->belongsTo('App\Cliente','IDclientes','IDclientes');
    }
    public function empresa(){
        return $this->belongsTo('App\Empresa','IDpref','IDpref');
    }
    public function almacen(){
        return $this->belongsTo('App\Almacen','IDalmacen','IDalmacen');
    }
}
