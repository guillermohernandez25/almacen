<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Mensajes extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'IDmensajes';
    protected $table = 'mensajes';
    protected $fillable = [
        'IDemisor',
        'Mensaje',
        'IDreceptor'
    ];

    public function emisor(){
        return $this->belongsTo('App\User','IDemisor' , 'IDusuario');
    }
    public function receptor(){
        return $this->belongsTo('App\User','IDreceptor' , 'IDusuario');
    }


}
