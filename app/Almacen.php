<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Almacen extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'IDalmacen';
    protected $table = 'almacen';
    protected $fillable = [


    ];


    public function administradores(){
        return $this->belongsTo('App\Administradores','IDadministradores');
    }
    public function empresa(){
        return $this->belongsTo('App\Empresa','IDpref');
    }
}
