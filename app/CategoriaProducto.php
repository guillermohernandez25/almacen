<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class CategoriaProducto extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'categoria';
    protected $primaryKey = 'IDcategoria';
    protected $fillable = [


    ];
    public function empresa(){
        return $this->belongsTo('App\Empresa','IDpref');
    }
    public function almacen(){
        return $this->belongsTo('App\Almacen','IDalmacen');
    }


    }
