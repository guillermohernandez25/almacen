<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Cliente extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'clientes';
    protected $primaryKey = 'IDclientes';
    protected $fillable = [


    ];
    public function almacenes(){
        return $this->belongsTo('App\Almacen','IDalmacen');
    }

    public function empresas(){
        return $this->belongsTo('App\Empresa','IDpref');
    }

}
