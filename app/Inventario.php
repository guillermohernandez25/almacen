<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Inventario extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'inventarios';
    protected $primaryKey = 'IDinventarios';
    protected $fillable = [


    ];

    public function almacenes(){
        return $this->belongsTo('App\Almacen','IDalmacen');
    }

    public function productos(){
        return $this->belongsTo('App\Producto','IDarticulo');
    }

    }
