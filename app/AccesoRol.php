<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AccesoRol extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'IDacceso_rol';
    protected $table = 'acceso_rol';
    protected $fillable = [
    ];


   /** public function administradores(){
        return $this->belongsTo('App\Administradores','administrador_id');
    }**/

    public function modulos(){
        return $this->belongsTo('App\Modulos','panel_id', 'IDmodulos_sistema');
    }
}
