-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.3.10-MariaDB-log - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para dbalmacenv2
CREATE DATABASE IF NOT EXISTS `dbalmacenv2` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `dbalmacenv2`;

-- Volcando estructura para tabla dbalmacenv2.abonos
CREATE TABLE IF NOT EXISTS `abonos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDAbono` int(11) NOT NULL DEFAULT 0,
  `IDCliente` int(11) DEFAULT NULL,
  `IDInteresAbono` int(11) DEFAULT NULL,
  `IDFactura` int(11) DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `Hora` time DEFAULT NULL,
  `Referencia` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ValorAbono` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ValorInteres` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ValorTotal` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ValorSaldo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.abonos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `abonos` DISABLE KEYS */;
/*!40000 ALTER TABLE `abonos` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.accion
CREATE TABLE IF NOT EXISTS `accion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDAccion` int(11) NOT NULL DEFAULT 0,
  `Accion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.accion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `accion` DISABLE KEYS */;
/*!40000 ALTER TABLE `accion` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.administradores
CREATE TABLE IF NOT EXISTS `administradores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dni` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.administradores: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `administradores` DISABLE KEYS */;
INSERT INTO `administradores` (`id`, `nombre`, `apellido`, `telefono`, `direccion`, `dni`, `created_at`, `updated_at`) VALUES
	(1, 'Guillermo', 'Hernandez', '04140484765', 'casa2', '20587620', '2019-05-10 01:19:24', '2019-06-23 20:53:16'),
	(3, 'Pedro', 'Perez', '04140484765', 'Casa2', '20586123', '2019-06-23 20:54:07', '2019-06-23 20:54:07');
/*!40000 ALTER TABLE `administradores` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.almacen
CREATE TABLE IF NOT EXISTS `almacen` (
  `IDAlmacen` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `administrador_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `Razon_Social` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Nombre_Comercial` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RUC` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Telefono1` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Telefono2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelfCelular1` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelfCelular2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Correo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Nombres_Gerente` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Correo_Gerente` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelfCelular_Gerente` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ObligadoLlevarContabilidad` int(11) DEFAULT NULL COMMENT '1 es SÍ, cero es NO',
  `SitioWeb` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Usuarios_IDUsuario` int(11) DEFAULT NULL,
  `Usuarios_Cargos_IDCargo` int(10) unsigned DEFAULT NULL,
  `Usuarios_Salarios_IDSalario` int(11) DEFAULT NULL,
  PRIMARY KEY (`IDAlmacen`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.almacen: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `almacen` DISABLE KEYS */;
INSERT INTO `almacen` (`IDAlmacen`, `nombre`, `direccion`, `telefono`, `descripcion`, `administrador_id`, `created_at`, `updated_at`, `Razon_Social`, `Nombre_Comercial`, `RUC`, `Telefono1`, `Telefono2`, `TelfCelular1`, `TelfCelular2`, `Correo`, `Nombres_Gerente`, `Correo_Gerente`, `TelfCelular_Gerente`, `ObligadoLlevarContabilidad`, `SitioWeb`, `Usuarios_IDUsuario`, `Usuarios_Cargos_IDCargo`, `Usuarios_Salarios_IDSalario`) VALUES
	(1, 'almacen11', 'por la casa', '02463432943', 'solo telas', 1, '2019-05-05 16:50:25', '2019-06-17 18:53:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'almacen2', 'por la casa', '02463432943', 'solo cueros', 1, '2019-05-05 16:50:25', '2019-05-05 17:09:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'ALMACEN3', 'CASA', '894375894379834', 'HOLA', 1, '2019-06-21 15:26:41', '2019-06-21 15:26:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 'ALMACEN4', 'CASA', '835438793847', 'HOLA', 1, '2019-06-21 15:30:32', '2019-06-21 15:30:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 'ALMACEN5', 'CASA', '90458093458', 'EJRKKSDJFSD', 3, '2019-06-23 21:17:55', '2019-06-23 21:17:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `almacen` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.arqueo
CREATE TABLE IF NOT EXISTS `arqueo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FECHA_CIERRE` datetime DEFAULT NULL,
  `VALOR_NETO` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IVA_GENERADO` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMPLOYEE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDArqueo` int(11) DEFAULT NULL,
  `NumComprob` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.arqueo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `arqueo` DISABLE KEYS */;
/*!40000 ALTER TABLE `arqueo` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.bancos
CREATE TABLE IF NOT EXISTS `bancos` (
  `IDTablaBancos` int(11) NOT NULL AUTO_INCREMENT,
  `IDalmacen` int(11) DEFAULT NULL,
  `Nombre_Banco` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Num_Cuenta` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TipoCuenta` int(11) DEFAULT NULL COMMENT '1 Ahorros, 0 Corriente',
  `OficialCuenta` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SaldoActual` decimal(15,2) DEFAULT NULL,
  `almacen_IDalmacen` int(11) NOT NULL,
  PRIMARY KEY (`IDTablaBancos`,`almacen_IDalmacen`),
  UNIQUE KEY `IDalmacen_UNIQUE` (`IDalmacen`),
  KEY `fk_Bancos_almacen1_idx` (`almacen_IDalmacen`),
  CONSTRAINT `fk_Bancos_almacen1` FOREIGN KEY (`almacen_IDalmacen`) REFERENCES `almacen` (`IDAlmacen`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.bancos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bancos` DISABLE KEYS */;
/*!40000 ALTER TABLE `bancos` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.bodega
CREATE TABLE IF NOT EXISTS `bodega` (
  `IDBodega` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Bodega` varchar(450) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Direccion_Bodega` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RUC` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IDBodega`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.bodega: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bodega` DISABLE KEYS */;
/*!40000 ALTER TABLE `bodega` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.bodega_has_catalogo_de_inventarios
CREATE TABLE IF NOT EXISTS `bodega_has_catalogo_de_inventarios` (
  `Bodega_IDBodega` int(11) NOT NULL,
  `Catalogo_de_Inventarios_IDCatalogo` bigint(20) NOT NULL,
  PRIMARY KEY (`Bodega_IDBodega`,`Catalogo_de_Inventarios_IDCatalogo`),
  KEY `fk_Bodega_has_Catalogo_de_Inventarios_Catalogo_de_Inventari_idx` (`Catalogo_de_Inventarios_IDCatalogo`),
  KEY `fk_Bodega_has_Catalogo_de_Inventarios_Bodega1_idx` (`Bodega_IDBodega`),
  CONSTRAINT `fk_Bodega_has_Catalogo_de_Inventarios_Bodega1` FOREIGN KEY (`Bodega_IDBodega`) REFERENCES `bodega` (`IDBodega`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Bodega_has_Catalogo_de_Inventarios_Catalogo_de_Inventarios1` FOREIGN KEY (`Catalogo_de_Inventarios_IDCatalogo`) REFERENCES `catalogo_de_inventarios` (`IDCatalogo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.bodega_has_catalogo_de_inventarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bodega_has_catalogo_de_inventarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `bodega_has_catalogo_de_inventarios` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.cargos
CREATE TABLE IF NOT EXISTS `cargos` (
  `IDCargo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NombreCargo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Prioridad` int(11) DEFAULT NULL,
  `IDUsuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`IDCargo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.cargos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `cargos` DISABLE KEYS */;
INSERT INTO `cargos` (`IDCargo`, `NombreCargo`, `Prioridad`, `IDUsuario`) VALUES
	(1, 'Indefinido', 1, 1);
/*!40000 ALTER TABLE `cargos` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.catalogo_de_inventarios
CREATE TABLE IF NOT EXISTS `catalogo_de_inventarios` (
  `IDCatalogo` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreCatalogo` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CtaContable` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDDescuento` int(11) DEFAULT NULL,
  `IDImpuesto` int(11) DEFAULT NULL,
  `IDBodega` int(11) DEFAULT NULL,
  `establecimiento_IDEstablecimiento` int(11) NOT NULL,
  `establecimiento_almacen_IDAlmacen` int(11) NOT NULL,
  PRIMARY KEY (`IDCatalogo`,`establecimiento_IDEstablecimiento`,`establecimiento_almacen_IDAlmacen`),
  UNIQUE KEY `IDCatInvs_UNIQUE` (`IDCatalogo`),
  KEY `fk_Catalogo_de_Inventarios_establecimiento1_idx` (`establecimiento_IDEstablecimiento`,`establecimiento_almacen_IDAlmacen`),
  CONSTRAINT `fk_Catalogo_de_Inventarios_establecimiento1` FOREIGN KEY (`establecimiento_IDEstablecimiento`, `establecimiento_almacen_IDAlmacen`) REFERENCES `establecimiento` (`IDEstablecimiento`, `almacen_IDalmacen`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.catalogo_de_inventarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `catalogo_de_inventarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalogo_de_inventarios` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.categoria
CREATE TABLE IF NOT EXISTS `categoria` (
  `IDCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `Categoria` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TipoCuenta` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TipoTrans` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CtaContable` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CategoriaFacturable` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDDescuento` int(11) DEFAULT NULL,
  `ProductoElaborado` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DebitarDelTotal` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`IDCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.categoria: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` (`IDCategoria`, `Categoria`, `TipoCuenta`, `TipoTrans`, `CtaContable`, `CategoriaFacturable`, `IDDescuento`, `ProductoElaborado`, `DebitarDelTotal`, `created_at`, `updated_at`) VALUES
	(1, 'categoria test2', 'pendiente', 'ñlkdfsñlsdk', '0945809428509342', '1', 1, '1', '20', '2019-05-10 00:20:58', '2019-05-10 00:53:57'),
	(2, 'categoria 2', 'pendiente', 'hola', '23894293084', 'factura', 2, 'total', '23', '2019-06-23 20:59:18', '2019-06-23 20:59:18');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.cierres
CREATE TABLE IF NOT EXISTS `cierres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDCierres` int(11) NOT NULL DEFAULT 0,
  `Fecha` date DEFAULT NULL,
  `Destino` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDCaja` int(11) DEFAULT NULL,
  `Hora` time DEFAULT NULL,
  `CierreRealizado` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDCajero` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.cierres: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `cierres` DISABLE KEYS */;
/*!40000 ALTER TABLE `cierres` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.ciudades_de_prov_estado
CREATE TABLE IF NOT EXISTS `ciudades_de_prov_estado` (
  `IDCiudades_De_Prov_Estado` int(11) NOT NULL AUTO_INCREMENT,
  `IDProvincia_Estado` int(11) DEFAULT NULL,
  `NombreCiudad` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AbreviacionCiudad` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PosicionGeografica` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NumeroDeHabitantes` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `Prov_Estado_IDProv_Estado` int(11) NOT NULL,
  `Prov_Estado_Nacion_IDNacion` int(11) NOT NULL,
  PRIMARY KEY (`IDCiudades_De_Prov_Estado`,`Prov_Estado_IDProv_Estado`,`Prov_Estado_Nacion_IDNacion`),
  UNIQUE KEY `IDCiudades_De_Prov_Estado_UNIQUE` (`IDCiudades_De_Prov_Estado`),
  KEY `fk_Ciudades_De_Prov_Estado_Prov_Estado1_idx` (`Prov_Estado_IDProv_Estado`,`Prov_Estado_Nacion_IDNacion`),
  CONSTRAINT `fk_Ciudades_De_Prov_Estado_Prov_Estado1` FOREIGN KEY (`Prov_Estado_IDProv_Estado`, `Prov_Estado_Nacion_IDNacion`) REFERENCES `prov_estado` (`IDProv_Estado`, `Nacion_IDNacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.ciudades_de_prov_estado: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ciudades_de_prov_estado` DISABLE KEYS */;
/*!40000 ALTER TABLE `ciudades_de_prov_estado` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `IDCliente` int(11) NOT NULL AUTO_INCREMENT,
  `IDAlmacen` int(11) DEFAULT NULL,
  `NumFamiliares` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Apellidos` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Nombres` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CedulaRUC` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Direccion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelefonoDom` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelefonoMovil` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelefonoTrabajo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NumHijos` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LugarTrabajo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Precio` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Descuento` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Referencia` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FechaInscripcion` date DEFAULT NULL,
  `FechaNacimiento` date DEFAULT NULL,
  `Ocupacion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EstadoCivil` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Genero` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Edad` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Foto` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ValorLimiteCredito` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pwd` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DireccionOficina` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CtaContable` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Notas` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`IDCliente`),
  KEY `FK_clientes_almacen` (`IDAlmacen`),
  CONSTRAINT `FK_clientes_almacen` FOREIGN KEY (`IDAlmacen`) REFERENCES `almacen` (`IDAlmacen`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.clientes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`IDCliente`, `IDAlmacen`, `NumFamiliares`, `Apellidos`, `Nombres`, `CedulaRUC`, `Direccion`, `TelefonoDom`, `TelefonoMovil`, `TelefonoTrabajo`, `NumHijos`, `LugarTrabajo`, `email`, `Precio`, `Descuento`, `Referencia`, `FechaInscripcion`, `FechaNacimiento`, `Ocupacion`, `EstadoCivil`, `Genero`, `Edad`, `Foto`, `ValorLimiteCredito`, `pwd`, `DireccionOficina`, `CtaContable`, `Notas`, `created_at`, `updated_at`) VALUES
	(2, 1, '3', 'kdjfsdkj', 'jdfskld', '903809485', NULL, '48375983475938', '398459384', '948590348', '2', 'dsjjskljfsñld', 'cliente@gmail.com', '87', '8', 'kiusdfkjsdl', '2019-06-23', '2019-06-21', 'kjsdjfksdj', 'M', 'M', '26', 'user.jpg', '23', NULL, 'sdkljfksdjfkl', '87234874', 'kskjdfskfs', '2019-06-21 18:16:55', '2019-06-21 18:30:49'),
	(3, 1, '2', 'sdkljkdf', 'jfklfk', '94903485', NULL, '734985739847', '94035809348', '384758934759', '1', 'casa', 'cliente2@gmail.com', '2', '2', 'ehfsjk', '2019-06-23', '1993-03-11', 'informatico', 'S', 'M', '26', 'tpmaptgente seria.png', '23', NULL, 'casa', '210893742389472', 'dkjfsdlkjskl', '2019-06-23 21:16:22', '2019-06-23 21:16:22');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.colores
CREATE TABLE IF NOT EXISTS `colores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='colores disponibles';

-- Volcando datos para la tabla dbalmacenv2.colores: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `colores` DISABLE KEYS */;
INSERT INTO `colores` (`id`, `color`, `updated_at`, `created_at`) VALUES
	(1, 'skin-blue', NULL, NULL),
	(2, 'skin-black', NULL, NULL),
	(3, 'skin-purple', NULL, NULL),
	(4, 'skin-green', NULL, NULL),
	(5, 'skin-red', NULL, NULL),
	(6, 'skin-yellow', NULL, NULL),
	(7, 'skin-blue-light', NULL, NULL),
	(8, 'skin-black-light', NULL, NULL),
	(9, 'skin-purple-light', NULL, NULL),
	(10, 'skin-green-light', NULL, NULL),
	(11, 'skin-red-light', NULL, NULL),
	(12, 'skin-yellow-light', NULL, NULL);
/*!40000 ALTER TABLE `colores` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.config
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Mensaje` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.config: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
/*!40000 ALTER TABLE `config` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.configacc
CREATE TABLE IF NOT EXISTS `configacc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TablaID` int(11) NOT NULL,
  `UserType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HorarioID` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.configacc: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `configacc` DISABLE KEYS */;
/*!40000 ALTER TABLE `configacc` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.configfe
CREATE TABLE IF NOT EXISTS `configfe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDConfigFE` int(11) NOT NULL DEFAULT 0,
  `RazonSocial` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DirMatriz` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DirEstablecimiento` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CodEstablecimiento` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CodPtoEmision` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SecuencialFactura` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NumResolucionContribEspecial` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ObligadoLlevarContabilidad` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FullSecuencialNotaCredito` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FullSecuencialNotaDebito` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FullSecuencialGuiaRemision` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FullSecuencialComprobanteRetencion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RutaFirmaElectronica` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PWDFirmaElectronica` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HostEmail` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PWDEmail` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PuertoEmail` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HabilitarSSL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RutaLogo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AsuntoEmail` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Ambiente` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TiempoLimiteEspera` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RutaDocsGenerados` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RutaDocsFirmados` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RutaDocsAutorizados` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RutaDocsNOAutorizados` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RutaDocsEnviados` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Moneda` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CadenaCron` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDCaja` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.configfe: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `configfe` DISABLE KEYS */;
/*!40000 ALTER TABLE `configfe` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.confighardware
CREATE TABLE IF NOT EXISTS `confighardware` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDConfigHardware` int(11) NOT NULL DEFAULT 0,
  `intLatenciaRelays` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDUser` int(11) DEFAULT NULL,
  `TmpoCierreBrazoEnt` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TmpoCierreBrazoSalida` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TmpoAperturaBrazoEnt` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TmpoAperturaBrazoSalida` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.confighardware: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `confighardware` DISABLE KEYS */;
/*!40000 ALTER TABLE `confighardware` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.configseg
CREATE TABLE IF NOT EXISTS `configseg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDConfigSeg` int(11) NOT NULL DEFAULT 0,
  `IDCaja` int(11) DEFAULT NULL,
  `lngAnulacionRequiereAutorizac` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FechaValidoDesde` date DEFAULT NULL,
  `FechaValidoHasta` date DEFAULT NULL,
  `HoraValidoDesde` time DEFAULT NULL,
  `HoraValidoHasta` time DEFAULT NULL,
  `lngInhabilitarReportesCaja` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.configseg: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `configseg` DISABLE KEYS */;
/*!40000 ALTER TABLE `configseg` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.configuracion
CREATE TABLE IF NOT EXISTS `configuracion` (
  `IDConfig` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Tab` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IDConfig`),
  UNIQUE KEY `IDConfiguracion_UNIQUE` (`IDConfig`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.configuracion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `configuracion` DISABLE KEYS */;
/*!40000 ALTER TABLE `configuracion` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.coordenadasfactura
CREATE TABLE IF NOT EXISTS `coordenadasfactura` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `IDCoordenadasFactura` int(11) DEFAULT NULL,
  `Indx` int(11) DEFAULT NULL,
  `NombresClienteX` int(11) DEFAULT NULL,
  `NombresClienteY` int(11) DEFAULT NULL,
  `OcultarNombres` int(11) DEFAULT NULL,
  `DirX` int(11) DEFAULT NULL,
  `DirY` int(11) DEFAULT NULL,
  `OcultarDir` int(11) DEFAULT NULL,
  `TelfX` int(11) DEFAULT NULL,
  `TelfY` int(11) DEFAULT NULL,
  `OcultarTelf` int(11) DEFAULT NULL,
  `RUCX` int(11) DEFAULT NULL,
  `RUCY` int(11) DEFAULT NULL,
  `OcultarRuc` int(11) DEFAULT NULL,
  `FechaX` int(11) DEFAULT NULL,
  `FechaY` int(11) DEFAULT NULL,
  `OcultarFecha` int(11) DEFAULT NULL,
  `HeaderParam1X` int(11) DEFAULT NULL,
  `HeaderParam1Y` int(11) DEFAULT NULL,
  `HeaderTextoParam1` int(11) DEFAULT NULL,
  `OcultarHeaderTextoParam1` int(11) DEFAULT NULL,
  `HeaderParam2X` int(11) DEFAULT NULL,
  `HeaderParam2Y` int(11) DEFAULT NULL,
  `HeaderTextoParam2` int(11) DEFAULT NULL,
  `OcultarHeaderTextoParam2` int(11) DEFAULT NULL,
  `HeaderParam3X` int(11) DEFAULT NULL,
  `HeaderParam3Y` int(11) DEFAULT NULL,
  `HeaderTextoParam3` int(11) DEFAULT NULL,
  `OcultarHeaderTextoParam3` int(11) DEFAULT NULL,
  `HeaderParam4X` int(11) DEFAULT NULL,
  `HeaderParam4Y` int(11) DEFAULT NULL,
  `HeaderTextoParam4` int(11) DEFAULT NULL,
  `OcultarHeaderTextoParam4` int(11) DEFAULT NULL,
  `CantX` int(11) DEFAULT NULL,
  `CantY` int(11) DEFAULT NULL,
  `OcultarCant` int(11) DEFAULT NULL,
  `DescripcionX` int(11) DEFAULT NULL,
  `DescripcionY` int(11) DEFAULT NULL,
  `OcultarDescripcion` int(11) DEFAULT NULL,
  `VUnitX` int(11) DEFAULT NULL,
  `VUnitY` int(11) DEFAULT NULL,
  `OcultarVU` int(11) DEFAULT NULL,
  `VTotalX` int(11) DEFAULT NULL,
  `VTotalY` int(11) DEFAULT NULL,
  `OcultarVT` int(11) DEFAULT NULL,
  `BodyParam1X` int(11) DEFAULT NULL,
  `BodyParam1Y` int(11) DEFAULT NULL,
  `BodyTextoParam1` int(11) DEFAULT NULL,
  `OcultarBodyTextoParam1` int(11) DEFAULT NULL,
  `BodyParam2X` int(11) DEFAULT NULL,
  `BodyParam2Y` int(11) DEFAULT NULL,
  `BodyTextoParam2` int(11) DEFAULT NULL,
  `OcultarBodyTextoParam2` int(11) DEFAULT NULL,
  `BodyParam3X` int(11) DEFAULT NULL,
  `BodyParam3Y` int(11) DEFAULT NULL,
  `BodyTextoParam3` int(11) DEFAULT NULL,
  `OcultarBodyTextoParam3` int(11) DEFAULT NULL,
  `BodyParam4X` int(11) DEFAULT NULL,
  `BodyParam4Y` int(11) DEFAULT NULL,
  `BodyTextoParam4` int(11) DEFAULT NULL,
  `OcultarBodyTextoParam4` int(11) DEFAULT NULL,
  `SepItemsPorFact` int(11) DEFAULT NULL,
  `SubtotalX` int(11) DEFAULT NULL,
  `SubtotalY` int(11) DEFAULT NULL,
  `OcultarSubtotal` int(11) DEFAULT NULL,
  `IVACeroX` int(11) DEFAULT NULL,
  `IVACeroY` int(11) DEFAULT NULL,
  `OcultarIVACero` int(11) DEFAULT NULL,
  `IVA12X` int(11) DEFAULT NULL,
  `IVA12Y` int(11) DEFAULT NULL,
  `OcultarIVAn` int(11) DEFAULT NULL,
  `TotalX` int(11) DEFAULT NULL,
  `TotalY` int(11) DEFAULT NULL,
  `OcultarTotal` int(11) DEFAULT NULL,
  `FooterParam1X` int(11) DEFAULT NULL,
  `FooterParam1Y` int(11) DEFAULT NULL,
  `FooterTextoParam1` int(11) DEFAULT NULL,
  `OcultarFooterTextoParam1` int(11) DEFAULT NULL,
  `FooterParam2X` int(11) DEFAULT NULL,
  `FooterParam2Y` int(11) DEFAULT NULL,
  `FooterTextoParam2` int(11) DEFAULT NULL,
  `OcultarFooterTextoParam2` int(11) DEFAULT NULL,
  `FooterParam3X` int(11) DEFAULT NULL,
  `FooterParam3Y` int(11) DEFAULT NULL,
  `FooterTextoParam3` int(11) DEFAULT NULL,
  `OcultarFooterTextoParam3` int(11) DEFAULT NULL,
  `FooterParam4X` int(11) DEFAULT NULL,
  `FooterParam4Y` int(11) DEFAULT NULL,
  `FooterTextoParam4` int(11) DEFAULT NULL,
  `OcultarFooterTextoParam4` int(11) DEFAULT NULL,
  `StepItemsPorFacturar` int(11) DEFAULT NULL,
  `NombreFuente` int(11) DEFAULT NULL,
  `TamFuente` int(11) DEFAULT NULL,
  `MaxNumCaracteresPorCampo` int(11) DEFAULT NULL,
  `IncluirGrilla` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.coordenadasfactura: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `coordenadasfactura` DISABLE KEYS */;
/*!40000 ALTER TABLE `coordenadasfactura` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.coordenadasfacturaextended
CREATE TABLE IF NOT EXISTS `coordenadasfacturaextended` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDCoordenadasFacturaExtended` int(11) DEFAULT 0,
  `Indx` int(11) DEFAULT 0,
  `txtEtiqParamEncab1` int(11) DEFAULT 0,
  `txtXEtiqParamEncab1` int(11) DEFAULT 0,
  `txtYEtiqParamEncab1` int(11) DEFAULT 0,
  `txtEtiqParamEncab2` int(11) DEFAULT 0,
  `txtXEtiqParamEncab2` int(11) DEFAULT 0,
  `txtYEtiqParamEncab2` int(11) DEFAULT 0,
  `txtEtiqParamEncab3` int(11) DEFAULT 0,
  `txtXEtiqParamEncab3` int(11) DEFAULT 0,
  `txtYEtiqParamEncab3` int(11) DEFAULT 0,
  `txtEtiqParamEncab4` int(11) DEFAULT 0,
  `txtXEtiqParamEncab4` int(11) DEFAULT 0,
  `txtYEtiqParamEncab4` int(11) DEFAULT 0,
  `txtEtiqParam5` int(11) DEFAULT 0,
  `txtXEtiqParamEncab5` int(11) DEFAULT 0,
  `txtYEtiqParamEncab5` int(11) DEFAULT 0,
  `txtEtiqParam6` int(11) DEFAULT 0,
  `txtXEtiqParamEncab6` int(11) DEFAULT 0,
  `txtYEtiqParamEncab6` int(11) DEFAULT 0,
  `txtEtiqParam7` int(11) DEFAULT 0,
  `txtXEtiqParamEncab7` int(11) DEFAULT 0,
  `txtYEtiqParamEncab7` int(11) DEFAULT 0,
  `txtEtiqParam8` int(11) DEFAULT 0,
  `txtXEtiqParamEncab8` int(11) DEFAULT 0,
  `txtYEtiqParamEncab8` int(11) DEFAULT 0,
  `txtParamEncab5` int(11) DEFAULT 0,
  `txtXParamEncab5` int(11) DEFAULT 0,
  `txtYParamEncab5` int(11) DEFAULT 0,
  `OcultartxtParamEncab5` int(11) DEFAULT 0,
  `txtParamEncab6` int(11) DEFAULT 0,
  `txtXParamEncab6` int(11) DEFAULT 0,
  `txtYParamEncab6` int(11) DEFAULT 0,
  `OcultartxtParamEncab6` int(11) DEFAULT 0,
  `txtParamEncab7` int(11) DEFAULT 0,
  `txtXParamEncab7` int(11) DEFAULT 0,
  `txtYParamEncab7` int(11) DEFAULT 0,
  `OcultartxtParamEncab7` int(11) DEFAULT 0,
  `txtParamEncab8` int(11) DEFAULT 0,
  `txtXParamEncab8` int(11) DEFAULT 0,
  `txtYParamEncab8` int(11) DEFAULT 0,
  `OcultartxtParamEncab8` int(11) DEFAULT 0,
  `txtEtiqParamCuerpo1` int(11) DEFAULT 0,
  `txtXEtiqParamCuerpo1` int(11) DEFAULT 0,
  `txtYEtiqParamCuerpo1` int(11) DEFAULT 0,
  `txtEtiqParamCuerpo2` int(11) DEFAULT 0,
  `txtXEtiqParamCuerpo2` int(11) DEFAULT 0,
  `txtYEtiqParamCuerpo2` int(11) DEFAULT 0,
  `txtEtiqParamCuerpo3` int(11) DEFAULT 0,
  `txtXEtiqParamCuerpo3` int(11) DEFAULT 0,
  `txtYEtiqParamCuerpo3` int(11) DEFAULT 0,
  `txtEtiqParamCuerpo4` int(11) DEFAULT 0,
  `txtXEtiqParamCuerpo4` int(11) DEFAULT 0,
  `txtYEtiqParamCuerpo4` int(11) DEFAULT 0,
  `txtEtiqParamCuerpo5` int(11) DEFAULT 0,
  `txtXEtiqParamCuerpo5` int(11) DEFAULT 0,
  `txtYEtiqParamCuerpo5` int(11) DEFAULT 0,
  `txtEtiqParamCuerpo6` int(11) DEFAULT 0,
  `txtXEtiqParamCuerpo6` int(11) DEFAULT 0,
  `txtYEtiqParamCuerpo6` int(11) DEFAULT 0,
  `txtEtiqParamCuerpo7` int(11) DEFAULT 0,
  `txtXEtiqParamCuerpo7` int(11) DEFAULT 0,
  `txtYEtiqParamCuerpo7` int(11) DEFAULT 0,
  `txtEtiqParamCuerpo8` int(11) DEFAULT 0,
  `txtXEtiqParamCuerpo8` int(11) DEFAULT 0,
  `txtYEtiqParamCuerpo8` int(11) DEFAULT 0,
  `txtParamCuerpo5` int(11) DEFAULT 0,
  `txtXParamCuerpo5` int(11) DEFAULT 0,
  `txtYParamCuerpo5` int(11) DEFAULT 0,
  `OcultartxtParamCuerpo5` int(11) DEFAULT 0,
  `txtParamCuerpo6` int(11) DEFAULT 0,
  `txtXParamCuerpo6` int(11) DEFAULT 0,
  `txtYParamCuerpo6` int(11) DEFAULT 0,
  `OcultartxtParamCuerpo6` int(11) DEFAULT 0,
  `txtParamCuerpo7` int(11) DEFAULT 0,
  `txtXParamCuerpo7` int(11) DEFAULT 0,
  `txtYParamCuerpo7` int(11) DEFAULT 0,
  `OcultartxtParamCuerpo7` int(11) DEFAULT 0,
  `txtParamCuerpo8` int(11) DEFAULT 0,
  `txtXParamCuerpo8` int(11) DEFAULT 0,
  `txtYParamCuerpo8` int(11) DEFAULT 0,
  `OcultartxtParamCuerpo8` int(11) DEFAULT 0,
  `txtEtiqParamPie1` int(11) DEFAULT 0,
  `txtXEtiqParamPie1` int(11) DEFAULT 0,
  `txtYEtiqParamPie1` int(11) DEFAULT 0,
  `txtEtiqParamPie2` int(11) DEFAULT 0,
  `txtXEtiqParamPie2` int(11) DEFAULT 0,
  `txtYEtiqParamPie2` int(11) DEFAULT 0,
  `txtEtiqParamPie3` int(11) DEFAULT 0,
  `txtXEtiqParamPie3` int(11) DEFAULT 0,
  `txtYEtiqParamPie3` int(11) DEFAULT 0,
  `txtEtiqParamPie4` int(11) DEFAULT 0,
  `txtXEtiqParamPie4` int(11) DEFAULT 0,
  `txtYEtiqParamPie4` int(11) DEFAULT 0,
  `txtEtiqParamPie5` int(11) DEFAULT 0,
  `txtXEtiqParamPie5` int(11) DEFAULT 0,
  `txtYEtiqParamPie5` int(11) DEFAULT 0,
  `txtEtiqParamPie6` int(11) DEFAULT 0,
  `txtXEtiqParamPie6` int(11) DEFAULT 0,
  `txtYEtiqParamPie6` int(11) DEFAULT 0,
  `txtEtiqParamPie7` int(11) DEFAULT 0,
  `txtXEtiqParamPie7` int(11) DEFAULT 0,
  `txtYEtiqParamPie7` int(11) DEFAULT 0,
  `txtEtiqParamPie8` int(11) DEFAULT 0,
  `txtXEtiqParamPie8` int(11) DEFAULT 0,
  `txtYEtiqParamPie8` int(11) DEFAULT 0,
  `txtParamPie5` int(11) DEFAULT 0,
  `txtXParamPie5` int(11) DEFAULT 0,
  `txtYParamPie5` int(11) DEFAULT 0,
  `OcultartxtParamPie5` int(11) DEFAULT 0,
  `txtParamPie6` int(11) DEFAULT 0,
  `txtXParamPie6` int(11) DEFAULT 0,
  `txtYParamPie6` int(11) DEFAULT 0,
  `OcultartxtParamPie6` int(11) DEFAULT 0,
  `txtParamPie7` int(11) DEFAULT 0,
  `txtXParamPie7` int(11) DEFAULT 0,
  `txtYParamPie7` int(11) DEFAULT 0,
  `OcultartxtParamPie7` int(11) DEFAULT 0,
  `txtParamPie8` int(11) DEFAULT 0,
  `txtXParamPie8` int(11) DEFAULT 0,
  `txtYParamPie8` int(11) DEFAULT 0,
  `OcultartxtParamPie8` int(11) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.coordenadasfacturaextended: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `coordenadasfacturaextended` DISABLE KEYS */;
/*!40000 ALTER TABLE `coordenadasfacturaextended` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.coordenadasfactura_
CREATE TABLE IF NOT EXISTS `coordenadasfactura_` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDCoordenadasFactura` int(11) NOT NULL DEFAULT 0,
  `indx` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NombresClienteX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NombresClienteY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DirX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DirY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelfX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelfY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RUCX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RUCY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FechaX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FechaY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HeaderParam1X` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HeaderParam1Y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HeaderTextoParam1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HeaderParam2X` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HeaderParam2Y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HeaderTextoParam2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HeaderParam3X` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HeaderParam3Y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HeaderTextoParam3` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HeaderParam4X` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HeaderParam4Y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HeaderTextoParam4` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CantX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CantY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DescripcionX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DescripcionY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VUnitX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VUnitY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VTotalX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VTotalY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BodyParam1X` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BodyParam1Y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BodyTextoParam1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BodyParam2X` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BodyParam2Y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BodyTextoParam2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BodyParam3X` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BodyParam3Y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BodyTextoParam3` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BodyParam4X` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BodyParam4Y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BodyTextoParam4` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SepItemsPorFact` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SubtotalX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SubtotalY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IVACeroX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IVACeroY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IVA12X` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IVA12Y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TotalX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TotalY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FooterParam1X` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FooterParam1Y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FooterTextoParam1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FooterParam2X` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FooterParam2Y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FooterTextoParam2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FooterParam3X` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FooterParam3Y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FooterTextoParam3` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FooterParam4X` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FooterParam4Y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FooterTextoParam4` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MaxNumCaracteresPorCampo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StepItemsPorFacturar` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TamFuente` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NombreFuente` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `apdated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.coordenadasfactura_: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `coordenadasfactura_` DISABLE KEYS */;
/*!40000 ALTER TABLE `coordenadasfactura_` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.costos
CREATE TABLE IF NOT EXISTS `costos` (
  `IDCosto` int(11) NOT NULL AUTO_INCREMENT,
  `NombreCosto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'PEjm Transporte. en definitiva, se trata de costeo',
  `IDProveedor` int(11) DEFAULT NULL,
  PRIMARY KEY (`IDCosto`),
  UNIQUE KEY `IDCosto_UNIQUE` (`IDCosto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.costos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `costos` DISABLE KEYS */;
/*!40000 ALTER TABLE `costos` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.descuentos
CREATE TABLE IF NOT EXISTS `descuentos` (
  `IDDescuento` int(11) NOT NULL AUTO_INCREMENT,
  `NombreDescuento` int(11) DEFAULT NULL,
  `FechaValidoDesde` int(11) DEFAULT NULL,
  `FechaValidoHasta` int(11) DEFAULT NULL,
  `ValorPorcentualDescuento` int(11) DEFAULT NULL,
  `ValorMonetarioDescuento` int(11) DEFAULT NULL,
  `CodigoCupon` int(11) DEFAULT NULL,
  `NumArtsMinParaDescuento` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`IDDescuento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.descuentos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `descuentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `descuentos` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.descuentosenfactura
CREATE TABLE IF NOT EXISTS `descuentosenfactura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDDescuentosEnFactura` int(11) NOT NULL DEFAULT 0,
  `NombreDescuento` int(11) NOT NULL DEFAULT 0,
  `IDDescuento` int(11) NOT NULL DEFAULT 0,
  `lngRequiereAutorizacion` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.descuentosenfactura: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `descuentosenfactura` DISABLE KEYS */;
/*!40000 ALTER TABLE `descuentosenfactura` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.dias
CREATE TABLE IF NOT EXISTS `dias` (
  `IDDia` int(11) NOT NULL AUTO_INCREMENT,
  `NombreDia` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Lunes',
  `NumeroDia` int(11) DEFAULT NULL COMMENT '0= Lunes',
  `IDHorario` int(11) DEFAULT NULL,
  PRIMARY KEY (`IDDia`),
  UNIQUE KEY `IDDias_UNIQUE` (`IDDia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.dias: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `dias` DISABLE KEYS */;
/*!40000 ALTER TABLE `dias` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.establecimiento
CREATE TABLE IF NOT EXISTS `establecimiento` (
  `IDEstablecimiento` int(11) NOT NULL AUTO_INCREMENT,
  `IDCatalogo` bigint(20) DEFAULT NULL,
  `Nombre` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RUC` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Direccion` varchar(450) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Telefono1` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Telefono2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelfCelular` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Correo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Nombres_Administrador` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelfCelular_Administrador` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Correo_Administrador` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Numero_De_Colaboradores` int(11) DEFAULT NULL,
  `IDalmacen` int(11) DEFAULT NULL,
  `almacen_IDalmacen` int(11) NOT NULL,
  PRIMARY KEY (`IDEstablecimiento`,`almacen_IDalmacen`),
  KEY `fk_Establecimiento_almacen1_idx` (`almacen_IDalmacen`),
  CONSTRAINT `fk_Establecimiento_almacen1` FOREIGN KEY (`almacen_IDalmacen`) REFERENCES `almacen` (`IDAlmacen`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.establecimiento: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `establecimiento` DISABLE KEYS */;
/*!40000 ALTER TABLE `establecimiento` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.factstemps
CREATE TABLE IF NOT EXISTS `factstemps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Factura_Id` int(11) DEFAULT NULL,
  `IDTransLab` int(11) DEFAULT NULL,
  `NombPaciente` int(11) DEFAULT NULL,
  `Cant` int(11) DEFAULT NULL,
  `Descripcion` int(11) DEFAULT NULL,
  `Valor_Unitario` int(11) DEFAULT NULL,
  `Valor_Total` int(11) DEFAULT NULL,
  `ValSubTotal` int(11) DEFAULT NULL,
  `Anulada` int(11) DEFAULT NULL,
  `Fecha` int(11) DEFAULT NULL,
  `Hora` int(11) DEFAULT NULL,
  `NumRecibo` int(11) DEFAULT NULL,
  `Registrada` int(11) DEFAULT NULL,
  `IVA` int(11) DEFAULT NULL,
  `TipoTrans` int(11) DEFAULT NULL,
  `Consulta_Espec_Otro` int(11) DEFAULT NULL,
  `NumCaja` int(11) DEFAULT NULL,
  `Cod` int(11) DEFAULT NULL,
  `Referencia` int(11) DEFAULT NULL,
  `ID_Cliente` int(11) DEFAULT NULL,
  `Cerrada` int(11) DEFAULT NULL,
  `NumeroTicket` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.factstemps: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `factstemps` DISABLE KEYS */;
/*!40000 ALTER TABLE `factstemps` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.facturacion
CREATE TABLE IF NOT EXISTS `facturacion` (
  `IDFacturacion` int(11) NOT NULL AUTO_INCREMENT,
  `IDAlamcen` int(11) NOT NULL DEFAULT 0,
  `Factura_Id` int(11) DEFAULT NULL,
  `Cantidad` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Descripcion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Valor_Unitario` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Valor_Total` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Anulada` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `Hora` time DEFAULT NULL,
  `Cod` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NumCaja` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FactAbierta` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Terminos` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDCliente` int(11) DEFAULT NULL,
  `Referencia` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IVA` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TipoTrans` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TipoFactura` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IVA_Luego_Descuento` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDPrecioDistribuidor` int(11) DEFAULT NULL,
  `VTotal_luego_descuento` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TipoDeComprobante` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FormaDePago` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NotaVenta` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`IDFacturacion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.facturacion: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `facturacion` DISABLE KEYS */;
INSERT INTO `facturacion` (`IDFacturacion`, `IDAlamcen`, `Factura_Id`, `Cantidad`, `Descripcion`, `Valor_Unitario`, `Valor_Total`, `Anulada`, `Fecha`, `Hora`, `Cod`, `NumCaja`, `FactAbierta`, `Terminos`, `IDCliente`, `Referencia`, `IVA`, `TipoTrans`, `TipoFactura`, `IVA_Luego_Descuento`, `IDPrecioDistribuidor`, `VTotal_luego_descuento`, `TipoDeComprobante`, `FormaDePago`, `NotaVenta`, `created_at`, `updated_at`) VALUES
	(1, 1, 124, '1', 'hghfgh', '50', '200', NULL, '2019-05-28', '12:11:32', '1234', '1', '1', 'hjhgjhghj', 1, 'fghd', '2', '1', '1', '1', 1, '1', '1', 'Efectivo', 'no', '2019-05-28 12:12:12', '2019-05-28 12:12:12'),
	(2, 3, 124, '1', 'hghfgh', '50', '200', NULL, '2019-05-28', '12:11:32', '1234', '1', '1', 'hjhgjhghj', 1, 'fghd', '2', '1', '1', '1', 1, '1', '1', 'Efectivo', 'no', '2019-05-28 12:12:12', '2019-05-28 12:12:12');
/*!40000 ALTER TABLE `facturacion` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.facturaproveedor
CREATE TABLE IF NOT EXISTS `facturaproveedor` (
  `IDFactProv` int(11) NOT NULL AUTO_INCREMENT,
  `IDProv` int(11) DEFAULT NULL,
  `NumFactura` int(11) DEFAULT NULL,
  `FechaEmision` int(11) DEFAULT NULL,
  `FechaVencimiento` int(11) DEFAULT NULL,
  `Subtotal` int(11) DEFAULT NULL,
  `IVA` int(11) DEFAULT NULL,
  `Descuento` int(11) DEFAULT NULL,
  `Total` int(11) DEFAULT NULL,
  `Proveedores_IDProv` int(11) DEFAULT NULL,
  `IDUser` int(11) DEFAULT NULL,
  `Conciliada` int(11) DEFAULT NULL,
  `Pagada` int(11) DEFAULT NULL,
  `FechaPago` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`IDFactProv`),
  KEY `FK_facturaproveedor_proveedores` (`IDProv`),
  CONSTRAINT `FK_facturaproveedor_proveedores` FOREIGN KEY (`IDProv`) REFERENCES `proveedores` (`IDProv`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.facturaproveedor: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `facturaproveedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturaproveedor` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.formularios
CREATE TABLE IF NOT EXISTS `formularios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDFrm` int(11) NOT NULL DEFAULT 0,
  `NombreFrm` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.formularios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `formularios` DISABLE KEYS */;
/*!40000 ALTER TABLE `formularios` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.horarios
CREATE TABLE IF NOT EXISTS `horarios` (
  `HorarioID` int(11) NOT NULL AUTO_INCREMENT,
  `TablaID` int(11) NOT NULL DEFAULT 0,
  `Desde` datetime DEFAULT NULL,
  `Hasta` datetime DEFAULT NULL,
  `Dia` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`HorarioID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.horarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `horarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `horarios` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.horarios_has_users
CREATE TABLE IF NOT EXISTS `horarios_has_users` (
  `Horarios_IDHorario` int(11) NOT NULL,
  `users_IDUsuario` int(11) NOT NULL,
  `users_Cargos_IDCargo` int(10) unsigned NOT NULL,
  `users_Salarios_IDSalario` int(11) NOT NULL,
  PRIMARY KEY (`Horarios_IDHorario`,`users_IDUsuario`,`users_Cargos_IDCargo`,`users_Salarios_IDSalario`),
  KEY `fk_Horarios_has_users_users1_idx` (`users_IDUsuario`,`users_Cargos_IDCargo`,`users_Salarios_IDSalario`),
  KEY `fk_Horarios_has_users_Horarios1_idx` (`Horarios_IDHorario`),
  CONSTRAINT `FK_horarios_has_users_horarios` FOREIGN KEY (`Horarios_IDHorario`) REFERENCES `horarios` (`HorarioID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_horarios_has_users_users` FOREIGN KEY (`users_IDUsuario`) REFERENCES `users` (`IDUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.horarios_has_users: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `horarios_has_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `horarios_has_users` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.horas
CREATE TABLE IF NOT EXISTS `horas` (
  `IDHora` int(11) NOT NULL,
  `Hora` time DEFAULT NULL,
  `IDDia` int(11) DEFAULT NULL,
  `IDHorario` int(11) DEFAULT NULL,
  PRIMARY KEY (`IDHora`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.horas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `horas` DISABLE KEYS */;
/*!40000 ALTER TABLE `horas` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.idautomatico
CREATE TABLE IF NOT EXISTS `idautomatico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDAutomatico` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.idautomatico: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `idautomatico` DISABLE KEYS */;
/*!40000 ALTER TABLE `idautomatico` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.impuestos
CREATE TABLE IF NOT EXISTS `impuestos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Codigo` int(11) DEFAULT NULL,
  `ImpstsID` int(11) DEFAULT NULL,
  `Nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Valor` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.impuestos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `impuestos` DISABLE KEYS */;
/*!40000 ALTER TABLE `impuestos` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.inventarios
CREATE TABLE IF NOT EXISTS `inventarios` (
  `IDInventario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `descripcion` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `almacen_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `Nombre_Inventario` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDCatalogo` int(11) DEFAULT NULL,
  `IDDescuento` int(11) DEFAULT NULL,
  `IDEstablecimiento` int(11) DEFAULT NULL,
  `CtaContable` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Catalogo_de_Inventarios_IDCatalogo` int(11) NOT NULL,
  PRIMARY KEY (`IDInventario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.inventarios: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `inventarios` DISABLE KEYS */;
INSERT INTO `inventarios` (`IDInventario`, `nombre`, `descripcion`, `almacen_id`, `created_at`, `updated_at`, `Nombre_Inventario`, `IDCatalogo`, `IDDescuento`, `IDEstablecimiento`, `CtaContable`, `Catalogo_de_Inventarios_IDCatalogo`) VALUES
	(2, 'inventario11', 'papeleria 2', 1, '2019-05-16 11:50:25', '2019-05-16 16:04:10', NULL, NULL, NULL, NULL, NULL, 0),
	(3, 'inventario2', 'plasticos', 1, '2019-05-16 15:53:29', '2019-05-16 15:53:29', NULL, NULL, NULL, NULL, NULL, 0);
/*!40000 ALTER TABLE `inventarios` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.inventario_articulo
CREATE TABLE IF NOT EXISTS `inventario_articulo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventario_id` int(11) NOT NULL,
  `articulo_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.inventario_articulo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `inventario_articulo` DISABLE KEYS */;
INSERT INTO `inventario_articulo` (`id`, `inventario_id`, `articulo_id`, `created_at`, `updated_at`) VALUES
	(3, 2, 1, '2019-05-16 18:57:21', '2019-05-16 18:57:21'),
	(4, 2, 4, '2019-06-22 15:14:59', '2019-06-22 15:14:59');
/*!40000 ALTER TABLE `inventario_articulo` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.itemsfactprov
CREATE TABLE IF NOT EXISTS `itemsfactprov` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDElemsFactProv` int(11) DEFAULT NULL,
  `IDFactProv` int(11) DEFAULT NULL,
  `IDProducto` int(11) DEFAULT NULL,
  `Cantidad` int(11) DEFAULT NULL,
  `Descripcion` int(11) DEFAULT NULL,
  `VUnit` int(11) DEFAULT NULL,
  `VTotal` int(11) DEFAULT NULL,
  `FacturaProveedor_IDFactProv` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_itemsfactprov_facturaproveedor` (`FacturaProveedor_IDFactProv`),
  CONSTRAINT `FK_itemsfactprov_facturaproveedor` FOREIGN KEY (`FacturaProveedor_IDFactProv`) REFERENCES `facturaproveedor` (`IDFactProv`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.itemsfactprov: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `itemsfactprov` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemsfactprov` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.itemsprodelaborado
CREATE TABLE IF NOT EXISTS `itemsprodelaborado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDItemsProdElaborado` int(11) DEFAULT NULL,
  `Cod` int(11) DEFAULT NULL,
  `Cant` int(11) DEFAULT NULL,
  `IDCategoria` int(11) DEFAULT NULL,
  `Notas` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.itemsprodelaborado: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `itemsprodelaborado` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemsprodelaborado` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Evento` int(11) DEFAULT 0,
  `User` int(11) DEFAULT 0,
  `Fecha` int(11) DEFAULT 0,
  `LogID` int(11) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.log: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.margins
CREATE TABLE IF NOT EXISTS `margins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Left` int(11) DEFAULT 0,
  `Top` int(11) DEFAULT 0,
  `Right` int(11) DEFAULT 0,
  `Bottom` int(11) DEFAULT 0,
  `Indx` int(11) DEFAULT 0,
  `PortraitLandscape` int(11) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.margins: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `margins` DISABLE KEYS */;
/*!40000 ALTER TABLE `margins` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.margins2
CREATE TABLE IF NOT EXISTS `margins2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Left` int(11) DEFAULT NULL,
  `Top` int(11) DEFAULT NULL,
  `Right` int(11) DEFAULT NULL,
  `Bottom` int(11) DEFAULT NULL,
  `Indx` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.margins2: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `margins2` DISABLE KEYS */;
/*!40000 ALTER TABLE `margins2` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.migrations: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.moduloproveedores
CREATE TABLE IF NOT EXISTS `moduloproveedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDProv` int(11) DEFAULT NULL,
  `Columna 3` int(11) DEFAULT NULL,
  `RazonSocial` int(11) DEFAULT NULL,
  `RUC` int(11) DEFAULT NULL,
  `TelfPrincipal` int(11) DEFAULT NULL,
  `Direccion` int(11) DEFAULT NULL,
  `Notas` int(11) DEFAULT NULL,
  `CtaContable` int(11) DEFAULT NULL,
  `Email` int(11) DEFAULT NULL,
  `VendedorContacto` int(11) DEFAULT NULL,
  `TlfVendedorContacto` int(11) DEFAULT NULL,
  `NombreBanco1` int(11) DEFAULT NULL,
  `NumCtaCorriente1` int(11) DEFAULT NULL,
  `NumCtaAhorros1` int(11) DEFAULT NULL,
  `NombreBanco2` int(11) DEFAULT NULL,
  `NumCtaCorriente2` int(11) DEFAULT NULL,
  `NumCtaAhorros2` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.moduloproveedores: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `moduloproveedores` DISABLE KEYS */;
/*!40000 ALTER TABLE `moduloproveedores` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.nacion
CREATE TABLE IF NOT EXISTS `nacion` (
  `IDNacion` int(11) NOT NULL,
  `NombreNacion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Continente` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PrefijoTelefono` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SufijoDominioInternet` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Sufijo dominio de Internet. P. Ejm., para Ecuador: .ec',
  PRIMARY KEY (`IDNacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.nacion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `nacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `nacion` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.notasfactura
CREATE TABLE IF NOT EXISTS `notasfactura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDNotasFactura` int(11) DEFAULT NULL,
  `TextoNota` int(11) DEFAULT NULL,
  `IDCaja` int(11) DEFAULT NULL,
  `FechaValidoDesde` int(11) DEFAULT NULL,
  `FechaValidoHasta` int(11) DEFAULT NULL,
  `Visible` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.notasfactura: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `notasfactura` DISABLE KEYS */;
/*!40000 ALTER TABLE `notasfactura` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.numeracionfactura
CREATE TABLE IF NOT EXISTS `numeracionfactura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDNumeracionFactura` int(11) DEFAULT NULL,
  `NumEstablecimiento` int(11) DEFAULT NULL,
  `NumPtoEmision` int(11) DEFAULT NULL,
  `NumSecuencial` int(11) DEFAULT NULL,
  `NumCajaFisica` int(11) DEFAULT NULL,
  `IDUnicoMAC` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.numeracionfactura: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `numeracionfactura` DISABLE KEYS */;
/*!40000 ALTER TABLE `numeracionfactura` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.numfacturainstantanea
CREATE TABLE IF NOT EXISTS `numfacturainstantanea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDNumFacturaInstantanea` int(11) DEFAULT NULL,
  `NumFacturaInstantanea` int(11) DEFAULT NULL,
  `IDCaja` int(11) DEFAULT NULL,
  `IDUser` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.numfacturainstantanea: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `numfacturainstantanea` DISABLE KEYS */;
/*!40000 ALTER TABLE `numfacturainstantanea` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.numfacturapreimpresa
CREATE TABLE IF NOT EXISTS `numfacturapreimpresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDNumFacturaPreimpresa` int(11) DEFAULT 0,
  `NumFacturaPreimpresa` int(11) DEFAULT 0,
  `IDCaja` int(11) DEFAULT 0,
  `IDUser` int(11) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.numfacturapreimpresa: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `numfacturapreimpresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `numfacturapreimpresa` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.paramsconfig
CREATE TABLE IF NOT EXISTS `paramsconfig` (
  `IDParamsConfig` int(11) NOT NULL AUTO_INCREMENT,
  `IDConfig` int(11) DEFAULT NULL,
  `Nombre_Parametro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Valor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Configuracion_IDConfig` int(11) NOT NULL,
  PRIMARY KEY (`IDParamsConfig`,`Configuracion_IDConfig`),
  UNIQUE KEY `IDParamsConfig_UNIQUE` (`IDParamsConfig`),
  KEY `fk_ParamsConfig_Configuracion1_idx` (`Configuracion_IDConfig`),
  CONSTRAINT `fk_ParamsConfig_Configuracion1` FOREIGN KEY (`Configuracion_IDConfig`) REFERENCES `configuracion` (`IDConfig`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.paramsconfig: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `paramsconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `paramsconfig` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.pedidos
CREATE TABLE IF NOT EXISTS `pedidos` (
  `IDPedido` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDArticulo` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CedulaRUC` varchar(19) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IDPedido`),
  UNIQUE KEY `IDPedidos_UNIQUE` (`IDPedido`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.pedidos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pedidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedidos` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.posestante
CREATE TABLE IF NOT EXISTS `posestante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDEstante` int(11) DEFAULT NULL,
  `FilaEstante` int(11) DEFAULT NULL,
  `ColumnaEstante` int(11) DEFAULT NULL,
  `PosRelativa` int(11) DEFAULT NULL,
  `IDProd` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.posestante: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `posestante` DISABLE KEYS */;
/*!40000 ALTER TABLE `posestante` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.preciodistribuidor
CREATE TABLE IF NOT EXISTS `preciodistribuidor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDPrecioDistribuidor` int(11) DEFAULT 0,
  `NombrePrecioDistribuidor` int(11) DEFAULT 0,
  `ValorPrecioDistribuidor` int(11) DEFAULT 0,
  `RefPrecioDistribuidor` int(11) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.preciodistribuidor: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `preciodistribuidor` DISABLE KEYS */;
/*!40000 ALTER TABLE `preciodistribuidor` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.preciodistrib_articulo
CREATE TABLE IF NOT EXISTS `preciodistrib_articulo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDPrecioDistrib_Articulo` int(11) DEFAULT 0,
  `Cod` int(11) DEFAULT 0,
  `IDPrecioDistribuidor` int(11) DEFAULT 0,
  `ValorUnitPrecioDistribuidor` int(11) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.preciodistrib_articulo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `preciodistrib_articulo` DISABLE KEYS */;
/*!40000 ALTER TABLE `preciodistrib_articulo` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.precios
CREATE TABLE IF NOT EXISTS `precios` (
  `IDPrecios` int(11) NOT NULL AUTO_INCREMENT,
  `IDProducto` int(11) NOT NULL,
  `Nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Valor` decimal(15,2) DEFAULT NULL,
  `Productos_IDProducto` int(11) NOT NULL,
  PRIMARY KEY (`IDPrecios`,`Productos_IDProducto`),
  UNIQUE KEY `IDPrecios_UNIQUE` (`IDPrecios`),
  KEY `fk_Precios_Productos1_idx` (`Productos_IDProducto`),
  CONSTRAINT `fk_Precios_Productos1` FOREIGN KEY (`Productos_IDProducto`) REFERENCES `productos` (`IDProducto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.precios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `precios` DISABLE KEYS */;
/*!40000 ALTER TABLE `precios` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.pref
CREATE TABLE IF NOT EXISTS `pref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Direcc` int(11) DEFAULT NULL,
  `Ciudad` int(11) DEFAULT NULL,
  `Telf1` int(11) DEFAULT NULL,
  `Telf2` int(11) DEFAULT NULL,
  `ColorFondo` int(11) DEFAULT NULL,
  `ArchGraf` int(11) DEFAULT NULL,
  `AlwaysOnTop` int(11) DEFAULT NULL,
  `MinIni` int(11) DEFAULT NULL,
  `AutoIni` int(11) DEFAULT NULL,
  `AutoOpenLibro` int(11) DEFAULT NULL,
  `AutoOpenFacts` int(11) DEFAULT NULL,
  `GananciaInd` int(11) DEFAULT NULL,
  `GananciaGlob` int(11) DEFAULT NULL,
  `Iva` int(11) DEFAULT NULL,
  `Ruta` int(11) DEFAULT NULL,
  `NoCartel` int(11) DEFAULT NULL,
  `RespFloppy` int(11) DEFAULT NULL,
  `RespHDD` int(11) DEFAULT NULL,
  `IngrArts` int(11) DEFAULT NULL,
  `ReAprov` int(11) DEFAULT NULL,
  `MargGan` int(11) DEFAULT NULL,
  `Pedidos` int(11) DEFAULT NULL,
  `IniFact` int(11) DEFAULT NULL,
  `PrefID` int(11) DEFAULT NULL,
  `Prefe` int(11) DEFAULT NULL,
  `Aut` int(11) DEFAULT NULL,
  `Provs` int(11) DEFAULT NULL,
  `RUC` int(11) DEFAULT NULL,
  `NombImpr` int(11) DEFAULT NULL,
  `NombreEstablecim` int(11) DEFAULT NULL,
  `LimiteInf` int(11) DEFAULT NULL,
  `NumDigsCodigo` int(11) DEFAULT NULL,
  `CtaContIngr` int(11) DEFAULT NULL,
  `CtaContIva` int(11) DEFAULT NULL,
  `Reg_Loc_Remoto` int(11) DEFAULT NULL,
  `DSN` int(11) DEFAULT NULL,
  `SecAutom` int(11) DEFAULT NULL,
  `CtaContDestIVA` int(11) DEFAULT NULL,
  `CtaContDestNeto` int(11) DEFAULT NULL,
  `IDCaja` int(11) DEFAULT NULL,
  `AlertaCaja` int(11) DEFAULT NULL,
  `vPaperSize` int(11) DEFAULT NULL,
  `vNamePaperSize` int(11) DEFAULT NULL,
  `vUserHeight` int(11) DEFAULT NULL,
  `vUserWidth` int(11) DEFAULT NULL,
  `EtiqNoRegs` int(11) DEFAULT NULL,
  `Fuente` int(11) DEFAULT NULL,
  `Tam` int(11) DEFAULT NULL,
  `CtaContUtilEspec` int(11) DEFAULT NULL,
  `CtaContUtilCons` int(11) DEFAULT NULL,
  `CtaContCons` int(11) DEFAULT NULL,
  `CtaContFichas` int(11) DEFAULT NULL,
  `CtaContLab` int(11) DEFAULT NULL,
  `CtaContRx` int(11) DEFAULT NULL,
  `TipoFactura` int(11) DEFAULT NULL,
  `NumInicialFacturaInstantanea` int(11) DEFAULT NULL,
  `FirstTime` int(11) DEFAULT NULL,
  `FormatoFact` int(11) DEFAULT NULL,
  `PortraitLandscape` int(11) DEFAULT NULL,
  `vSegFact` int(11) DEFAULT NULL,
  `UnidadExternaUSB` int(11) DEFAULT NULL,
  `IncluirNumRefEnFactura` int(11) DEFAULT NULL,
  `confManualLector` int(11) DEFAULT NULL,
  `FechaAutorizacionDe` int(11) DEFAULT NULL,
  `FechaAutorizacionHasta` int(11) DEFAULT NULL,
  `TipoInventario` int(11) DEFAULT NULL,
  `AccionesSiStockAgotado` int(11) DEFAULT NULL,
  `NumInicialFacturaPreimpresa` int(11) DEFAULT NULL,
  `DSNUser` int(11) DEFAULT NULL,
  `DSNPassword` int(11) DEFAULT NULL,
  `vSegClientes` int(11) DEFAULT NULL,
  `vSegInventarios` int(11) DEFAULT NULL,
  `vSegReaprovisionar` int(11) DEFAULT NULL,
  `lngStepFactInstantanea` int(11) DEFAULT NULL,
  `vPreviewFactura` int(11) DEFAULT NULL,
  `lngDuracionSesion` int(11) DEFAULT NULL,
  `IDPref` int(11) DEFAULT NULL,
  `NIAC` int(11) DEFAULT NULL,
  `intSecuenciaFacturacion` int(11) DEFAULT NULL,
  `vColorSeleccionado` int(11) DEFAULT NULL,
  `lngStepFactInstantaneaDown` int(11) DEFAULT NULL,
  `intNumCopias` int(11) DEFAULT NULL,
  `lngTipoFacturacion` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.pref: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pref` DISABLE KEYS */;
/*!40000 ALTER TABLE `pref` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.pref2
CREATE TABLE IF NOT EXISTS `pref2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDPref2` int(11) DEFAULT NULL,
  `IDCaja` int(11) DEFAULT NULL,
  `PuertoSerial` int(11) DEFAULT NULL,
  `BitsXsegundo` int(11) DEFAULT NULL,
  `BitsDatos` int(11) DEFAULT NULL,
  `Paridad` int(11) DEFAULT NULL,
  `BitsParada` int(11) DEFAULT NULL,
  `ControlFlujo` int(11) DEFAULT NULL,
  `FormatoComunicBalanza` int(11) DEFAULT NULL,
  `NumMaxLineasImpresion` int(11) DEFAULT NULL,
  `ImprimirProforma` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.pref2: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pref2` DISABLE KEYS */;
/*!40000 ALTER TABLE `pref2` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.prefx
CREATE TABLE IF NOT EXISTS `prefx` (
  `id` int(11) NOT NULL,
  `Direcc` int(11) DEFAULT NULL,
  `Ciudad` int(11) DEFAULT NULL,
  `Telf1` int(11) DEFAULT NULL,
  `Telf2` int(11) DEFAULT NULL,
  `ColorFondo` int(11) DEFAULT NULL,
  `ArchGraf` int(11) DEFAULT NULL,
  `AlwaysOnTop` int(11) DEFAULT NULL,
  `AutoIni` int(11) DEFAULT NULL,
  `AutoOpenLibro` int(11) DEFAULT NULL,
  `AutoOpenFacts` int(11) DEFAULT NULL,
  `GananciaInd` int(11) DEFAULT NULL,
  `GananciaGlob` int(11) DEFAULT NULL,
  `Iva` int(11) DEFAULT NULL,
  `Ruta` int(11) DEFAULT NULL,
  `NoCartel` int(11) DEFAULT NULL,
  `RespFloppy` int(11) DEFAULT NULL,
  `RespHDD` int(11) DEFAULT NULL,
  `IngrArts` int(11) DEFAULT NULL,
  `ReAprov` int(11) DEFAULT NULL,
  `MargGan` int(11) DEFAULT NULL,
  `Pedidos` int(11) DEFAULT NULL,
  `IniFact` int(11) DEFAULT NULL,
  `PrefID` int(11) DEFAULT NULL,
  `Prefe` int(11) DEFAULT NULL,
  `Aut` int(11) DEFAULT NULL,
  `Provs` int(11) DEFAULT NULL,
  `RUC` int(11) DEFAULT NULL,
  `NombImpr` int(11) DEFAULT NULL,
  `NombreEstablecim` int(11) DEFAULT NULL,
  `LimiteInf` int(11) DEFAULT NULL,
  `NumDigsCodigo` int(11) DEFAULT NULL,
  `CtaContIngr` int(11) DEFAULT NULL,
  `CtaContIva` int(11) DEFAULT NULL,
  `Reg_Loc_Remoto` int(11) DEFAULT NULL,
  `DSN` int(11) DEFAULT NULL,
  `SecAutom` int(11) DEFAULT NULL,
  `CtaContDestIVA` int(11) DEFAULT NULL,
  `CtaContDestNeto` int(11) DEFAULT NULL,
  `IDCaja` int(11) DEFAULT NULL,
  `AlertaCaja` int(11) DEFAULT NULL,
  `vPaperSize` int(11) DEFAULT NULL,
  `vNamePaperSize` int(11) DEFAULT NULL,
  `vUserHeight` int(11) DEFAULT NULL,
  `vUserWidth` int(11) DEFAULT NULL,
  `EtiqNoRegs` int(11) DEFAULT NULL,
  `Tam` int(11) DEFAULT NULL,
  `CtaContUtilEspec` int(11) DEFAULT NULL,
  `CtaContUtilCons` int(11) DEFAULT NULL,
  `CtaContCons` int(11) DEFAULT NULL,
  `CtaContFichas` int(11) DEFAULT NULL,
  `CtaContLab` int(11) DEFAULT NULL,
  `CtaContRx` int(11) DEFAULT NULL,
  `TipoFactura` int(11) DEFAULT NULL,
  `NumInicialFacturaInstantanea` int(11) DEFAULT NULL,
  `FirstTime` int(11) DEFAULT NULL,
  `FormatoFact` int(11) DEFAULT NULL,
  `PortraitLandscape` int(11) DEFAULT NULL,
  `vSegFact` int(11) DEFAULT NULL,
  `UnidadExternaUSB` int(11) DEFAULT NULL,
  `IncluirNumRefEnFactura` int(11) DEFAULT NULL,
  `confManualLector` int(11) DEFAULT NULL,
  `FechaAutorizacionDe` int(11) DEFAULT NULL,
  `FechaAutorizacionHasta` int(11) DEFAULT NULL,
  `TipoInventario` int(11) DEFAULT NULL,
  `AccionesSiStockAgotado` int(11) DEFAULT NULL,
  `NumInicialFacturaPreimpresa` int(11) DEFAULT NULL,
  `DSNUser` int(11) DEFAULT NULL,
  `DSNPassword` int(11) DEFAULT NULL,
  `vSegClientes` int(11) DEFAULT NULL,
  `vSegInventarios` int(11) DEFAULT NULL,
  `vSegReaprovisionar` int(11) DEFAULT NULL,
  `IDPref` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.prefx: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `prefx` DISABLE KEYS */;
/*!40000 ALTER TABLE `prefx` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.prioridad_usuarios
CREATE TABLE IF NOT EXISTS `prioridad_usuarios` (
  `id_prioridad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_prioridad` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '0=Empleado',
  PRIMARY KEY (`id_prioridad`),
  UNIQUE KEY `id_grupo_UNIQUE` (`id_prioridad`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.prioridad_usuarios: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `prioridad_usuarios` DISABLE KEYS */;
INSERT INTO `prioridad_usuarios` (`id_prioridad`, `nombre_prioridad`) VALUES
	(1, 'empleado'),
	(2, 'supervisor'),
	(3, 'administrador');
/*!40000 ALTER TABLE `prioridad_usuarios` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.productoelaborado
CREATE TABLE IF NOT EXISTS `productoelaborado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDArticulo` int(11) DEFAULT NULL,
  `IDCategoria` int(11) DEFAULT NULL,
  `Articulo` int(11) DEFAULT NULL,
  `Costo` int(11) DEFAULT NULL,
  `Cantidad` int(11) DEFAULT NULL,
  `TipoTrans` int(11) DEFAULT NULL,
  `Consulta_Espec_Otro` int(11) DEFAULT NULL,
  `AplicaIva` int(11) DEFAULT NULL,
  `CtaContable` int(11) DEFAULT NULL,
  `Codigo_Impuestos` int(11) DEFAULT NULL,
  `FechaDeCompra` int(11) DEFAULT NULL,
  `Margen` int(11) DEFAULT NULL,
  `MinIndisp` int(11) DEFAULT NULL,
  `RUC` int(11) DEFAULT NULL,
  `Presentacion` int(11) DEFAULT NULL,
  `Contenido` int(11) DEFAULT NULL,
  `PVP` int(11) DEFAULT NULL,
  `Cod` int(11) DEFAULT NULL,
  `IDEstante` int(11) DEFAULT NULL,
  `AdvertirReposicion` int(11) DEFAULT NULL,
  `FechaCaducidad` int(11) DEFAULT NULL,
  `AdvertirCaducidad` int(11) DEFAULT NULL,
  `IDUser` int(11) DEFAULT NULL,
  `FechaCierreTransaccion` int(11) DEFAULT NULL,
  `TransaccionCerrada` int(11) DEFAULT NULL,
  `IDCaja` int(11) DEFAULT NULL,
  `PDMa` int(11) DEFAULT NULL,
  `PDMi` int(11) DEFAULT NULL,
  `PE1` int(11) DEFAULT NULL,
  `PE2` int(11) DEFAULT NULL,
  `IDDescuento` int(11) DEFAULT NULL,
  `ProductoElaborado` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.productoelaborado: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `productoelaborado` DISABLE KEYS */;
/*!40000 ALTER TABLE `productoelaborado` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.productos
CREATE TABLE IF NOT EXISTS `productos` (
  `IDProducto` int(11) NOT NULL AUTO_INCREMENT,
  `IDCategoria` int(11) DEFAULT NULL,
  `AlmacenCentral` int(11) DEFAULT 1,
  `Articulo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Costo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cantidad` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TipoTrans` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Consulta_Espec_Otro` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AplicaIva` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CtaContable` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Codigo_Impuestos` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FechaDeCompra` date DEFAULT NULL,
  `Margen` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MinIndisp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RUC` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Presentacion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Contenido` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PVP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cod` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDEstante` int(11) DEFAULT NULL,
  `AdvertirReposicion` int(11) DEFAULT NULL,
  `FechaCaducidad` date DEFAULT NULL,
  `AdvertirCaducidad` int(11) DEFAULT NULL,
  `IDUser` int(11) DEFAULT NULL,
  `FechaCierreTransaccion` date DEFAULT NULL,
  `TransaccionCerrada` int(11) DEFAULT NULL,
  `IDCaja` int(11) DEFAULT NULL,
  `PDMa` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PDMi` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PE1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PE2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDDescuento` int(11) DEFAULT NULL,
  `ProductoElaborado` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CodComponenteProductoElaborado` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Antelacion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AlfaNumericoPosEstante` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `IDCatalogo` bigint(20) DEFAULT NULL,
  `IDInventario` bigint(20) DEFAULT NULL,
  `IDProv` int(11) DEFAULT NULL,
  `Codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Código alfanumérico de producto. Estándar: 13 dígitos sin letras, pero el código admitirá letras y números',
  `Nombre` varchar(450) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CostoPrevio` decimal(15,2) DEFAULT NULL,
  `CostoActual` decimal(15,2) DEFAULT NULL,
  `Porcent_Ajuste_Inflacion` int(11) DEFAULT NULL COMMENT 'ajuste porcentual al costo por inflación',
  `Frecuencia_Ajuste_Porcentual` int(11) DEFAULT NULL COMMENT '1 diario, 2 semanal, 3 mensual, 4 trimestral, 5 semestral y 6 anual. CERO (0) si no hay necesidad de ajuste',
  `CodigoImpuesto1` int(11) DEFAULT NULL COMMENT '1 IVA',
  `CodigoImpuesto2` int(11) DEFAULT NULL,
  `CodigoImpuesto3` int(11) DEFAULT NULL,
  `Inventario_IDInventario` bigint(20) DEFAULT NULL,
  `Inventario_Catalogo_de_Inventarios_IDCatalogo` bigint(20) DEFAULT NULL,
  `FechaCompra` date DEFAULT NULL,
  `AdveritReposicion` int(11) DEFAULT NULL,
  `Minimo_Para_Reposicion` int(11) DEFAULT NULL,
  `MargenGanancia` int(11) DEFAULT NULL COMMENT 'Valor porcentual para determinar el nivel de ganacia de cada producto',
  `MargenGlobalAjuste` int(11) DEFAULT NULL COMMENT 'Margen porcentual para modificar globalmente TODOS LOS PRECIOS de la tabla PRECIOS. ',
  `NivelEstante` int(11) DEFAULT NULL COMMENT 'en qué fila de la estantería está ubicado el producto',
  `ColumnaEstante` int(11) DEFAULT NULL COMMENT 'en qué columna se encuentra, considerando que cada producto equivale a una columna.',
  `RutaImagenCodigoDeBarras` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RutaImagenProducto` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IDProducto`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.productos: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` (`IDProducto`, `IDCategoria`, `AlmacenCentral`, `Articulo`, `Costo`, `Cantidad`, `TipoTrans`, `Consulta_Espec_Otro`, `AplicaIva`, `CtaContable`, `Codigo_Impuestos`, `FechaDeCompra`, `Margen`, `MinIndisp`, `RUC`, `Presentacion`, `Contenido`, `PVP`, `Cod`, `IDEstante`, `AdvertirReposicion`, `FechaCaducidad`, `AdvertirCaducidad`, `IDUser`, `FechaCierreTransaccion`, `TransaccionCerrada`, `IDCaja`, `PDMa`, `PDMi`, `PE1`, `PE2`, `IDDescuento`, `ProductoElaborado`, `CodComponenteProductoElaborado`, `Antelacion`, `AlfaNumericoPosEstante`, `created_at`, `updated_at`, `IDCatalogo`, `IDInventario`, `IDProv`, `Codigo`, `Nombre`, `CostoPrevio`, `CostoActual`, `Porcent_Ajuste_Inflacion`, `Frecuencia_Ajuste_Porcentual`, `CodigoImpuesto1`, `CodigoImpuesto2`, `CodigoImpuesto3`, `Inventario_IDInventario`, `Inventario_Catalogo_de_Inventarios_IDCatalogo`, `FechaCompra`, `AdveritReposicion`, `Minimo_Para_Reposicion`, `MargenGanancia`, `MargenGlobalAjuste`, `NivelEstante`, `ColumnaEstante`, `RutaImagenCodigoDeBarras`, `RutaImagenProducto`) VALUES
	(1, 1, 0, 'carro', '222', '12', 'normal', 'ok', 'no', '213134i3285783', NULL, '2019-02-01', '12', '12', '84327892475', 'hola', 'hola', '12', '1234', 2, 1, '2019-02-01', 1, NULL, '2019-02-01', NULL, NULL, '1', '1', '1', '1', NULL, NULL, '1', 'si', '12klsld', '2019-05-10 16:14:27', '2019-06-17 19:12:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 1, 1, 'carro', '222', '12', 'normal', 'ok', 'no', '213134i3285783', NULL, '2019-02-01', '12', '12', '84327892475', 'hola', 'hola', '12', '1234', 2, 1, '2019-02-01', 1, NULL, '2019-02-01', NULL, NULL, '1', '1', '1', '1', NULL, NULL, '1', 'si', '12klsld', '2019-05-10 16:14:27', '2019-06-17 19:12:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 1, 1, 'carro', '222', '12', 'normal', 'ok', 'no', '213134i3285783', NULL, '2019-02-01', '12', '12', '84327892475', 'hola', 'hola', '12', '1234', 2, 1, '2019-02-01', 1, NULL, '2019-02-01', NULL, NULL, '1', '1', '1', '1', NULL, NULL, '1', 'si', '12klsld', '2019-05-10 16:14:27', '2019-06-17 19:12:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 1, 0, 'telefono', '150', '20', '8', 'jskldjfkld', 'si', '8947589437583', NULL, '2019-06-22', '3', '2', '8073458734', 'dksjfklsjfkl', '1 telefono', '23', '9843983', 1, 1, '2019-06-29', 1, NULL, '2019-06-30', NULL, NULL, 'sdkfjsdklfsj', 'dksfjskdj', 'kjsdkajd', 'kdsfsdkfj', NULL, NULL, '98439058309kljdf', 'skdfjksdfj', 'sdkjfksdjfkls', '2019-06-22 14:44:58', '2019-06-22 15:14:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.proformas
CREATE TABLE IF NOT EXISTS `proformas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDProforma` int(11) DEFAULT NULL,
  `Factura_Id` int(11) DEFAULT NULL,
  `Cantidad` int(11) DEFAULT NULL,
  `Descripcion` int(11) DEFAULT NULL,
  `Valor_Unitario` int(11) DEFAULT NULL,
  `Valor_Total` int(11) DEFAULT NULL,
  `Anulada` int(11) DEFAULT NULL,
  `Fecha` int(11) DEFAULT NULL,
  `Hora` int(11) DEFAULT NULL,
  `Cod` int(11) DEFAULT NULL,
  `NumCaja` int(11) DEFAULT NULL,
  `FactAbierta` int(11) DEFAULT NULL,
  `Terminos` int(11) DEFAULT NULL,
  `IDCliente` int(11) DEFAULT NULL,
  `Referencia` int(11) DEFAULT NULL,
  `IVA` int(11) DEFAULT NULL,
  `TipoTrans` int(11) DEFAULT NULL,
  `TipoFactura` int(11) DEFAULT NULL,
  `IVA_Luego_Descuento` int(11) DEFAULT NULL,
  `IDPrecioDistribuidor` int(11) DEFAULT NULL,
  `VTotal_luego_descuento` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.proformas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `proformas` DISABLE KEYS */;
/*!40000 ALTER TABLE `proformas` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.proveedores
CREATE TABLE IF NOT EXISTS `proveedores` (
  `IDProv` int(11) NOT NULL AUTO_INCREMENT,
  `IDAlmacen` int(11) DEFAULT NULL,
  `RazonSocial` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `RUC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelfPrincipal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vendedor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelfVendedor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Banco1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NumCtaCorriente1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NumCtaAhorros1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Banco2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NumCtaCorriente2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NumCtaAhorros2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Notas` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Ciudad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Proveedor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Direccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Comentarios` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CtaContable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDEsp` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`IDProv`),
  KEY `FK_proveedores_almacen` (`IDAlmacen`),
  CONSTRAINT `FK_proveedores_almacen` FOREIGN KEY (`IDAlmacen`) REFERENCES `almacen` (`IDAlmacen`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.proveedores: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` (`IDProv`, `IDAlmacen`, `RazonSocial`, `RUC`, `TelfPrincipal`, `Email`, `Vendedor`, `TelfVendedor`, `Banco1`, `NumCtaCorriente1`, `NumCtaAhorros1`, `Banco2`, `NumCtaCorriente2`, `NumCtaAhorros2`, `Notas`, `Ciudad`, `Proveedor`, `Direccion`, `Telefono`, `Comentarios`, `CtaContable`, `IDEsp`, `created_at`, `updated_at`) VALUES
	(1, 1, 'proveedor', '32423423434', '8743289537458', 'proveedor@gmail.com', 'Guillermo Hernandez', '8978942379843', 'banco1', '0942850924850983494', '384573905893809034', 'banco2', '8947589347983475988', '9080948390845909834', 'HOLA', NULL, NULL, 'casa', NULL, 'hola', '893475897349857349', NULL, '2019-06-21 15:13:11', '2019-06-21 15:22:14');
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.proveedores_has_productos
CREATE TABLE IF NOT EXISTS `proveedores_has_productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Proveedores_IDProv` int(11) DEFAULT NULL,
  `Productos_IDProducto` int(11) DEFAULT NULL,
  `Productos_Inventario_IDInventario` int(11) DEFAULT NULL,
  `Productos_Inventario_Catalogo_de_Inventarios_IDCatalogo` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.proveedores_has_productos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `proveedores_has_productos` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedores_has_productos` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.proveedores_nuevo_diseño
CREATE TABLE IF NOT EXISTS `proveedores_nuevo_diseño` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ruc` bigint(20) NOT NULL,
  `razonsocial` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telfprincipal` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notas` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendedorcontacto` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tlfvendedorcontacto` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ctacontable` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombrebanco1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numctacorriente1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numctaahorros1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombrebanco2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numctacorriente2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numctaahorros2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.proveedores_nuevo_diseño: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `proveedores_nuevo_diseño` DISABLE KEYS */;
INSERT INTO `proveedores_nuevo_diseño` (`id`, `ruc`, `razonsocial`, `direccion`, `telfprincipal`, `notas`, `email`, `vendedorcontacto`, `tlfvendedorcontacto`, `comentario`, `ctacontable`, `nombrebanco1`, `numctacorriente1`, `numctaahorros1`, `nombrebanco2`, `numctacorriente2`, `numctaahorros2`, `created_at`, `updated_at`) VALUES
	(4, 4235235678, 'fdfgdsfsd', 'cxvxcvxc', '9094584309', 'ulskdfjsd', 'scienceguillermo@gmail.com', 'jksdajñsal', '3209480923', 'dksfmñkdlsaf', '90438950483098509', 'dkjafñklasj', '897987', NULL, 'jkhkuhkjh', '897987', '878989', '2019-04-29 20:40:06', '2019-04-29 20:56:10'),
	(5, 123456789, 'proveedor1', 'casa', '04140484765', 'sdjhfsdhfk', 'scienceguillermo@gmail.com', 'sergio', '04140484765', 'hola', '8734982374982374982749823', 'banco1', '43878934759438759384798375983', '83247928374982749827482379842734', 'banco2', '4287928437398475983427589', '48973498759384759834', '2019-05-05 23:36:11', '2019-05-05 23:36:11');
/*!40000 ALTER TABLE `proveedores_nuevo_diseño` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.prov_estado
CREATE TABLE IF NOT EXISTS `prov_estado` (
  `IDProv_Estado` int(11) NOT NULL AUTO_INCREMENT,
  `IDNacion` int(11) DEFAULT NULL,
  `NombreProv_Estado` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AbreviacionProvEstado` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PosicionGeografica` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Nacion_IDNacion` int(11) NOT NULL,
  PRIMARY KEY (`IDProv_Estado`,`Nacion_IDNacion`),
  UNIQUE KEY `IDProv_Estado_UNIQUE` (`IDProv_Estado`),
  KEY `fk_Prov_Estado_Nacion1_idx` (`Nacion_IDNacion`),
  CONSTRAINT `fk_Prov_Estado_Nacion1` FOREIGN KEY (`Nacion_IDNacion`) REFERENCES `nacion` (`IDNacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.prov_estado: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `prov_estado` DISABLE KEYS */;
/*!40000 ALTER TABLE `prov_estado` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.red
CREATE TABLE IF NOT EXISTS `red` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDReg` int(11) DEFAULT NULL,
  `strMACAddress` int(11) DEFAULT NULL,
  `strRegFinal` int(11) DEFAULT NULL,
  `Version` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.red: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `red` DISABLE KEYS */;
/*!40000 ALTER TABLE `red` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.registarnotaventa
CREATE TABLE IF NOT EXISTS `registarnotaventa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDReg` int(11) DEFAULT NULL,
  `strMACAddress` int(11) DEFAULT NULL,
  `strRegFinal` int(11) DEFAULT NULL,
  `Version` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.registarnotaventa: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `registarnotaventa` DISABLE KEYS */;
/*!40000 ALTER TABLE `registarnotaventa` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.salarios
CREATE TABLE IF NOT EXISTS `salarios` (
  `IDSalario` int(11) NOT NULL AUTO_INCREMENT,
  `NombreSalario` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ValorSalario` decimal(28,0) DEFAULT NULL,
  `IDUsuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`IDSalario`),
  UNIQUE KEY `IDSalarios_UNIQUE` (`IDSalario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.salarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `salarios` DISABLE KEYS */;
INSERT INTO `salarios` (`IDSalario`, `NombreSalario`, `ValorSalario`, `IDUsuario`) VALUES
	(1, 'Indefinido', 0, 1);
/*!40000 ALTER TABLE `salarios` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.sec
CREATE TABLE IF NOT EXISTS `sec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TipoUsuario` int(11) DEFAULT NULL,
  `TipoHorario` int(11) DEFAULT NULL,
  `User` int(11) DEFAULT NULL,
  `Pass` int(11) DEFAULT NULL,
  `ComponentSis` int(11) DEFAULT NULL,
  `HoraEntr` int(11) DEFAULT NULL,
  `HoraSal` int(11) DEFAULT NULL,
  `SecID` int(11) DEFAULT NULL,
  `Prioridad` int(11) DEFAULT NULL,
  `FechaIngr` int(11) DEFAULT NULL,
  `IDUser` int(11) DEFAULT NULL,
  `IDHor` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.sec: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `sec` DISABLE KEYS */;
/*!40000 ALTER TABLE `sec` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.sysop
CREATE TABLE IF NOT EXISTS `sysop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SYSOP` int(11) DEFAULT NULL,
  `Signature` int(11) DEFAULT NULL,
  `NIAC` int(11) DEFAULT NULL,
  `Fecha` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.sysop: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `sysop` DISABLE KEYS */;
/*!40000 ALTER TABLE `sysop` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.tema_color
CREATE TABLE IF NOT EXISTS `tema_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `color` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.tema_color: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tema_color` DISABLE KEYS */;
INSERT INTO `tema_color` (`id`, `user_id`, `color`, `created_at`, `updated_at`) VALUES
	(1, 1, '6', NULL, '2019-05-06 01:19:35'),
	(2, NULL, '6', '2019-06-23 21:16:49', '2019-06-23 21:16:49');
/*!40000 ALTER TABLE `tema_color` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.tipofrm
CREATE TABLE IF NOT EXISTS `tipofrm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Fabricante` int(11) DEFAULT NULL,
  `Indx` int(11) DEFAULT NULL,
  `IDFrm` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.tipofrm: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tipofrm` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipofrm` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.tiposesps
CREATE TABLE IF NOT EXISTS `tiposesps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDTipEsp` int(11) DEFAULT NULL,
  `NombreTipEsp` int(11) DEFAULT NULL,
  `CtaContable` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.tiposesps: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tiposesps` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiposesps` ENABLE KEYS */;

-- Volcando estructura para tabla dbalmacenv2.users
CREATE TABLE IF NOT EXISTS `users` (
  `IDUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `Foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user.jpg',
  `Apellidos` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Nombres` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Edad` int(11) DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EmailPersonal` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EmailEmpresarial` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Direccion` varchar(545) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelfCasa` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TelfCelular` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RutaFoto` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Genero` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'M= Masculino',
  `FechaNacimiento` date DEFAULT NULL,
  `FechaContratacion` date DEFAULT NULL,
  `CedulaRUC` varchar(19) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PWD` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Contraseña',
  `EstadoCivil` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'S = soltero',
  `Conyuge` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nombre de cónyuge',
  `ProfesionOficio` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NumCargasFamiliares` int(11) DEFAULT NULL,
  `NumHijos` int(11) DEFAULT NULL,
  `Comentarios` varchar(1250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDEmpresa` int(11) DEFAULT NULL,
  `IDHorario` int(11) DEFAULT NULL,
  `Cargos_IDCargo` int(10) unsigned NOT NULL DEFAULT 1,
  `Salarios_IDSalario` int(11) NOT NULL DEFAULT 1,
  `id_prioridad` int(11) DEFAULT NULL,
  `prioridad_users_id_prioridad` int(11) NOT NULL DEFAULT 1,
  `info_completa` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IDUsuario`,`Cargos_IDCargo`,`Salarios_IDSalario`,`prioridad_users_id_prioridad`),
  UNIQUE KEY `idPerfil_UNIQUE` (`IDUsuario`),
  KEY `fk_users_Cargos1_idx` (`Cargos_IDCargo`),
  KEY `fk_users_Salarios1_idx` (`Salarios_IDSalario`),
  KEY `fk_users_prioridad_users1_idx` (`prioridad_users_id_prioridad`),
  CONSTRAINT `fk_users_Cargos1` FOREIGN KEY (`Cargos_IDCargo`) REFERENCES `cargos` (`IDCargo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_Salarios1` FOREIGN KEY (`Salarios_IDSalario`) REFERENCES `salarios` (`IDSalario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_prioridad_users1` FOREIGN KEY (`prioridad_users_id_prioridad`) REFERENCES `prioridad_usuarios` (`id_prioridad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla dbalmacenv2.users: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`IDUsuario`, `Foto`, `Apellidos`, `Nombres`, `Edad`, `email`, `email_verified_at`, `password`, `remember_token`, `EmailPersonal`, `EmailEmpresarial`, `Direccion`, `TelfCasa`, `TelfCelular`, `RutaFoto`, `Genero`, `FechaNacimiento`, `FechaContratacion`, `CedulaRUC`, `PWD`, `EstadoCivil`, `Conyuge`, `ProfesionOficio`, `NumCargasFamiliares`, `NumHijos`, `Comentarios`, `IDEmpresa`, `IDHorario`, `Cargos_IDCargo`, `Salarios_IDSalario`, `id_prioridad`, `prioridad_users_id_prioridad`, `info_completa`, `created_at`, `updated_at`) VALUES
	(5, 'user.jpg', 'Hernandez Centeno', 'Guillermo Antonio', 26, 'admin@gmail.com', NULL, '$2y$10$jTfly1Uydx.QIO91ObtDqeFbIUAuQP91zWyKhDtJNCYKmjGtewDaS', NULL, 'scienceguillermo@gmail.com', 'scienceguillermo@gmail.com', 'casa', '02465685596', '04140484765', NULL, 'M', '1993-03-11', NULL, '20587620', NULL, 'F', 'Maryelis Mendoza', 'Ing: Informatica', 2, 1, 'trabajando', 1, NULL, 1, 1, NULL, 1, 0, '2019-06-15 22:19:48', '2019-06-17 18:11:06');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
