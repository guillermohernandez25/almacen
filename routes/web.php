<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Auth::routes();*/

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
/*Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');*/

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

// Email Verification Routes...
Route::emailVerification();



Route::get('/home',['as'=>'home','uses'=>'HomeController@index']);
Route::get('/home/{id}',['as'=>'home_select','uses'=>'HomeController@nuevapag']);

Route::post('/color/store',['as'=>'color_store','uses'=>'ColorController@store']);
Route::get('/color/create',['as'=>'color','uses'=>'ColorController@create']);

Route::get('/empresa/',['as'=>'empresa','uses'=>'EmpresaController@index']);
Route::post('/empresa/store',['as'=>'empresa-store-A','uses'=>'EmpresaController@store']);
Route::post('/empresa/update/{id}',['as'=>'empresa-update-A','uses'=>'EmpresaController@update']);
Route::get('/empresa/status/{id}',['as'=>'empresa-status-A','uses'=>'EmpresaController@status']);
Route::get('/empresa/edit/{id}',['as'=>'empresa-edit-A','uses'=>'EmpresaController@edit']);
Route::get('/empresa/create',['as'=>'empresa-create-A','uses'=>'EmpresaController@create']);
Route::get('/empresa/destroy/{id}',['as'=>'empresa-destroy-A','uses'=>'EmpresaController@destroy']);    

Route::get('/proveedor/',['as'=>'proveedor','uses'=>'ProviderController@index']);
Route::post('/proveedor/store',['as'=>'proveedor-store-A','uses'=>'ProviderController@store']);
Route::post('/proveedor/update/{id}',['as'=>'proveedor-update-A','uses'=>'ProviderController@update']);
Route::get('/proveedor/status/{id}',['as'=>'proveedor-status-A','uses'=>'ProviderController@status']);
Route::get('/proveedor/edit/{id}',['as'=>'proveedor-edit-A','uses'=>'ProviderController@edit']);
Route::get('/proveedor/create',['as'=>'proveedor-create-A','uses'=>'ProviderController@create']);
Route::get('/proveedor/destroy/{id}',['as'=>'proveedor-destroy-A','uses'=>'ProviderController@destroy']);

Route::get('/administradores/',['as'=>'administradores','uses'=>'AdministradoresController@index']);
Route::post('/administradores/store',['as'=>'administradores-store-A','uses'=>'AdministradoresController@store']);
Route::post('/administradores/update/{id}',['as'=>'administradores-update-A','uses'=>'AdministradoresController@update']);
Route::get('/administradores/status/{id}',['as'=>'administradores-status-A','uses'=>'AdministradoresController@status']);
Route::get('/administradores/edit/{id}',['as'=>'administradores-edit-A','uses'=>'AdministradoresController@edit']);
Route::get('/administradores/create',['as'=>'administradores-create-A','uses'=>'AdministradoresController@create']);
Route::get('/administradores/destroy/{id}',['as'=>'administradores-destroy-A','uses'=>'AdministradoresController@destroy']);

Route::get('/almacenes/',['as'=>'almacenes','uses'=>'AlmacenController@index']);
Route::post('/almacenes/store',['as'=>'almacenes-store-A','uses'=>'AlmacenController@store']);
Route::post('/almacenes/update/{id}',['as'=>'almacenes-update-A','uses'=>'AlmacenController@update']);
Route::get('/almacenes/status/{id}',['as'=>'almacenes-status-A','uses'=>'AlmacenController@status']);
Route::get('/almacenes/almacen/{id}',['as'=>'almacenes-almacen-A','uses'=>'AlmacenController@almacen']);
Route::get('/almacenes/edit/{id}',['as'=>'almacenes-edit-A','uses'=>'AlmacenController@edit']);
Route::get('/almacenes/create',['as'=>'almacenes-create-A','uses'=>'AlmacenController@create']);
Route::get('/almacenes/destroy/{id}',['as'=>'almacenes-destroy-A','uses'=>'AlmacenController@destroy']);
Route::get('/almacenes/administradores/{id}',['as'=>'almacenes-find-A','uses'=>'AlmacenController@administradores']);
Route::get('/almacenes/ventas/{id?}',['as'=>'almacenes-ventas-A','uses'=>'AlmacenController@ventas']);

Route::get('/categoriaproducto/',['as'=>'categoriaproducto','uses'=>'CategoriaProductosController@index']);
Route::post('/categoriaproducto/store',['as'=>'categoriaproducto-store-A','uses'=>'CategoriaProductosController@store']);
Route::post('/categoriaproducto/update/{id}',['as'=>'categoriaproducto-update-A','uses'=>'CategoriaProductosController@update']);
Route::get('/categoriaproducto/status/{id}',['as'=>'categoriaproducto-status-A','uses'=>'CategoriaProductosController@status']);
Route::get('/categoriaproducto/edit/{id}',['as'=>'categoriaproducto-edit-A','uses'=>'CategoriaProductosController@edit']);
Route::get('/categoriaproducto/create',['as'=>'categoriaproducto-create-A','uses'=>'CategoriaProductosController@create']);
Route::get('/categoriaproducto/destroy/{id}',['as'=>'categoriaproducto-destroy-A','uses'=>'CategoriaProductosController@destroy']);
Route::get('/categoriaproducto/administradores/{id}',['as'=>'categoriaproducto-find-A','uses'=>'CategoriaProductosController@administradores']);

Route::get('/producto/',['as'=>'producto','uses'=>'ProductosController@index']);
Route::get('/productoslow/',['as'=>'productoslow','uses'=>'ProductosController@indexslow']);
Route::get('/c_almacen/{id}',['as'=>'c-almacen','uses'=>'ProductosController@c_almacen']);
Route::get('/c_inventario/{id}',['as'=>'c-inventario','uses'=>'ProductosController@c_inventario']);
Route::post('/producto/store',['as'=>'producto-store-A','uses'=>'ProductosController@store']);
Route::post('/producto/update/{id}',['as'=>'producto-update-A','uses'=>'ProductosController@update']);
Route::get('/producto/status/{id}',['as'=>'producto-status-A','uses'=>'ProductosController@status']);
Route::get('/producto/edit/{id}',['as'=>'producto-edit-A','uses'=>'ProductosController@edit']);
Route::get('/producto/create',['as'=>'producto-create-A','uses'=>'ProductosController@create']);
Route::get('/producto/destroy/{id}',['as'=>'producto-destroy-A','uses'=>'ProductosController@destroy']);
Route::get('/producto/administradores/{id}',['as'=>'producto-find-A','uses'=>'ProductosController@administradores']);

Route::get('/inventario/',['as'=>'inventario','uses'=>'InventariosController@index']);
Route::post('/inventario/store',['as'=>'inventario-store-A','uses'=>'InventariosController@store']);
Route::post('/inventario/update/{id}',['as'=>'inventario-update-A','uses'=>'InventariosController@update']);
Route::get('/inventario/ver/{id}',['as'=>'inventario-ver-A','uses'=>'InventariosController@ver']);
Route::get('/inventario/edit/{id}',['as'=>'inventario-edit-A','uses'=>'InventariosController@edit']);
Route::get('/inventario/create',['as'=>'inventario-create-A','uses'=>'InventariosController@create']);
Route::get('/inventario/asignacion',['as'=>'inventario-asignacion-A','uses'=>'InventariosController@asignacion']);
Route::get('/inventario/asignacion/excel',['as'=>'inventario-asignacion-excel-A','uses'=>'InventariosController@asignacion_excel']);
Route::get('/inventario/consulinventario/{id}',['as'=>'inventario-consulinventario-A','uses'=>'InventariosController@consulinventario']);
Route::get('/inventario/consularticulo/{id}',['as'=>'inventario-consularticulo-A','uses'=>'InventariosController@consularticulo']);
Route::post('/inventario/asignacionstore',['as'=>'inventario-asignacionstore-A','uses'=>'InventariosController@asignacionstore']);
Route::post('/inventario/asignacionstore/excel',['as'=>'inventario-asignacionstore-excel-A','uses'=>'InventariosController@asignacionstore_excel']);
Route::get('/inventario/destroy/{id}',['as'=>'inventario-destroy-A','uses'=>'InventariosController@destroy']);
Route::get('/inventario/administradores/{id}',['as'=>'inventario-find-A','uses'=>'InventariosController@administradores']);

Route::get('/clientes/',['as'=>'clientes','uses'=>'ClientesController@index']);
Route::get('/clientes/almacen/{id}',['as'=>'clientes-almacen','uses'=>'ClientesController@clientes_almacen']);
Route::post('/clientes/store',['as'=>'clientes-store-A','uses'=>'ClientesController@store']);
Route::post('/clientes/update/{id}',['as'=>'clientes-update-A','uses'=>'ClientesController@update']);
Route::get('/clientes/status/{id}',['as'=>'clientes-status-A','uses'=>'ClientesController@status']);
Route::get('/clientes/edit/{id}',['as'=>'clientes-edit-A','uses'=>'ClientesController@edit']);
Route::get('/clientes/create',['as'=>'clientes-create-A','uses'=>'ClientesController@create']);
Route::get('/clientes/destroy/{id}',['as'=>'clientes-destroy-A','uses'=>'ClientesController@destroy']);

Route::get('/compras/',['as'=>'compras','uses'=>'CompraController@index']);
Route::post('/compras/store',['as'=>'compras-store-A','uses'=>'CompraController@store']);
Route::post('/compras/update/{id}',['as'=>'compras-update-A','uses'=>'CompraController@update']);
Route::get('/compras/status/{id}',['as'=>'compras-status-A','uses'=>'CompraController@status']);
Route::get('/compras/edit/{id}',['as'=>'compras-edit-A','uses'=>'CompraController@edit']);
Route::get('/compras/create',['as'=>'compras-create-A','uses'=>'CompraController@create']);
Route::get('/compras/destroy/{id}',['as'=>'compras-destroy-A','uses'=>'CompraController@destroy']);

Route::get('/ventas',['as'=>'ventas','uses'=>'VentaController@index']);
Route::post('/ventas/store',['as'=>'ventas-store-A','uses'=>'VentaController@store']);
Route::post('/ventas/update/{id}',['as'=>'ventas-update-A','uses'=>'VentaController@update']);
Route::get('/ventas/status/{id}',['as'=>'ventas-status-A','uses'=>'VentaController@status']);
Route::get('/ventas/show/{id}',['as'=>'ventas-show-A','uses'=>'VentaController@show']);
Route::get('/ventas/create',['as'=>'ventas-create-A','uses'=>'VentaController@create']);
Route::get('/ventas/destroy/{id}',['as'=>'ventas-destroy-A','uses'=>'VentaController@destroy']);


Route::get('/perfil/',['as'=>'perfil','uses'=>'PerfilController@index']);
Route::post('/perfil/store',['as'=>'perfil-store-A','uses'=>'PerfilController@store']);
Route::post('/perfil/update',['as'=>'perfil-update-A','uses'=>'PerfilController@update']);
Route::get('/perfil/status/{id}',['as'=>'perfil-status-A','uses'=>'PerfilController@status']);
Route::get('/perfil/edit',['as'=>'perfil-edit-A','uses'=>'PerfilController@edit']);
Route::get('/perfil/create',['as'=>'perfil-create-A','uses'=>'PerfilController@create']);
Route::get('/perfil/destroy/{id}',['as'=>'perfil-destroy-A','uses'=>'PerfilController@destroy']);
Route::post('/perfil/update-image/{id}',['as'=>'perfil-update-image-A','uses'=>'PerfilController@updateimage']);

Route::get('/galmacen/',['as'=>'galmacen','uses'=>'AlmacenGeneralController@index']);
Route::post('/galmacen/store',['as'=>'galmacen-store-A','uses'=>'AlmacenGeneralController@store']);
Route::post('/galmacen/update/{id}',['as'=>'galmacen-update-A','uses'=>'AlmacenGeneralController@update']);
Route::get('/galmacen/status/{id}',['as'=>'galmacen-status-A','uses'=>'AlmacenGeneralController@status']);
Route::get('/galmacen/show/{id}',['as'=>'galmacen-show-A','uses'=>'AlmacenGeneralController@show']);
Route::get('/galmacen/create',['as'=>'galmacen-create-A','uses'=>'AlmacenGeneralController@create']);
Route::get('/galmacen/destroy/{id}',['as'=>'galmacen-destroy-A','uses'=>'AlmacenGeneralController@destroy']);

Route::get('/inhabilitar',['as'=>'inhabilitar','uses'=>'InhabilitarController@index']);
Route::get('/inhabilitar/create',['as'=>'inhabilitar_create','uses'=>'InhabilitarController@user_create']);
Route::get('/inhabilitar/{id}',['as'=>'inhabilitar_status','uses'=>'InhabilitarController@status']);
Route::get('/inhabilitar/edit/{id}',['as'=>'inhabilitar_edit','uses'=>'InhabilitarController@user_edit']);
Route::post('/inhabilitar/update/{id}',['as'=>'inhabilitar_update','uses'=>'InhabilitarController@user_update']);
Route::post('/inhabilitar/store',['as'=>'inhabilitar_store','uses'=>'InhabilitarController@user_store']);

Route::get('/roles',['as'=>'roles','uses'=>'RolesController@index']);
Route::get('/roles/create',['as'=>'roles_create','uses'=>'RolesController@create']);
Route::get('/roles/{id}',['as'=>'roles_status','uses'=>'RolesController@status']);
Route::post('/roles/edit/{id}',['as'=>'roles_edit','uses'=>'RolesController@edit']);
Route::post('/roles/update/{id}',['as'=>'roles_update','uses'=>'RolesController@update']);
Route::post('/roles/store',['as'=>'roles_store','uses'=>'RolesController@store']);



Route::get('/mensajes/recibidos',['as'=>'mensajes-recibidos','uses'=>'MensajesController@recibidos']);
Route::get('/mensajes/contador',['as'=>'mensajes-contador','uses'=>'MensajesController@contador']);
Route::get('/mensajes/enviados',['as'=>'mensajes-enviados','uses'=>'MensajesController@enviados']);
Route::get('/mensajes/leer/recibidos/{id}',['as'=>'mensajes-leer-recibidos','uses'=>'MensajesController@leer_recibido']);
Route::get('/mensajes/leer/enviados/{id}',['as'=>'mensajes-leer-enviados','uses'=>'MensajesController@leer_enviado']);
Route::post('/mensajes/store',['as'=>'mensajes-store','uses'=>'MensajesController@store']);
Route::post('/mensajes/update/{id}',['as'=>'mensajes-update','uses'=>'MensajesController@update']);
Route::get('/mensajes/status/{id}',['as'=>'mensajes-status','uses'=>'MensajesController@status']);
Route::get('/mensajes/edit/{id}',['as'=>'mensajes-edit','uses'=>'MensajesController@edit']);
Route::get('/mensajes/nuevomensaje',['as'=>'mensajes-create','uses'=>'MensajesController@create']);
Route::get('/mensajes/destroy/{id}',['as'=>'mensajes-destroy','uses'=>'MensajesController@destroy']);



Route::resource('/kardex', 'KardexController');
Route::resource('/kardexout', 'RegSalidaController');
Route::resource('/kardexin', 'RegEntradaController');

