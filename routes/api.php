<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
    Route::get('buskalmacenes/{empresa}', 'UploadController@consulta_almacen');
    Route::get('download/{empresa}/{almacen?}', 'UploadController@download');
    Route::post('upload', 'UploadController@upload');
