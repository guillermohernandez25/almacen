@extends('layouts.appmod')

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            // Bar chart
            var total = $('#cantidad').val();
            //alert(total);
            var i = 1;
                <?php $i = 0; ?>
            for (i ; i < (parseInt(total)+parseInt(1)) ; i ++){


            //console.log("hola"+informacion);
            new Chart(document.getElementsByName('barchart'+parseInt(i)), {

                type: 'bar',
                data: {
                    labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                    datasets: [
                        {
                            label: "Niveles de Ventas",
                            backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd", "#8e5ea2",],
                            data: <?php echo(json_encode($mensual_ventas_almacen[$i])); ?>
                        }

                    ]
                },
                options: {
                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'Niveles de Ventas'
                    }
                }

            });

            }

        })
    </script>
    <script>
        $(function () {
            $('.ultimas').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 5,
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
            $('.stock').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 5,
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
            $('.compras').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 5,
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
    <script>
            $(document).ready(function(){
                var actual = $('#actual').val();
                var total = $('#cantidad').val();
                $('#'+actual).prop('hidden',false);
                if(actual  == total ){
                    $('.adelante').hide();
                }
                $('.atras').hide();
            });
            $('.adelante').click(function(e){
                e.preventDefault();
                var anterior = $('#actual').val();
                $('#'+anterior).prop('hidden',true);
                var actual = parseInt($('#actual').val()) + parseInt(1);
                $('#actual').val(actual);
                $('#'+actual).prop('hidden',false);
                $('.atras').show();
                var total = $('#cantidad').val();
                if(total == actual){
                    $('.adelante').hide();
                }
            });

            $('.atras').click(function(e){
                e.preventDefault();
                var anterior = $('#actual').val();
                $('#'+anterior).prop('hidden',true);
                var actual = parseInt($('#actual').val()) - parseInt(1);
                $('#actual').val(actual);
                $('#'+actual).prop('hidden',false);
                $('.adelante').show();
                var total = $('#cantidad').val();
                if(actual == 1){
                    $('.atras').hide();
                }
            });

    </script>
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
        <input type="text" id="cantidad" value="{{sizeof($almacenes)}}" style="display: none;">
        <input type="text" id="actual" value="1" style="display: none;">
        <div class="row">
            <div style="align-content: center; align-items: center; text-align: center;">
                <div class="col-md-2"></div>
                <div class="col-md-4"><button class=" btn btn-block btn-xs btn-linkedin atras"><i class="fa fa-chevron-left"></i></button></div>

                <div class="col-md-4"><button class=" btn btn-block btn-xs btn-linkedin adelante"><i class="fa fa-chevron-right"></i></button></div>
                <div class="col-md-2"></div>
            </div>
            <hr>
        </div>

        <?php
        $count = 1;
        ?>
        @for($i = 0; $i < sizeof($almacenes); $i++)
            <input style="display: none;" type="text" class="grafico{{$count}}" id="grafico{{$count}}" value="{{json_encode($mensual_ventas_almacen[$i])}}">
            <div id="{{$count}}" hidden>
                <div style="align-items: center; align-content: center; text-align: center;">
                    <h4>
                        <strong>
                            Nombre de Almacen: {{$almacenes[$i]->nombre}}, Descripción: {{$almacenes[$i]->descripcion}}.
                        </strong>
                    </h4>
                </div>
                <div class="row">

                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{$count_factura_proveedor[$i]}}</h3>

                                <p>Compras</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="{{route('compras')}}" class="small-box-footer">Ver Comparas <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>{{$articulos_almacen[$i]}}<sup style="font-size: 20px"></sup></h3>

                                <p>Productos</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            @if(isset($almacenes[$i]->IDAlmacen))
                            <a href="{{route('almacenes-almacen-A',$almacenes[$i]->IDAlmacen)}}" class="small-box-footer">Ir al Almacen <i class="fa fa-arrow-circle-right"></i></a>
                                @endif
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>{{$clientes[$i]}}</h3>
                                <p>Clientes</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            @if($clientes[$i] >= 1)
                            <a href="{{route('clientes-almacen',$almacenes[$i]->IDAlmacen)}}" class="small-box-footer">Ver Clientes <i class="fa fa-arrow-circle-right"></i></a>
                            @endif
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3>{{$total_ventas_almacen[$i]}}</h3>

                                <p>Ventas</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            @if($total_ventas_almacen[$i] >= 1)
                            <a href="{{route('almacenes-ventas-A',$almacenes[$i]->IDAlmacen)}}" class="small-box-footer">Ver Ventas <i class="fa fa-arrow-circle-right"></i></a>
                                @endif
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <div class="row">

                    <div class="col-md-5">
                        <h4 style="color:#ffffff;  background-color:#309961; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0; border-radius: 7px;">
                            Ventas mensuales
                        </h4>
                        <!-- Bar chart -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <i class="fa fa-bar-chart-o"></i>

                                <h3 class="box-title">Grafico de Ventas Mensuales</h3>

                                <div class="box-tools pull-right">
                                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                                </div>
                            </div>
                            <div class="box-body">
                                <canvas id="" name="barchart{{$count}}" class="" width="800" height="450" style="height: 300px; padding: 0px; position: relative;">


                                </canvas>
                            </div>
                            <!-- /.box-body-->
                        </div>
                        <!-- /.box -->

                    </div>
                    <div class="col-md-7">
                        <h4 style="color:#ffffff; background-color:#309961; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0; border-radius: 7px;">
                            Últimas Ventas Realizadas
                        </h4>
                        <div class="table-responsive">
                            <table id="" class="table table-bordered ultimas" role="grid" aria-describedby="categoria_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Factura</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Fecha de Venta</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Total</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Cliente</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($ventas_almacen[$i]))
                                    @foreach($ventas_almacen[$i] as $ventas)
                                <tr role="row" class="odd">

                                    <!--<td class="sorting_1"></td>-->

                                    <td>{{$ventas->Factura_Id}}</td>
                                    <td>{{$ventas->Fecha}}</td>
                                    <td>{{$ventas->Valor_Total}}</td>
                                    <td>{{$ventas->IDCliente}}</td>

                                </tr>
                                    @endforeach
                                @endif
                                </tbody>

                                <tfoot>
                                <tr><th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Factura</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Fecha de Venta</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Total</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Cliente</th></tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <div class="row">


                    <div class="col-md-5">
                        <h4 style="color:#ffffff;  background-color:#0b7399; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0; border-radius: 7px;">
                            Productos Sin Stock y con Inventario Bajo
                        </h4>
                        <div class="table-responsive">
                            <table id="" class="table table-bordered stock" role="grid" aria-describedby="categoria_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Codigo</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Producto</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Cantidad</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Estado</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if(isset($producto_stock[$i]))
                                    @foreach($producto_stock[$i] as $producto)
                                        <tr role="row" class="odd">
                                            <!--<td class="sorting_1"></td>-->
                                            <td>{{$producto->Cod}}</td>
                                            <td>{{$producto->Articulo}}</td>
                                            <td>{{$producto->Cantidad}}</td>
                                            <td>Baja Disponibilidad</td>
                                            <td><a class="btn btn-warning btn-xs" href="{{ route('producto-edit-A', $producto->IDProducto) }}" data-toggle="tooltip" data-placement="left" title="Reponer Cantidad">
                                                    <i class="fa fa-pencil fa-lg"></i>
                                                </a></td>


                                        </tr>
                                    @endforeach
                                @endif

                                </tbody>

                                <tfoot>
                                <tr>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Codigo</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Producto</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Cantidad</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Estado</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Acciones</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                        <!-- /.box -->

                    </div>
                    <div class="col-md-7">
                        <h4 style="color:#ffffff; background-color:#db4020; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0; border-radius: 7px;">
                            Últimas Compras Realizadas
                        </h4>
                        <div class="table-responsive">
                            <table id="" class="table table-bordered compras" role="grid" aria-describedby="categoria_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Nro Doc</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Fecha de Compra</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Nro Factura</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Fecha de Emision</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Total</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Proveedor</th>
                                </tr>
                                </thead>
                                <tbody>



                                @if(isset($factura_proveedor[$i]))
                                    @foreach($factura_proveedor[$i] as $factura)
                                        <tr role="row" class="odd">

                                            <!--<td class="sorting_1"></td>-->

                                            <td>{{$factura->IDFactProv}}</td>

                                            <td>{{$factura->FechaEmision}}</td>
                                            <td>{{$factura->NumFactura}}</td>
                                            <td>{{$factura->FechaEmision}}</td>
                                            <td>{{$factura->Total}}</td>
                                            <td>{{$factura->IDProv}}</td>

                                        </tr>
                                        @endforeach
                                        @endif



                                </tbody>

                                <tfoot>
                                <tr>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Nro Doc</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">FEcha de Compra</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Nro Factura</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Fecha de Emision</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Total</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Proveedor</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <div hidden>{{$count = $count + 1}}</div>
        @endfor
    </section>
@endsection


