@extends('layouts.appmod')


@section('content')
    <section class="content-header">
        <h1>
            Nuevo Proveedor
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Nuevo Proveedor</li>
        </ol>
    </section>
    <section class="content">
        <form class="form-horizontal" role="form" method="POST" action="{{route('proveedor-store-A')}}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <!---------------------------------------------------------------------------------------------------------------->
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Legal</h3>

                            <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                                <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <!--<div class="btn-group">
                                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-wrench"></i></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>-->
                                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">


                                    <div class="form-group">
                                        <label for="exampleInputEmail1">RUC</label>
                                        <input type="text" class="form-control" id="RUC" name="RUC" placeholder="RUC">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Razon social</label>
                                        <input type="text" class="form-control" id="RazonSocial" name="RazonSocial" placeholder="Razon social">
                                    </div>

                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Dirección</label>
                                        <input type="text" class="form-control" id="Direccion" name="Direccion" placeholder="Dirección">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Telefono principal</label>
                                        <input type="text" class="form-control" id="TelfPrincipal" name="telfprincipal" placeholder="Telefono Principal">
                                    </div>
                                </div>
                            </div>
                                <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Almacen</label>
                                        <select class="form-control" name="IDalmacen" id="IDalmacen" required>
                                            @foreach($almacen as $alma)
                                            <option value="{{$alma->IDalmacen}}">{{$alma->nombre}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                </div>

                                <!-- /.row -->


                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>


                <!----------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Contacto</h3>

                                <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                                    <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <!--<div class="btn-group">
                                        <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-wrench"></i></button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>-->
                                    <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">

                                    <div class="col-md-1"></div>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Notas</label>
                                            <input type="text" class="form-control" id="Notas" name="Notas" placeholder="Notas">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Correo Electrónico</label>
                                            <input type="email" class="form-control" id="Email" name="Email" placeholder="Email">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Vendedor Contacto</label>
                                            <input type="text" class="form-control" id="Vendedor" name="Vendedor" placeholder="vendedorcontacto">
                                        </div>



                                    </div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Telefono vendedor contacto</label>
                                        <input type="text" class="form-control" id="TelfVendedor" name="TelfVendedor" placeholder="Telefono">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Comentario</label>
                                        <input type="text" class="form-control" id="Comentarios" name="Comentarios" placeholder="Comentario">
                                    </div>
                                    </div>
                                    <!-- /.row -->
                                </div>

                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>

                    <!------------------------------------------------------------------------------------------------------------------------------>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box ">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Bancario</h3>

                                    <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                                        <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <!--<div class="btn-group">
                                            <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-wrench"></i></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>-->
                                        <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">

                                    <div class="row">

                                        <div class="col-md-1"></div>

                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Cuenta contable</label>
                                                <input type="text" class="form-control" id="CtaContable" name="CtaContable" placeholder="Cuenta contable">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nombre de Banco 1</label>
                                                <input type="text" class="form-control" id="Banco1" name="Banco1" placeholder="Nombre de Banco 1">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Numero de CCorriente 1</label>
                                                <input type="text" class="form-control" id="NumCtaCorriente1" name="NumCtaCorriente1" placeholder="Numero de Cuenta Corriente 1">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Numero de CAhorros 2</label>
                                                <input type="text" class="form-control" id="NumCtaAhorros1" name="NumCtaAhorros1" placeholder="Numero de Cuenta de Ahorros 1">
                                            </div>
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nombre de Banco 2</label>
                                                <input type="text" class="form-control" id="Banco2" name="Banco2" placeholder="Nombre de Banco 2">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Numero de CCorriente 1</label>
                                                <input type="text" class="form-control" id="NumCtaCorriente2" name="NumCtaCorriente2" placeholder="Numero de Cuenta Corriente 2">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Numero de CAhorros 2</label>
                                                <input type="text" class="form-control" id="NumCtaAhorros2" name="NumCtaAhorros2" placeholder="Numero de Cuenta de Ahorros 2">
                                            </div>


                                        </div>
                                        <div class="col-md-2"></div>

                                    </div>

                                    <!-- /.row -->
                                </div>

                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>


            <button type="submit" class="btn btn-info pull-right">Guardar</button>

        </form>
    </section>

@endsection
