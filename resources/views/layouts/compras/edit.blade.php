@extends('layouts.appmod')


@section('content')
    <section class="content-header">
        <h1>
            Editar Compra
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Editar Compra</li>
        </ol>
    </section>

        <section class="content">
            <form class="form-horizontal" role="form" method="POST" action="{{route('compras-update-A', $compras->id)}}" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <!---------------------------------------------------------------------------------------------------------------->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box ">
                            <div class="box-header with-border">
                                <h3 class="box-title">Legal</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <!--<div class="btn-group">
                                        <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-wrench"></i></button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>-->
                                    <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4">


                                        <div class="form-group">
                                            <label for="exampleInputEmail1">RUC</label>
                                            <input type="text" class="form-control" id="ruc" name="ruc" value="{{$compras->ruc}}" placeholder="RUC">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Razon social</label>
                                            <input type="text" class="form-control" id="razonsocial" name="razonsocial" value="{{$compras->razonsocial}}" placeholder="Razon social">
                                        </div>

                                    </div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Dirección</label>
                                            <input type="text" class="form-control" id="direccion" name="direccion" value="{{$compras->direccion}}" placeholder="Dirección">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Telefono principal</label>
                                            <input type="text" class="form-control" id="telfprincipal" name="telfprincipal" value="{{$compras->telfprincipal}}" placeholder="Telefono Principal">
                                        </div>
                                    </div>

                                    <!-- /.row -->
                                </div>

                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>


                <!----------------------------------------------------------------------------------------------------->

                <div class="row">
                    <div class="col-md-12">
                        <div class="box ">
                            <div class="box-header with-border">
                                <h3 class="box-title">Contacto</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <!--<div class="btn-group">
                                        <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-wrench"></i></button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>-->
                                    <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">

                                    <div class="col-md-1"></div>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Notas</label>
                                            <input type="text" class="form-control" id="notas" name="notas" value="{{$compras->notas}}" placeholder="Notas">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Correo Electrónico</label>
                                            <input type="email" class="form-control" id="email" name="email" value="{{$compras->email}}" placeholder="Email">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Vendedor Contacto</label>
                                            <input type="text" class="form-control" id="vendedorcontacto" name="vendedorcontacto" value="{{$compras->vendedorcontacto}}" placeholder="vendedorcontacto">
                                        </div>



                                    </div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Telefono vendedor contacto</label>
                                            <input type="text" class="form-control" id="tlfvendedorcontacto" name="tlfvendedorcontacto" value="{{$compras->tlfvendedorcontacto}}" placeholder="Telefono">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Comentario</label>
                                            <input type="text" class="form-control" id="comentario" name="comentario" value="{{$compras->comentario}}" placeholder="Comentario">
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>

                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>

                <!------------------------------------------------------------------------------------------------------------------------------>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box ">
                            <div class="box-header with-border">
                                <h3 class="box-title">Bancario</h3>

                                <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                                    <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <!--<div class="btn-group">
                                        <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-wrench"></i></button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>-->
                                    <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">

                                    <div class="col-md-1"></div>

                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Cuenta contable</label>
                                            <input type="text" class="form-control" id="ctacontable" name="ctacontable" value="{{$compras->ctacontable}}" placeholder="Cuenta contable">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nombre de Banco 1</label>
                                            <input type="text" class="form-control" id="nombrebanco1" name="nombrebanco1" value="{{$compras->nombrebanco1}}" placeholder="Nombre de Banco 1">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Numero de CCorriente 1</label>
                                            <input type="text" class="form-control" id="numctacorriente1" name="numctacorriente1" value="{{$compras->numctacorriente1}}" placeholder="Numero de Cuenta Corriente 1">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Numero de CAhorros 2</label>
                                            <input type="text" class="form-control" id="numctaahorros1" name="numctaahorros1" value="{{$compras->numctaahorros1}}" placeholder="Numero de Cuenta de Ahorros 1">
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nombre de Banco 2</label>
                                            <input type="text" class="form-control" id="nombrebanco2" name="nombrebanco2" value="{{$compras->nombrebanco2}}" placeholder="Nombre de Banco 2">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Numero de CCorriente 1</label>
                                            <input type="text" class="form-control" id="numctacorriente2" name="numctacorriente2" value="{{$compras->numctacorriente1}}" placeholder="Numero de Cuenta Corriente 2">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Numero de CAhorros 2</label>
                                            <input type="text" class="form-control" id="numctaahorros2" name="numctaahorros2" value="{{$compras->numctaahorros2}}" placeholder="Numero de Cuenta de Ahorros 2">
                                        </div>


                                    </div>
                                    <div class="col-md-2"></div>

                                </div>

                                <!-- /.row -->
                            </div>

                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>


                <button type="submit" class="btn btn-info pull-right">Guardar</button>

            </form>
        </section>
@endsection
