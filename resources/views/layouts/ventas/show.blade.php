@extends('layouts.appmod')


@section('content')
    <section class="content-header">
        <h1>
            Destalles de Factura
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Detalles de Factura</li>
        </ol>
    </section>
        <section class="content">
            <!---------------------------------------------------------------------------------------------------------------->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box ">
                            <div class="box-header with-border">
                                <h3 class="box-title">Detalles de Factura</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <!--<div class="btn-group">
                                        <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-wrench"></i></blutton>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>-->
                                    <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for=""><strong>Fecha:</strong> {{$ventas[0]->Fecha}}</label>
                                        <br>
                                        <label for=""><strong>Cliente:</strong> {{$ventas[0]->IDCliente}}</label>
                                        <br>
                                        <label for=""><strong>Factura:</strong> {{$ventas[0]->Factura_Id}}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-10">

    <table class="table table-bordered" id="tabla">
        <thead>
            <tr>
                <td>Cantidad</td>
                <td>Descripcion</td>
                <td>V. Unitario</td>
                <td>V. Total</td>
            </tr>
        </thead>
        <tbody>
        @foreach($ventas as $venta)
        <tr>
            <td>{{$venta->Cantidad}}</td>
            <td>{{$venta->Descripcion}}</td>
            <td>{{$venta->Valor_Unitario}}</td>
            <td>{{$venta->Valor_Total}}</td>

        </tr>
        @endforeach
        </tbody>
    </table>





                                    </div>
                                    <!-- /.row -->
                                </div>

                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
        </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {

            $('#tabla').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel','pdf','csv'
                ],
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 5,
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });

    </script>

@endsection
