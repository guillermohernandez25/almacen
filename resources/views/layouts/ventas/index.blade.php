@extends('layouts.appmod')

@section('css')
    <style>
    #ventas{
    overflow:scroll;
    height:200px;
    width:500px;
    }
    </style>
    @endsection

@section('content')
    <section class="content-header">
        <h1>
            Ventas
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Listado</li>
        </ol>
        <div class="x_content">
            @if(Session::has('message'))
                <div class='alert alert-success'>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <p>{!! Session::get('message') !!}</p>
                </div>
            @endif
        </div>
        <div class="x_content">
            @if(Session::has('message2'))
                <div class='alert alert-danger'>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <p>{!! Session::get('message2') !!}</p>
                </div>
            @endif
        </div>

    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado de Ventas
                            @if(isset($almacen))
                                Por Almacen: {{$almacen->nombre}}
                            @else
                                General
                            @endif
                        </h3>

                        <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                            <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <!--<div class="btn-group">
                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-wrench"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>-->
                            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            @if(isset($id))
                            <form class="form-horizontal" role="form" method="get" action="{{route('almacenes-ventas-A',$id)}}" enctype="multipart/form-data">
                                @else
                                    <form class="form-horizontal" role="form" method="get" action="{{route('ventas')}}" enctype="multipart/form-data">
                                    @endif
                                {!! csrf_field() !!}
                            <div class="col-md-3"><h3>Rango de Fechas:</h3></div>
                            <div class="col-md-3">
                                <label for="fecha_inicial">Fecha Inicial</label>
                                <input type="date" name="fecha_inicial" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <label for="fecha_inicial">Fecha Final</label>
                                <input type="date" name="fecha_final" class="form-control">
                            </div>
                                <div class="col-md-2">
                                    <label for="consultar">Consultar</label>
                                <button id="consultar" type="submit" class="btn btn-info pull-right"><i class="fa fa-mail-forward"></i></button>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <h3>Detalle de Ventas por Fecha</h3>
                                <table id="reporte">
                                    <thead>
                                    <tr>
                                        <td><strong>Fecha</strong></td>
                                        <td><strong>Factura</strong></td>
                                        <td></td>
                                        <td><strong>Total</strong></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($ventas_total))
                                    @if(sizeof($ventas_total) >= 1)
                                    @for($i = 0; $i < sizeof($ventas_total); $i++ )
                                    <tr>
                                        <td style="align-content: center; align-items: center; text-align: center;">
                                        {{$ventas_total[$i]->Fecha}}
                                        </td>
                                        <td style="align-content: center; align-items: center; text-align: center;">
                                            {{$ventas_total[$i]->Factura_Id}}
                                        </td>
                                        <td style="align-content: center; align-items: center; text-align: center;">
                                            ........
                                        </td>
                                        <td style="align-content: center; align-items: center; text-align: center;">
                                            {{$ventas_suma[$i]}}
                                        </td>
                                    </tr>
                                        @endfor
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td style="align-content: center; align-items: center; text-align: center;"><strong>Suma de Totales</strong></td>
                                        <td style="align-content: center; align-items: center; text-align: center;"><strong>{{$ventas_final}}</strong></td>
                                    </tr>
                                        @endif
                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado de Ventas @if(isset($almacen))
                                                                    Por Almacen: {{$almacen->nombre}}
                            @else
                                General
                        @endif</h3>

                        <div class="" style=" position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                            <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <!--<div class="btn-group">
                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-wrench"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>-->
                            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">

                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                        <table id="ventas" class="table table-bordered" role="grid" aria-describedby="ventas_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="ventas" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Fecha</th>
                                                <th class="sorting" tabindex="0" aria-controls="ventas" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 107px;">Numero de Caja</th>
                                                <th class="sorting" tabindex="0" aria-controls="ventas" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 107px;">Cliente</th>
                                                <th class="sorting" tabindex="0" aria-controls="ventas" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 107px;">Forma de Pago</th>


                                                <th class="sorting" tabindex="0" aria-controls="ventas" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 107px;">Acciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($ventas as $ven)
                                            <tr role="row" class="odd">
                                                <!--<td class="sorting_1">{{$ven->IDFacturacion}}</td>-->

                                                     <td>{{$ven->Fecha }}</td>
                                                     <td>{{$ven->NumCaja }}</td>
                                                     <td> @if(isset($ven->clientes->Nombres)) {{$ven->clientes->Nombres}} {{$ven->clientes->Apellidos}} @else S/R @endif</td>
                                                     <td>{{$ven->FormaDePago}}</td>
                                                <td><a class="btn btn-success btn-xs" href="{{ route('ventas-show-A', $ven->Factura_Id) }}" data-toggle="tooltip" data-placement="left" title="Ver Detalle">
                                                        <i class="fa fa-eye fa-lg"></i>
                                                    </a>

                                                </td>
                                            </tr>
                                                 @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <!--<div class="col-md-4">
                                <p class="text-center">
                                    <strong>Configuración Global</strong>
                                </p>
                                <form class="form-horizontal" role="form" method="POST" action="{{route('color_store')}}" enctype="multipart/form-data">
                                    {!! csrf_field() !!}
                                <div class="form-group">
                                    <label>Listado de Colores</label>
                                    <select class="form-control" name="color">

                                        <option value=""></option>

                                    </select>
                                </div>
                                <button type="submit" class="btn btn-info pull-right">Guardar</button>
                                </form>

                            </div>-->
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>

                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>

    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {

            $('#ventas').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    {
                        extend:'copy',
                        title: '<?php echo __('Ventas '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'csv',
                        title: '<?php echo __('Ventas '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'excel',
                        title: '<?php echo __('Ventas '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'pdf',
                        title: '<?php echo __('Ventas '); echo(date('Y-m-d')); ?>',

                    },

                    {
                        extend:'print',
                        title: '<?php echo __('Ventas '); echo(date('Y-m-d')); ?>'
                    }
                ],
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 5,
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
            $('#reporte').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    {
                        extend:'copy',
                        title: '<?php echo __('Ventas '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'csv',
                        title: '<?php echo __('Ventas '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'excel',
                        title: '<?php echo __('Ventas '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'pdf',
                        orientation: 'portrait',
                        pageSize: 'letter',
                        filename:'<?php echo __('Ventas '); echo(date('Y-m-d')); ?>',
                        title:'{{Auth::User()->empresas->NombreEstablecim}}' ,
                        messageTop:'Direccion: {{Auth::User()->empresas->Direcc}} \n Cajero: {{Auth::User()->Nombres}} {{Auth::User()->Apellidos}}  \n Hora de Impresion: <?php $time = time();

                            echo date("H:i:s", $time); ?> \n \n Rango de Fecha desde: <?php echo($desde); ?> Hasta:<?php echo($hasta); ?>',
                        customize: function(doc) {
                            doc.styles.title = {
                                color: 'red',
                                fontSize: '40',
                                //background: 'blue',
                                alignment: 'center'
                            }
                        }
                        //messageBottom:'The information in this table is copyright to Sirius Cybernetics Corp.'
                    },
                    {
                        extend:'print',
                        title: '<?php echo __('Ventas '); echo(date('Y-m-d')); ?>'
                    }
                ],
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true,
                "pageLength": 5,
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });

    </script>

    @endsection
