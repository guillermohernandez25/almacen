@extends('layouts.appmod')


@section('content')
    <section class="content-header">
        <h1>
            Editar Rol
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Editar Rol</li>
        </ol>
    </section>

    <section class="content">
        <form class="form-horizontal" role="form" method="POST" action="{{route('roles_update', $roles->id)}}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <!---------------------------------------------------------------------------------------------------------------->
            <div class="row">
                <div class="col-md-12">
                    <div class="box ">
                        <div class="box-header with-border">
                            <h3 class="box-title">General</h3>

                            <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                                <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                                <!--<div class="btn-group">
                                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-wrench"></i></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>-->
                                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">

                                <div class="col-md-2"></div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="nombre">Empresa</label>
                                        <input type="text" name="IDpref" value="{{$empresa->IDpref}}" hidden="true">
                                        <input class="form-control" type="text" readonly value="{{$empresa->NombreEstablecim}}">

                                    </div>
                                </div>
                            </div>


                            <div class="row">

                                <div class="col-md-2"></div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="nombre">Nombre del Rol</label>
                                        <input type="text" class="form-control" id="tipo" name="tipo" placeholder="Nombre" value="{{$roles->tipo}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <h6>Seleccionar todos los modulos que desea permitir incluso si ya estaban disponibles previamente...</h6>
                                        <label for="select">Reescribir los permisos para este rol</label>
                                        <select style="width: 100%;" class="select form-control " name="roles[]" multiple="multiple">
                                            @for($i = 0; $i <sizeof($modulos); $i++))
                                            @if(isset($roles_actual[$i]->panel_id))
                                                @foreach($modulos as $modulo)
                                                @if($roles_actual[$i]->panel_id == $modulo->IDmodulos_sistema)
                                                    <option selected value="{{$modulo->IDmodulos_sistema}}">{{$modulo->modulo}}</option>
                                                @else
                                                @endif
                                                @endforeach
                                            @else
                                                <option value="{{$modulos[$i]->IDmodulos_sistema}}">{{$modulos[$i]->modulo}}</option>
                                            @endif
                                            @endfor

                                        </select>

                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <div class="row">
                                <div class="col-md-12"><label for="select">Modulos Permitidos actualmente para este Rol </label></div>
                                @foreach($roles_actual as $ro)
                                    <div class="col-md-4">
                                        <li class="label label-success">{{$ro->modulos->modulo}}</li>
                                    </div>
                                @endforeach

                            </div>

                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>








            <button type="submit" class="btn btn-info pull-right">Guardar</button>

        </form>
    </section>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.select').select2();
        });
    </script>
@endsection
