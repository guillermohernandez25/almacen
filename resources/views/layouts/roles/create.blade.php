@extends('layouts.appmod')


@section('content')
    <section class="content-header">
        <h1>
            Nuevo Administrador
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Nuevo Administrador</li>
        </ol>
    </section>
    <section class="content">
        <form class="form-horizontal" role="form" method="POST" action="{{route('administradores-store-A')}}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <!---------------------------------------------------------------------------------------------------------------->
            <div class="row">
                <div class="col-md-12">
                    <div class="box ">
                        <div class="box-header with-border" >
                            <h3 class="box-title">General</h3>

                            <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                                <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                                <!--<div class="btn-group">
                                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-wrench"></i></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>-->
                                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">


                                    <div class="form-group">
                                        <label for="nombre">Nombre</label>
                                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
                                    </div>
                                    <div class="form-group">
                                        <label for="apellido">Apellido</label>
                                        <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido">
                                    </div>
                                    <div class="form-group">
                                        <label for="dni">Cédula</label>
                                        <input type="text" class="form-control" id="dni" name="dni" placeholder="DNI">
                                    </div>

                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="direccion">Dirección</label>
                                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección">
                                    </div>
                                    <div class="form-group">
                                        <label for="telefono">Telefono principal</label>
                                        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono Principal">
                                    </div>
                                </div>

                                <!-- /.row -->
                            </div>

                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>


            <button type="submit" class="btn btn-info pull-right">Guardar</button>

        </form>
    </section>

@endsection
