@extends('layouts.appmod')


@section('content')
    <section class="content-header">
        <h1>
            Leer Mesanje
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Leer Mensaje @if(isset($mensajes->emisor->Nombres)) Recibido de {{$mensajes->emisor->Nombres}} {{$mensajes->emisor->Apellidos}}  @endif </li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa  fa-envelope"></i>

                    <h3 class="box-title"> {{$mensajes->Asunto}}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <blockquote>
                        <?php echo($mensajes->Mensaje); ?>
                    </blockquote>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!--<div class="col-md-3">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <!-- /.box-header -->
                <!--<div class="box-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{route('empresa-store-A')}}" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <td>

                            <input type="text" hidden name="IDrespuesta" value="{{$mensajes->IDemisor}}">
                            <button type="button" class="btn btn-block btn-info btn-flat">Responder</button>
                        </td>
                    </form>

                </div>
                <!-- /.box-body -->
            <!--</div>
            <!-- /.box -->
        <!--</div>-->

    </section>

@endsection

@section('script')
    <script type="text/javascript">



    </script>
    @endsection
