@extends('layouts.appmod')

@section('css')
    <style>
    #Mensajes{
    overflow:scroll;
    height:200px;
    width:500px;
    }
    </style>
    @endsection

@section('content')
    <section class="content-header">
        <h1>
            Bandeja de Mensajes de {{Auth::User()->Nombres}} {{Auth::User()->Apellidos}}
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li class="active">Mensajes Recibidos</li>
        </ol>
        <div class="x_content">
            @if(Session::has('message'))
                <div class='alert alert-success'>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <p>{!! Session::get('message') !!}</p>
                </div>
            @endif
        </div>
        <div class="x_content">
            @if(Session::has('message2'))
                <div class='alert alert-danger'>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <p>{!! Session::get('message2') !!}</p>
                </div>
            @endif
        </div>

    </section>
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado Mensajes Recibidos</h3>
                        <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                            <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">

                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                        <table id="Mensajes" class="table table-bordered" role="grid" aria-describedby="proveedor_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="Mensajes" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Asunto</th>
                                                <th class="sorting" tabindex="0" aria-controls="Mensajes" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 190px;">Estatus</th>
                                                <th class="sorting" tabindex="0" aria-controls="Mensajes" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 149px;">Quien Envia</th>
                                                <th class="sorting" tabindex="0" aria-controls="Mensajes" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 149px;">Quien Recibe</th>
                                                <th class="sorting" tabindex="0" aria-controls="Mensajes" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 107px;">Acciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($mensajes as $mensa)
                                            <tr role="row" class="odd">
                                                    <td>{{$mensa->Asunto }}</td>
                                                <td>@if ($mensa->Estatus == 1) <h4 style="color: #c55200;"> Sin leer </h4> @else <h4 style="color: #81b01d;"> Leído </h4>  @endif</td>
                                                <td>{{$mensa->emisor->Nombres}} {{$mensa->emisor->Apellidos}}</td>
                                                <td>{{$mensa->receptor->Nombres}} {{$mensa->receptor->Apellidos}}</td>
                                                <td><a class="btn btn-warning btn-xs" href="{{ route('mensajes-leer-recibidos', $mensa->IDmensajes) }}" data-toggle="tooltip" data-placement="left" title="Leer">
                                                        <i class="fa  fa-file-text-o fa-lg"> Leer</i>
                                                    </a>
                                                    </td>
                                            </tr>
                                                 @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>

                                            </tr>
                                            </tfoot>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <!--<div class="col-md-4">
                                <p class="text-center">
                                    <strong>Configuración Global</strong>
                                </p>
                                <form class="form-horizontal" role="form" method="POST" action="{{route('color_store')}}" enctype="multipart/form-data">
                                    {!! csrf_field() !!}
                                <div class="form-group">
                                    <label>Listado de Colores</label>
                                    <select class="form-control" name="color">

                                        <option value=""></option>

                                    </select>
                                </div>
                                <button type="submit" class="btn btn-info pull-right">Guardar</button>
                                </form>

                            </div>-->
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>

                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>

    </section>
@endsection

@section('script')
    <script>
        $(function () {

            $('#Mensajes').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    {
                        extend:'copy',
                        title: '<?php echo __('Mensajes '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'csv',
                        title: '<?php echo __('Mensajes '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'excel',
                        title: '<?php echo __('Mensajes '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'pdf',
                        title: '<?php echo __('Mensajes '); echo(date('Y-m-d')); ?>'
                    },
                    ,
                    {
                        extend:'print',
                        title: '<?php echo __('Mensajes '); echo(date('Y-m-d')); ?>'
                    }
                ],
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 5,
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });

    </script>

    @endsection
