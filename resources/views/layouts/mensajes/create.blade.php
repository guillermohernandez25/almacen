@extends('layouts.appmod')


@section('content')
    <section class="content-header">
        <h1>
            Mensaje Nuevo
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inico</a></li>
            <li class="active">Mensaje nuevo</li>
        </ol>
    </section>
    <section class="content">
        <form class="form-horizontal" role="form" method="POST" action="{{route('mensajes-store')}}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <!---------------------------------------------------------------------------------------------------------------->
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Mensaje Nuevo</h3>

                            <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                                <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <!--<div class="btn-group">
                                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-wrench"></i></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>-->
                                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <label  for="IDreceptor">Destinatario</label>
                                    <select style="width: 100%;" class="select form-control" name="IDreceptor[]" multiple="multiple" id="IDreceptor">
                                        @foreach($usuarios as $user)
                                        <option value="{{$user->IDusuario}}">{{$user->Nombres}} {{$user->Apellidos}}</option>
                                            @endforeach
                                    </select>
                                    <label  for="Asunto">Asunto</label>
                                    <input class="form-control" type="text" name="Asunto">
                                </div>
                            </div>
                                <div class="row">
                                <div class="col-md-12">
                                    <label for="Mensaje">Mensaje</label>
                                    <textarea id="Mensaje" name="Mensaje" rows="10" cols="80" style="visibility: hidden; display: none;">

                                    </textarea>
                                </div>
                                <!-- /.row -->
                            </div>

                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>



            <button type="submit" class="btn btn-info pull-right">Enviar</button>

        </form>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.select').select2();
        });
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('Mensaje')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
    @endsection
