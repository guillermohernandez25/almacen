@extends('layouts.appmod')


@section('content')
    <section class="content-header">
        <h1>
            Editor de Empresa
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Editor de Empresa</li>
        </ol>
    </section>

        <section class="content">
            <form class="form-horizontal" role="form" method="POST" action="{{route('empresa-update-A', $empresa->IDpref)}}" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <!---------------------------------------------------------------------------------------------------------------->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box ">
                            <div class="box-header with-border">
                                <h3 class="box-title">General</h3>

                                <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                                    <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>

                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">

                                    <div class="col-md-1"></div>
                                    <div class="col-md-4">


                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nombre de Establecimiento</label>
                                            <input type="text" class="form-control" id="NombreEstablecim" name="NombreEstablecim" value="{{$empresa->NombreEstablecim}}" placeholder="Nombre de Establecimiento" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Ciudad</label>
                                            <input type="text" class="form-control" id="Ciudad" name="Ciudad" value="{{$empresa->Ciudad}}" placeholder="Ciudad" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Dirección</label>
                                            <input type="text" class="form-control" id="Direcc" name="Direcc" value="{{$empresa->Direcc}}" placeholder="Dirección de Establecimiento" required>
                                        </div>

                                    </div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">RUC / NIF</label>
                                            <input type="text" class="form-control" id="RUC" name="RUC" value="{{$empresa->RUC}}" placeholder="RUC / NIF" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Telefono Principal</label>
                                            <input type="text" class="form-control" id="Telf1" name="Telf1" value="{{$empresa->Telf1}}" placeholder="Telefono Principal" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Telefono Secundario</label>
                                            <input type="text" class="form-control" id="Telf2" name="Telf2" value="{{$empresa->Telf2}}" placeholder="Telefono Secundario" required>
                                        </div>


                                    </div>

                                    <!-- /.row -->
                                </div>

                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
                <button type="submit" class="btn btn-info pull-right">Guardar</button>

            </form>
        </section>
@endsection
@section('script')

@endsection
