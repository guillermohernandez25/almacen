@extends('layouts.appmod')
@section('script')
    <script type="text/javascript">
        $('#IDpref').change(function(e){
            e.preventDefault();
            var id = $(this).val();
            var APP_URL = {!! json_encode(url('/')) !!};
            var url = APP_URL + '/c_almacen/'+ id;
            $.get(url,function(resul){
                console.log(resul);
                if(resul.registro == "true"){
                    alert('Ahora selecciona el almacen para esta empresa');
                    $("#IDalmacen").append('<option value="">Selecciona un Almacen</option>');
                    $.each(resul.resultado, function(id,value) {
                        $("#IDalmacen").append('<option value="' + value.IDalmacen + '">' + value.nombre + '</option>');
                    });
                }else{
                    alert('No hay almacen para esta empresa');
                    $('#IDalmacen').empty();
                }

            });
        });
        </script>
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Nueva Categoria
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Nueva Categoria</li>
        </ol>
    </section>
    <section class="content">
        <form class="form-horizontal" role="form" method="POST" action="{{route('categoriaproducto-store-A')}}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <!---------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col-md-12">
                    <div class="box ">
                        <div class="box-header with-border">
                            <h3 class="box-title">General</h3>

                            <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                                <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                                <!--<div class="btn-group">
                                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-wrench"></i></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>-->
                                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">

                                <div class="col-md-6">
                                    <label for="IDPref">Empresa</label>
                                    <select class="form-control" name="IDpref" id="IDpref" required>
                                        <option value="">Selecciona una empresa</option>
                                        @foreach($empresas as $emp)
                                            <option value="{{$emp->IDpref}}">{{$emp->NombreEstablecim}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label for="IDPref">Almacen</label>
                                    <select class="form-control" name="IDalmacen" id="IDalmacen" required>


                                    </select>
                                </div>
                            </div>
                                <div class="row">

                                <div class="col-md-6">

                                        <label for="Categoria">Categoria</label>
                                        <input type="text" class="form-control" id="Categoria" name="Categoria" placeholder="Categoria">

                                    <!--<div class="form-group">
                                        <label for="TipoCuenta">Tipo de Cuenta</label>
                                        <input type="text" class="form-control" id="TipoCuenta" name="TipoCuenta" placeholder="Tipo de Cuenta">
                                    </div>-->
                                </div>

                                <div class="col-md-6">
                                    <!--<div class="form-group">
                                        <label for="TipoTrans">Tipo de Trans..</label>
                                        <input type="text" class="form-control" id="TipoTrans" name="TipoTrans" placeholder="Tipo de Trans..">
                                    </div>-->

                                        <label for="CtaContable">Cta Contable</label>
                                        <input type="text" class="form-control" id="CtaContable" name="CtaContable" placeholder="Cta Contable">

                                </div>
                                <!-- /.row -->
                           <!-- </div><div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="CategoriaFacturable">Categoria Facturable</label>
                                        <input type="text" class="form-control" id="CategoriaFacturable" name="CategoriaFacturable" placeholder="Categoria Facturable">
                                    </div>
                                    <div class="form-group">
                                        <label for="IDDescuento">Descuento</label>
                                        <input type="text" class="form-control" id="IDDescuento" name="IDDescuento" placeholder="Descuento">
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ProductoElaborado">Producto Elaborado</label>
                                        <input type="text" class="form-control" id="ProductoElaborado" name="ProductoElaborado" placeholder="Producto Elaborado">
                                    </div>
                                    <div class="form-group">
                                        <label for="DebitarDelTotal">Debitar del Total</label>
                                        <input type="text" class="form-control" id="DebitarDelTotal" name="DebitarDelTotal" placeholder="Debitar del Total">
                                    </div>
                                </div>
                                <!-- /.row -->
                            <!--</div>-->
                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>





            <button type="submit" class="btn btn-info pull-right">Guardar</button>

        </form>
    </section>

@endsection
