<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<title>@yield('titulo')</title>-->
    <title>::Factumarket | {{Auth::User()->empresas->NombreEstablecim}}::</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" type="text/css" href="{{ asset("DataTables/datatables.min.css")}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("Select2/select2.min.css")}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("DataTables/Buttons-1.5.6/css/buttons.bootstrap.css")}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("DataTables/Buttons-1.5.6/css/buttons.bootstrap4.css")}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("DataTables/Buttons-1.5.6/css/buttons.dataTables.css")}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("DataTables/Buttons-1.5.6/css/buttons.foundation.css")}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("DataTables/Buttons-1.5.6/css/buttons.jqueryui.css")}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("DataTables/css/buttons.semanticui.css")}}"/>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset("AdminLTE-2.4.10/bower_components/bootstrap/dist/css/bootstrap.min.css")}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset("AdminLTE-2.4.10/bower_components/font-awesome/css/font-awesome.min.css")}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset("AdminLTE-2.4.10/bower_components/Ionicons/css/ionicons.min.css")}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset("AdminLTE-2.4.10/bower_components/jvectormap/jquery-jvectormap.css")}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset("AdminLTE-2.4.10/dist/css/AdminLTE.min.css")}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset("AdminLTE-2.4.10/dist/css/skins/_all-skins.min.css")}}">
    <link rel="stylesheet" href="{{asset("AdminLTE-2.4.10/plugins/iCheck/all.css")}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
    .loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url({{asset("images/tenor2.gif")}}) 50% 50% no-repeat rgb(223, 226, 220);
    opacity: .8;
    }
    </style>
@yield('css')


</head>
@if(!empty($color))
<body class="sidebar-mini {{$color->nombre_color->color}}">
@else
    <body class="sidebar-mini skin-blue">
    @endif

<section id="container" class="">

    <div class="wrapper">

        @include('layouts.navbar')
        @include('layouts.sidebar')
        <div class="content-wrapper">
            <section class="content">
                <div class="loader">
                    @yield('warning')
                </div>
            @yield('content')

            </section>
        </div>



    @include('layouts.footer')
    </div>
</section>




    <!-- jQuery 3 -->
    <script src="{{ asset("AdminLTE-2.4.10/bower_components/jquery/dist/jquery.min.js")}}"></script>
    <script type="text/javascript" src="{{ asset("DataTables/datatables.min.js")}}"></script>

    <script type="text/javascript" src="{{ asset("DataTables/Buttons-1.5.6/js/dataTables.buttons.js")}}"></script>
    <script type="text/javascript" src="{{ asset("DataTables/Buttons-1.5.6/js/buttons.print.js")}}"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js "></script>
    <script type="text/javascript" src="http://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="http://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="{{ asset("DataTables/Buttons-1.5.6/js/buttons.bootstrap.js")}}"></script>
    <script type="text/javascript" src="{{ asset("DataTables/Buttons-1.5.6/js/buttons.bootstrap4.js")}}"></script>
    <script type="text/javascript" src="{{ asset("DataTables/Buttons-1.5.6/js/buttons.colVis.js")}}"></script>
    <script type="text/javascript" src="{{ asset("DataTables/Buttons-1.5.6/js/buttons.flash.js")}}"></script>
    <script type="text/javascript" src="{{ asset("DataTables/Buttons-1.5.6/js/buttons.foundation.js")}}"></script>
    <script type="text/javascript" src="{{ asset("DataTables/Buttons-1.5.6/js/buttons.html5.js")}}"></script>
    <script type="text/javascript" src="{{ asset("DataTables/Buttons-1.5.6/js/buttons.jqueryui.js")}}"></script>

    <script type="text/javascript" src="{{ asset("DataTables/Buttons-1.5.6/js/buttons.semanticui.js")}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset("AdminLTE-2.4.10/bower_components/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- iCheck -->
    <script src="{{ asset("AdminLTE-2.4.10/plugins/iCheck/icheck.min.js")}}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
        $(document).ready(function() {

            setInterval('contador()',2000);

        });
        function contador(){

            var APP_URL = {!! json_encode(url('/')) !!};
            var url = APP_URL + '/mensajes/contador';
            $.get(url,function(resul){
                console.log(resul);
                $('#valoro').text(resul);
            });
        };

        $(document).ready(function() {
            $('.loader').fadeOut("slow");
        });
    </script>
    <!-- DataTables -->

    <script src="{{ asset("AdminLTE-2.4.10/bower_components/fastclick/lib/fastclick.js")}}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset("AdminLTE-2.4.10/dist/js/adminlte.min.js")}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset("AdminLTE-2.4.10/dist/js/demo.js")}}"></script>
    <!--<script src="{{ asset("AdminLTE-2.4.10/bower_components/chart.js/Chart.js")}}"></script>-->
    <script src="{{ asset("Chart.min.js")}}"></script>
    <script src="{{ asset("Select2/select2.min.js")}}"></script>
    <script src="{{asset('plugin/assets/js/ace-elements.min.js')}}"></script>
    <script src="{{asset('plugin/assets/js/ace.min.js')}}"></script>
    <script src="{{asset('plugin/toast/toastr.min.js')}}"></script>
    <script src="{{asset('AdminLTE-2.4.10/bower_components/ckeditor/ckeditor.js')}}"></script>

@yield('script')
</body>
</html>
