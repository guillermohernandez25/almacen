


<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset("uploads/usuarios/".Auth::User()->Foto)}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::User()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i>En Linea</a>
            </div>
        </div>
        <!-- search form -->
        <!--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>-->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Panel de Navegación</li>
            <!--<li class="active treeview menu-open">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                    <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Layout Options</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
                    <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
                    <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
                    <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
                </ul>
            </li>
            <li>
                <a href="pages/widgets.html">
                    <i class="fa fa-th"></i> <span>Widgets</span>
                    <span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Charts</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
                    <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
                    <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
                    <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
                </ul>
            </li>-->
            @if(Request::path() == "home")
                <li><a style="color: #ff851b;" href="{{route('home')}}"><i class="fa fa-circle-o text-red"></i> <span>Inicio</span></a></li>
            @else
                <li><a href="{{route('home')}}"><i class="fa fa-circle-o text-red"></i> <span>Inicio</span></a></li>
            @endif

            @if(Request::path() == "empresa")                                                                                                                                                                       )
            <li class="treeview menu-open">
            @else
                <li class="treeview">
                    @endif
                    <a href="#">
                        <i class="fa fa-building"></i>
                        <span>Empresa</span>
                        <span class="pull-right-container">
                                                                                                                                                                                          <i class="fa fa-angle-left pull-right"></i>
                                                                                                                                                                                        </span>
                    </a>
                    @if(Request::path() == "empresa")
                        <ul class="treeview-menu" style="display: block;">
                            @else
                                <ul class="treeview-menu" >
                                    @endif
                                    @if(in_array(22,json_decode($rol),false))
                                        @if(Request::path() == "empresa")
                                            <li><a style="color: #ff851b;" href="{{route('empresa')}}"><i class="fa fa-circle-o"></i>Listar</a></li>
                                        @else
                                            <li><a href="{{route('empresa')}}"><i class="fa fa-circle-o"></i> Listar</a></li>
                                        @endif
                                    @endif
                                    @if(in_array(23,json_decode($rol),false))
                                        @if(Request::path() == "empresa/create")
                                            <li><a style="color: #ff851b;" href="{{route('empresa-create-A')}}"><i class="fa fa-plus"></i>Nuevo</a></li>
                                        @else
                                            <li><a href="{{route('empresa-create-A')}}"><i class="fa fa-plus"></i>Nuevo</a></li>
                                        @endif
                                    @endif
                                </ul>
                                </li>





                                @if(Request::path() == "almacenes"
                                or Request::path() == "almacenes/create"
                                or Request::path() == "proveedor"
                                or Request::path() == "proveedor/create"
                                or Request::path() == "inventario"
                                or Request::path() == "inventario/create"
                                or Request::path() == "inventario/asignacion"
                                or Request::path() == "producto"
                                or Request::path() == "productoslow"
                                or Request::path() == "producto/create"
                                or Request::path() == "ventas"
                                 or Request::path() == "kardex"
                                        or Request::path() == "kardexin"
                                        or Request::path() == "kardexout"
                                        or Request::path() == "compras"
                                )

                                    <li class="treeview menu-open">
                                @else
                                    <li class="treeview">
                                        @endif
                                        <a href="#">
                                            <i class="fa fa-cubes"></i>
                                            <span>Almacenes</span>
                                            <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
                   </span>
                                        </a>
                                        @if(Request::path() == "almacenes"
                                        or Request::path() == "almacenes/create"
                                        or Request::path() == "proveedor"
                                        or Request::path() == "proveedor/create"
                                        or Request::path() == "inventario"
                                        or Request::path() == "inventario/create"
                                        or Request::path() == "inventario/asignacion"
                                        or Request::path() == "producto"
                                        or Request::path() == "productoslow"
                                        or Request::path() == "producto/create"
                                        or Request::path() == "ventas"
                                        or Request::path() == "kardex"
                                        or Request::path() == "kardexin"
                                        or Request::path() == "kardexout"
                                        or Request::path() == "compras"
                                       )
                                            <ul class="treeview-menu" style="display: block;">
                                                @else
                                                    <ul class="treeview-menu">
                                                        @endif
                                                        @if(in_array(1,json_decode($rol),false))
                                                            @if(Request::path() == "almacenes")
                                                                <li><a style="color: #ff851b;" href="{{route('almacenes')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                            @else
                                                                <li><a href="{{route('almacenes')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                            @endif
                                                        @endif
                                                        @if(in_array(2,json_decode($rol),false))
                                                            @if(Request::path() == "almacenes/create")
                                                                <li><a style="color: #ff851b;" href="{{route('almacenes-create-A')}}"><i class="fa fa-plus"></i>Crear Nuevo</a></li>
                                                            @else
                                                                <li><a href="{{route('almacenes-create-A')}}"><i class="fa fa-plus"></i>Crear Nuevo</a></li>
                                                            @endif
                                                        @endif

                                                        @if(Request::path() == "proveedor"
                                                        or Request::path() == "proveedor/create")

                                                            <li class="treeview menu-open">
                                                        @else
                                                            <li class="treeview">
                                                                @endif
                                                                <a href="#">
                                                                    <i class="fa fa-truck"></i>
                                                                    <span>Proveedores</span>
                                                                    <span class="pull-right-container">
                                         <i class="fa fa-angle-left pull-right"></i>
                                       </span>
                                                                </a>
                                                                @if(Request::path() == "proveedor"
                                                                or Request::path() == "proveedor/create")
                                                                    <ul class="treeview-menu" style="display: block;">
                                                                        @else
                                                                            <ul class="treeview-menu" >
                                                                                @endif
                                                                                @if(in_array(3,json_decode($rol),false))
                                                                                    @if(Request::path() == "proveedor")
                                                                                        <li><a style="color: #ff851b;" href="{{route('proveedor')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                    @else
                                                                                        <li><a href="{{route('proveedor')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                    @endif
                                                                                @endif
                                                                                @if(in_array(4,json_decode($rol),false))
                                                                                    @if(Request::path() == "proveedor/create")
                                                                                        <li><a style="color: #ff851b;" href="{{route('proveedor-create-A')}}"><i class="fa fa-plus"></i>Crear Nuevo</a></li>
                                                                                    @else
                                                                                        <li><a href="{{route('proveedor-create-A')}}"><i class="fa fa-plus"></i>Crear Nuevo</a></li>
                                                                                    @endif
                                                                                @endif

                                                                            </ul>
                                                                            </li>
                                                                            @if(Request::path() == "inventario"
                                                                             or Request::path() == "inevntario/create"
                                                                             or Request::path() == "inventario/asignacion"
                                                                             )
                                                                                <li class="treeview menu-open">
                                                                            @else
                                                                                <li class="treeview">
                                                                                    @endif
                                                                                    <a href="#">
                                                                                        <i class="fa fa-th"></i>
                                                                                        <span>Inventarios</span>
                                                                                        <span class="pull-right-container">
                                         <i class="fa fa-angle-left pull-right"></i>
                                       </span>
                                                                                    </a>
                                                                                    @if(Request::path() == "inventario"
                                                                                            or Request::path() == "inventario/create"
                                                                                            or Request::path() == "inventario/asignacion"

                                                                                            )
                                                                                        <ul class="treeview-menu" style="display: block;">
                                                                                            @else
                                                                                                <ul class="treeview-menu" >
                                                                                                    @endif
                                                                                                    @if(in_array(5,json_decode($rol),false))
                                                                                                        @if(Request::path() == "inventario")
                                                                                                            <li><a style="color: #ff851b;" href="{{route('inventario')}}"><i class="fa fa-circle-o"></i>Inventarios por Almacen</a></li>
                                                                                                        @else
                                                                                                            <li><a href="{{route('inventario')}}"><i class="fa fa-circle-o"></i>Inventarios por Almacen</a></li>
                                                                                                        @endif
                                                                                                    @endif
                                                                                                    @if(in_array(6,json_decode($rol),false))
                                                                                                        @if(Request::path() == "inventario/create")
                                                                                                            <li><a style="color: #ff851b;" href="{{route('inventario-create-A')}}"><i class="fa fa-plus"></i>Nuevo Inventario</a></li>
                                                                                                        @else
                                                                                                            <li><a href="{{route('inventario-create-A')}}"><i class="fa fa-plus"></i>Nuevo Inventario</a></li>
                                                                                                        @endif
                                                                                                    @endif
                                                                                                    @if(in_array(7,json_decode($rol),false))
                                                                                                        @if(Request::path() == "inventario/asignacion")
                                                                                                            <li><a style="color: #ff851b;" href="{{route('inventario-asignacion-A')}}"><i class="fa fa-plus"></i>Producto <i class="fa fa-arrow-right"></i>Inventario</a></li>
                                                                                                        @else
                                                                                                            <li><a href="{{route('inventario-asignacion-A')}}"><i class="fa fa-plus"></i>Producto <i class="fa fa-arrow-right"></i>Inventario</a></li>
                                                                                                        @endif
                                                                                                    @endif
                                                                                                </ul>
                                                                                                </li>
                                                                                                @if(Request::path() == "producto"
                                                                                                or Request::path() == "productoslow"
                                                                                                or Request::path() == "producto/create"
                                                                                                or Request::path() == "kardex"
                                                                                                or Request::path() == "kardexin"
                                                                                                or Request::path() == "kardexout"    )
                                                                                                    <li class="treeview menu-open">
                                                                                                @else
                                                                                                    <li class="treeview">
                                                                                                        @endif
                                                                                                        <a href="#">
                                                                                                            <i class="fa fa-cube"></i>
                                                                                                            <span>Articulos</span>
                                                                                                            <span class="pull-right-container">
                                                                                                         <i class="fa fa-angle-left pull-right"></i>
                                                                                                       </span>
                                                                                                        </a>
                                                                                                        @if(Request::path() == "producto"
                                                                                                        or Request::path() == "productoslow"
                                                                                                         or Request::path() == "producto/create"
                                                                                                         or Request::path() == "kardex"
                                                                                                         or Request::path() == "kardexin"
                                                                                                         or Request::path() == "kardexout")
                                                                                                            <ul class="treeview-menu" style="display: block;">
                                                                                                                @else
                                                                                                                    <ul class="treeview-menu" >
                                                                                                                        @endif
                                                                                                                        @if(in_array(8,json_decode($rol),false))
                                                                                                                            @if(Request::path() == "producto")
                                                                                                                                <li><a style="color: #ff851b;" href="{{route('producto')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                                                            @else
                                                                                                                                <li><a href="{{route('producto')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                                                            @endif
                                                                                                                        @endif

                                                                                                                        @if(in_array(9,json_decode($rol),false))
                                                                                                                            @if(Request::path() == "producto/create")
                                                                                                                                <li><a style="color: #ff851b;" href="{{route('producto-create-A')}}"><i class="fa fa-plus"></i>Crear Nuevo</a></li>
                                                                                                                            @else
                                                                                                                                <li><a href="{{route('producto-create-A')}}"><i class="fa fa-plus"></i>Crear Nuevo</a></li>
                                                                                                                            @endif
                                                                                                                        @endif
                                                                                                                        @if(in_array(9,json_decode($rol),false))
                                                                                                                            @if(Request::path() == "kardexin")
                                                                                                                                <li><a style="color: #ff851b;" href="{{url('kardexin')}}"><i class="fa  fa-mail-forward"></i>Ingreso Kardex</a></li>
                                                                                                                            @else
                                                                                                                                <li><a href="{{url('kardexin')}}"><i class="fa  fa-mail-forward"></i>Ingreso Kardex</a></li>
                                                                                                                            @endif
                                                                                                                        @endif
                                                                                                                        @if(in_array(9,json_decode($rol),false))
                                                                                                                            @if(Request::path() == "kardexout")
                                                                                                                                <li><a style="color: #ff851b;" href="{{url('kardexout')}}"><i class="fa  fa-mail-reply"></i>Egreso Kardex</a></li>
                                                                                                                            @else
                                                                                                                                <li><a href="{{url('kardexout')}}"><i class="fa  fa-mail-reply"></i>Egreso Kardex</a></li>
                                                                                                                            @endif
                                                                                                                        @endif
                                                                                                                        @if(in_array(9,json_decode($rol),false))
                                                                                                                            @if(Request::path() == "kardex")
                                                                                                                                <li><a style="color: #ff851b;" href="{{url('kardex')}}"><i class="fa fa-table"></i>Listado Kardex </a></li>
                                                                                                                            @else
                                                                                                                                <li><a href="{{url('kardex')}}"><i class="fa fa-table"></i>Listado Kardex</a></li>
                                                                                                                            @endif
                                                                                                                        @endif

                                                                                                                    </ul>
                                                                                                                    </li>

                                                                                                                    @if(Request::path() == "ventas")
                                                                                                                        <li class="treeview menu-open">
                                                                                                                    @else
                                                                                                                        <li class="treeview">
                                                                                                                            @endif
                                                                                                                            <a href="#">
                                                                                                                                <i class="fa  fa-shopping-cart"></i>
                                                                                                                                <span>Ventas</span>
                                                                                                                                <span class="pull-right-container">
                                                                                                                             <i class="fa fa-angle-left pull-right"></i>
                                                                                                                           </span>
                                                                                                                            </a>
                                                                                                                            @if(Request::path() == "ventas")
                                                                                                                                <ul class="treeview-menu" style="display: block;">
                                                                                                                                    @else
                                                                                                                                        <ul class="treeview-menu" >
                                                                                                                                            @endif
                                                                                                                                            @if(in_array(10,json_decode($rol),false))
                                                                                                                                                @if(Request::path() == "ventas")
                                                                                                                                                    <li><a style="color: #ff851b;" href="{{route('ventas')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                                                                                @else
                                                                                                                                                    <li><a href="{{route('ventas')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                                                                                @endif
                                                                                                                                            @endif

                                                                                                                                        </ul>
                                                                                                                                        </li>

                                                                                                                                        @if(Request::path() == "compras")
                                                                                                                                            <li class="treeview menu-open">
                                                                                                                                        @else
                                                                                                                                            <li class="treeview">
                                                                                                                                                @endif
                                                                                                                                                <a href="#">
                                                                                                                                                    <i class="fa  fa-cart-plus"></i>
                                                                                                                                                    <span>Compras</span>
                                                                                                                                                    <span class="pull-right-container">
                                         <i class="fa fa-angle-left pull-right"></i>
                                       </span>
                                                                                                                                                </a>
                                                                                                                                                @if(Request::path() == "compras"

                                                                                                                                                  )
                                                                                                                                                    <ul class="treeview-menu" style="display: block;">
                                                                                                                                                        @else
                                                                                                                                                            <ul class="treeview-menu" >
                                                                                                                                                                @endif
                                                                                                                                                                @if(in_array(11,json_decode($rol),false))
                                                                                                                                                                    @if(Request::path() == "compras")
                                                                                                                                                                        <li><a style="color: #ff851b;" href="{{route('compras')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                                                                                                    @else
                                                                                                                                                                        <li><a href="{{route('compras')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                                                                                                    @endif
                                                                                                                                                                @endif

                                                                                                                                                            </ul>
                                                                                                                                                            </li>

                                                                                                                                                    </ul>


                                                                                                                                            </li>



                                                                                                                                            @if(Request::path() == "administradores"
                                                                                                                                            or Request::path() == "administradores/create"

                                                                                                                                                                 )
                                                                                                                                                <li class="treeview menu-open">
                                                                                                                                            @else
                                                                                                                                                <li class="treeview">
                                                                                                                                                    @endif
                                                                                                                                                    <a href="#">
                                                                                                                                                        <i class="fa fa-user-secret"></i>
                                                                                                                                                        <span>Administradores Almacen</span>
                                                                                                                                                        <span class="pull-right-container">  <i class="fa fa-angle-left pull-right"></i>  </span>
                                                                                                                                                    </a>
                                                                                                                                                    @if(Request::path() == "administradores"
                                                                                                                                                    or Request::path() == "administradores/create")
                                                                                                                                                        <ul class="treeview-menu" style="display: block;">
                                                                                                                                                            @else
                                                                                                                                                                <ul class="treeview-menu" >
                                                                                                                                                                    @endif
                                                                                                                                                                    @if(in_array(12,json_decode($rol),false))
                                                                                                                                                                        @if(Request::path() == "administradores")
                                                                                                                                                                            <li><a style="color: #ff851b;" href="{{route('administradores')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                                                                                                        @else
                                                                                                                                                                            <li><a href="{{route('administradores')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                                                                                                        @endif
                                                                                                                                                                    @endif
                                                                                                                                                                    @if(in_array(13,json_decode($rol),false))
                                                                                                                                                                        @if(Request::path() == "administradores/create")
                                                                                                                                                                            <li><a style="color: #ff851b;" href="{{route('administradores-create-A')}}"><i class="fa fa-plus"></i>Crear Nuevo</a></li>
                                                                                                                                                                        @else
                                                                                                                                                                            <li><a href="{{route('administradores-create-A')}}"><i class="fa fa-plus"></i>Crear Nuevo</a></li>
                                                                                                                                                                        @endif
                                                                                                                                                                    @endif

                                                                                                                                                                </ul>
                                                                                                                                                                </li>
                                                                                                                                                                @if(Request::path() == "categoriaproducto"
                                                                                                                                                                                         or Request::path() == "categoriaproducto/create")
                                                                                                                                                                    <li class="treeview menu-open">
                                                                                                                                                                @else
                                                                                                                                                                    <li class="treeview">
                                                                                                                                                                        @endif
                                                                                                                                                                        <a href="#">
                                                                                                                                                                            <i class="fa fa-list-ul"></i>
                                                                                                                                                                            <span>Categoria de Productos</span>
                                                                                                                                                                            <span class="pull-right-container">
                                                                                                                                         <i class="fa fa-angle-left pull-right"></i>
                                                                                                                                       </span>
                                                                                                                                                                        </a>
                                                                                                                                                                        @if(Request::path() == "categoriaproducto"  or Request::path() == "categoriaproducto/create")
                                                                                                                                                                            <ul class="treeview-menu" style="display: block;">
                                                                                                                                                                                @else
                                                                                                                                                                                    <ul class="treeview-menu" >
                                                                                                                                                                                        @endif
                                                                                                                                                                                        @if(in_array(14,json_decode($rol),false))
                                                                                                                                                                                            @if(Request::path() == "categoriaproducto")
                                                                                                                                                                                                <li><a style="color: #ff851b;" href="{{route('categoriaproducto')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                                                                                                                            @else
                                                                                                                                                                                                <li><a href="{{route('categoriaproducto')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                                                                                                                            @endif
                                                                                                                                                                                        @endif
                                                                                                                                                                                        @if(in_array(15,json_decode($rol),false))
                                                                                                                                                                                            @if(Request::path() == "categoriaproducto/create")
                                                                                                                                                                                                <li><a style="color: #ff851b;" href="{{route('categoriaproducto-create-A')}}"><i class="fa fa-plus"></i>Crear Nuevo</a></li>
                                                                                                                                                                                            @else
                                                                                                                                                                                                <li><a href="{{route('categoriaproducto-create-A')}}"><i class="fa fa-plus"></i>Crear Nuevo</a></li>
                                                                                                                                                                                            @endif
                                                                                                                                                                                        @endif
                                                                                                                                                                                    </ul>
                                                                                                                                                                                    </li>

                                                                                                                                                                                    @if(Request::path() == "clientes" or Request::path() == "clientes/create")                                                                                                                                                                       )
                                                                                                                                                                                    <li class="treeview menu-open">
                                                                                                                                                                                    @else
                                                                                                                                                                                        <li class="treeview">
                                                                                                                                                                                            @endif
                                                                                                                                                                                            <a href="#">
                                                                                                                                                                                                <i class="fa  fa-user"></i>
                                                                                                                                                                                                <span>Clientes</span>
                                                                                                                                                                                                <span class="pull-right-container">
                                                                                                                                                          <i class="fa fa-angle-left pull-right"></i>
                                                                                                                                                        </span>
                                                                                                                                                                                            </a>
                                                                                                                                                                                            @if(Request::path() == "clientes" or Request::path() == "clientes/create")
                                                                                                                                                                                                <ul class="treeview-menu" style="display: block;">
                                                                                                                                                                                                    @else
                                                                                                                                                                                                        <ul class="treeview-menu" >
                                                                                                                                                                                                            @endif
                                                                                                                                                                                                            @if(in_array(16,json_decode($rol),false))
                                                                                                                                                                                                                @if(Request::path() == "clientes")
                                                                                                                                                                                                                    <li><a style="color: #ff851b;" href="{{route('clientes')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                                                                                                                                                @else
                                                                                                                                                                                                                    <li><a href="{{route('clientes')}}"><i class="fa fa-circle-o"></i>Listado</a></li>
                                                                                                                                                                                                                @endif
                                                                                                                                                                                                            @endif
                                                                                                                                                                                                            @if(in_array(17,json_decode($rol),false))
                                                                                                                                                                                                                @if(Request::path() == "clientes/create")
                                                                                                                                                                                                                    <li><a style="color: #ff851b;" href="{{route('clientes-create-A')}}"><i class="fa fa-plus"></i>Crear Nuevo</a></li>
                                                                                                                                                                                                                @else
                                                                                                                                                                                                                    <li><a href="{{route('clientes-create-A')}}"><i class="fa fa-plus"></i>Crear Nuevo</a></li>
                                                                                                                                                                                                                @endif
                                                                                                                                                                                                            @endif
                                                                                                                                                                                                        </ul>
                                                                                                                                                                                                        </li>

                                                                                                                                                                                                        @if(Request::path() == "color/create" or Request::path() == "inhabilitar")                                                                                                                                                                       )
                                                                                                                                                                                                        <li class="treeview menu-open">
                                                                                                                                                                                                        @else
                                                                                                                                                                                                            <li class="treeview">
                                                                                                                                                                                                                @endif
                                                                                                                                                                                                                <a href="#">
                                                                                                                                                                                                                    <i class="fa fa-cogs"></i>
                                                                                                                                                                                                                    <span>Configuracion de entorno</span>
                                                                                                                                                                                                                    <span class="pull-right-container">
                                                                                                                                                                                          <i class="fa fa-angle-left pull-right"></i>
                                                                                                                                                                                        </span>
                                                                                                                                                                                                                </a>


                                                                                                                                                                                                                @if(Request::path() == "color/create" or Request::path() == "inhabilitar")
                                                                                                                                                                                                                    <ul class="treeview-menu" style="display: block;">
                                                                                                                                                                                                                        @else
                                                                                                                                                                                                                            <ul class="treeview-menu" >
                                                                                                                                                                                                                                @endif
                                                                                                                                                                                                                                @if(in_array(18,json_decode($rol),false))
                                                                                                                                                                                                                                    @if(Request::path() == "color/create")
                                                                                                                                                                                                                                        <li><a style="color: #ff851b;" href="{{route('color')}}"><i class="fa fa-paint-brush"></i> Color</a></li>
                                                                                                                                                                                                                                    @else
                                                                                                                                                                                                                                        <li><a href="{{route('color')}}"><i class="fa fa-paint-brush"></i> Color</a></li>
                                                                                                                                                                                                                                    @endif
                                                                                                                                                                                                                                @endif
                                                                                                                                                                                                                                @if(in_array(19,json_decode($rol),false))
                                                                                                                                                                                                                                    @if(Request::path() == "inhabilitar")
                                                                                                                                                                                                                                        <li><a style="color: #ff851b;" href="{{route('inhabilitar')}}"><i class="fa fa-user-secret"></i>Administrar Usuarios</a></li>
                                                                                                                                                                                                                                    @else
                                                                                                                                                                                                                                        <li><a href="{{route('inhabilitar')}}"><i class="fa fa-user-secret"></i>Administrar Usuarios</a></li>
                                                                                                                                                                                                                                    @endif
                                                                                                                                                                                                                                @endif
                                                                                                                                                                                                                                @if(in_array(20,json_decode($rol),false))
                                                                                                                                                                                                                                    @if(Request::path() == "roles")
                                                                                                                                                                                                                                        <li><a style="color: #ff851b;" href="{{route('roles')}}"><i class="fa fa-key"></i>Administrar Roles</a></li>
                                                                                                                                                                                                                                    @else
                                                                                                                                                                                                                                        <li><a href="{{route('roles')}}"><i class="fa fa-key"></i>Administrar Roles</a></li>
                                                                                                                                                                                                                                    @endif
                                                                                                                                                                                                                                @endif



                                                                                                                                                                                                                            </ul>


                                                                                                                                                                                                                            </li>

                                                                                                                                                                                                                            @if(Request::path() == "galmacen")                                                                                                                                                                       )
                                                                                                                                                                                                                            <li class="treeview menu-open">
                                                                                                                                                                                                                            @else
                                                                                                                                                                                                                                <li class="treeview">
                                                                                                                                                                                                                                    @endif
                                                                                                                                                                                                                                    <a href="#">
                                                                                                                                                                                                                                        <i class="fa fa-building"></i>
                                                                                                                                                                                                                                        <span>Almacen General</span>
                                                                                                                                                                                                                                        <span class="pull-right-container">
                                                                                                                                                                                          <i class="fa fa-angle-left pull-right"></i>
                                                                                                                                                                                        </span>
                                                                                                                                                                                                                                    </a>
                                                                                                                                                                                                                                    @if(Request::path() == "galmacen")
                                                                                                                                                                                                                                        <ul class="treeview-menu" style="display: block;">
                                                                                                                                                                                                                                            @else
                                                                                                                                                                                                                                                <ul class="treeview-menu" >
                                                                                                                                                                                                                                                    @endif
                                                                                                                                                                                                                                                    @if(in_array(21,json_decode($rol),false))
                                                                                                                                                                                                                                                        @if(Request::path() == "galmacen")
                                                                                                                                                                                                                                                            <li><a style="color: #ff851b;" href="{{route('galmacen')}}"><i class="fa fa-circle-o"></i> Listar</a></li>
                                                                                                                                                                                                                                                        @else
                                                                                                                                                                                                                                                            <li><a href="{{route('galmacen')}}"><i class="fa fa-circle-o"></i> Listar</a></li>
                                                                                                                                                                                                                                                        @endif
                                                                                                                                                                                                                                                    @endif
                                                                                                                                                                                                                                                </ul>
                                                                                                                                                                                                                                                </li>
                                                                                                                                                                                                                                                @if(Request::path() == "mensajes/recibidos"
                                                                                                                                                                                                                                        or Request::path() == "mensajes/enviados"
                                                                                                                                                                                                                                         or Request::path() == "mensajes/nuevomensaje")                                                                                                                                                                       )
                                                                                                                                                                                                                                                <li class="treeview menu-open">
                                                                                                                                                                                                                                                @else
                                                                                                                                                                                                                                                    <li class="treeview">
                                                                                                                                                                                                                                                        @endif
                                                                                                                                                                                                                                                        <a href="#">
                                                                                                                                                                                                                                                            <i class="fa  fa-commenting"></i>
                                                                                                                                                                                                                                                            <span>Sistema de Correos</span>
                                                                                                                                                                                                                                                            <span class="pull-right-container">
                                                                                                                                                                                          <i class="fa fa-angle-left pull-right"></i>
                                                                                                                                                                                        </span>
                                                                                                                                                                                                                                                        </a>


                                                                                                                                                                                                                                                        @if(Request::path() == "mensajes/recibidos"
                                                                                                                                                                                                                                        or Request::path() == "mensajes/enviados"
                                                                                                                                                                                                                                         or Request::path() == "mensajes/nuevomensaje")
                                                                                                                                                                                                                                                            <ul class="treeview-menu" style="display: block;">
                                                                                                                                                                                                                                                                @else
                                                                                                                                                                                                                                                                    <ul class="treeview-menu" >
                                                                                                                                                                                                                                                                        @endif
                                                                                                                                                                                                                                                                        @if(in_array(18,json_decode($rol),false))
                                                                                                                                                                                                                                                                            @if(Request::path() == "mensajes/nuevomensaje")
                                                                                                                                                                                                                                                                                <li><a style="color: #ff851b;" href="{{route('mensajes-create')}}"><i class="fa fa-envelope"></i> Enviar Mensaje</a></li>
                                                                                                                                                                                                                                                                            @else
                                                                                                                                                                                                                                                                                <li><a href="{{route('mensajes-create')}}"><i class="fa fa-envelope"></i> Enviar Mensaje</a></li>
                                                                                                                                                                                                                                                                            @endif
                                                                                                                                                                                                                                                                        @endif
                                                                                                                                                                                                                                                                        @if(in_array(19,json_decode($rol),false))
                                                                                                                                                                                                                                                                            @if(Request::path() == "mensajes/recibidos")
                                                                                                                                                                                                                                                                                <li><a style="color: #ff851b;" href="{{route('mensajes-recibidos')}}"><i class="fa  fa-mail-forward "></i>Mensajes Recibidos</a></li>
                                                                                                                                                                                                                                                                            @else
                                                                                                                                                                                                                                                                                <li><a href="{{route('mensajes-recibidos')}}"><i class="fa  fa-mail-forward "></i>Mensajes Recibidos</a></li>
                                                                                                                                                                                                                                                                            @endif
                                                                                                                                                                                                                                                                        @endif
                                                                                                                                                                                                                                                                        @if(in_array(20,json_decode($rol),false))
                                                                                                                                                                                                                                                                            @if(Request::path() == "mensajes/enviados")
                                                                                                                                                                                                                                                                                <li><a style="color: #ff851b;" href="{{route('mensajes-enviados')}}"><i class="fa fa-mail-reply"></i>Mensajes Enviados</a></li>
                                                                                                                                                                                                                                                                            @else
                                                                                                                                                                                                                                                                                <li><a href="{{route('mensajes-enviados')}}"><i class="fa fa-mail-reply"></i>Mensajes Enviados</a></li>
                                                                                                                                                                                                                                                                            @endif
                                                                                                                                                                                                                                                                        @endif



                                                                                                                                                                                                                                                                    </ul>


                                                                                                                                                                                                                                                                    </li>



    </section>
    <!-- /.sidebar -->
</aside>
