@extends('layouts.appmod')

@section('content')
    <div class="box padding_box1">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-user"></i>Editar Cliente</h2>
                        <hr>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="x_content">
                            @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <p>{!! Session::get('message') !!}</p>
                                </div>
                            @endif
                        </div>
                        <div class="x_content">
                            @if(Session::has('message2'))
                                <div class='alert alert-danger'>
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <p>{!! Session::get('message2') !!}</p>
                                </div>
                            @endif
                        </div>
                        <form class="form-horizontal" role="form" method="POST"
                              action="{{route('clientes-update-A',$clientes->IDclientes)}}"
                              enctype="multipart/form-data">
                            {!! csrf_field() !!}

                            <div class="col-sm-4 col-sm-offset-1">
                                <div class="form-group {{ $errors->has('Nombres') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Nombres</label>
                                    <input type="text" class="form-control" name="Nombres" value="{{$clientes->Nombres}}" placeholder="Nombres">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Nombres'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Nombres') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Apellidos') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Apellidos</label>
                                    <input type="text" class="form-control" name="Apellidos" value="{{$clientes->Apellidos}}" placeholder="Apellidos">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Apellidos'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Apellidos') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('CedulaRUC') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Celuda/RUC</label>
                                    <input type="text" class="form-control" name="CedulaRUC" value="{{$clientes->CedulaRUC}}" placeholder="Cedula / RUC">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('CedulaRUC'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('CedulaRUC') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('FechaNacimiento') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Fecha de Nacimiento</label>
                                    <input type="date" class="form-control" name="FechaNacimiento" value="{{$clientes->FechaNacimiento}}" placeholder="FechaNacimiento">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('FechaNacimiento'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('FechaNacimiento') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Edad') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Edad</label>
                                    <input type="number" class="form-control" name="Edad" value="{{$clientes->Edad}}" placeholder="Edad">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Edad'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Edad') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Genero') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Genero</label>
                                    <select class="form-control" name="Genero" id="Genero">
                                        @if($clientes->Genero == "M") <option value="M">Masculino</option>
                                        @elseif($clientes->Genero == "F") <option value="F">Femenino</option>
                                        @elseif($clientes->Genero == "I") <option value="I">Indeterminado</option>
                                        @else <option value="">Seleccionar Genero</option>  @endif
                                        <option value="M">Masculino</option>
                                        <option value="F">Femenino</option>
                                        <option value="I">Indeterminado</option>
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Genero'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Genero') }}</strong>
                                      </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('FechaInscripcion') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Fecha de Registro de Cliente</label>
                                    <input type="date" class="form-control" name="FechaInscripcion" value="{{date('y-mm-dd')}}" placeholder="Fecha de Inscripcion">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('FechaInscripcion'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('FechaInscripcion') }}</strong>
                                      </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('EstadoCivil') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Estado Civil</label>
                                    <select class="form-control" name="EstadoCivil" id="EstadoCivil">
                                        @if($clientes->EstadoCivil == "C") <option value="M">Casado</option>
                                        @elseif($clientes->EstadoCivil == "S") <option value="F">Soltero</option>
                                        @elseif($clientes->EstadoCivil == "V") <option value="I">Viudo</option>
                                        @elseif($clientes->EstadoCivil == "D") <option value="I">Divorciado</option>
                                        @else <option value="">Seleccionar Estado Civil</option>  @endif
                                        <option value="C">Casado</option>
                                        <option value="S">Soltero</option>
                                        <option value="V">Viudo</option>
                                        <option value="D">Divorciado</option>
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('EstadoCivil'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('EstadoCivil') }}</strong>
                                      </span>
                                    @endif
                                </div>


                                <div class="form-group {{ $errors->has('TelefonoDom') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Telefono Domicilio</label>
                                    <input type="number" class="form-control" name="TelefonoDom" value="{{$clientes->TelefonoDom}}" placeholder="Telefono Domicilio">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('TelefonoDom'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('TelefonoDom') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('TelefonoMovil') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Telefono Celular</label>
                                    <input type="number" class="form-control" name="TelefonoMovil" value="{{$clientes->TelefonoMovil}}" placeholder="Telefono Movil">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('TelefonoMovil'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('TelefonoMovil') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('TelefonoTrabajo') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Telefono Celular</label>
                                    <input type="number" class="form-control" name="TelefonoTrabajo" value="{{$clientes->TelefonoTrabajo}}" placeholder="Telefono Trabajo">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('TelefonoTrabajo'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('TelefonoTrabajo') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('NumFamiliares') ? ' has-error' : '' }} has-feedback">
                                    <label for="">NumFamiliares</label>
                                    <input type="number" class="form-control" name="NumFamiliares" value="{{$clientes->NumFamiliares}}" placeholder="Numero de Familiares">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('NumFamiliares'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('NumFamiliares') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('LugarTrabajo') ? ' has-error' : '' }} has-feedback">
                                    <label for="">LugarTrabajo</label>
                                    <input type="text" class="form-control" name="LugarTrabajo" value="{{$clientes->LugarTrabajo}}" placeholder="Lugar de Trabajo">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('LugarTrabajo'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('LugarTrabajo') }}</strong>
                                      </span>
                                    @endif
                                </div>



                            </div>
                            <div class="col-sm-4 col-sm-offset-1">

                                <div class="form-group {{ $errors->has('DireccionOficina') ? ' has-error' : '' }} has-feedback">
                                    <label for="">DireccionOficina</label>
                                    <input type="text" class="form-control" name="DireccionOficina" value="{{$clientes->DireccionOficina}}" placeholder="Lugar de Trabajo">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('DireccionOficina'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('DireccionOficina') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Ocupacion') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Profesion/Oficio</label>
                                    <input type="text" class="form-control" name="Ocupacion" value="{{$clientes->Ocupacion}}" placeholder="Profesion/Oficio">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Ocupacion'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Ocupacion') }}</strong>
                                      </span>
                                    @endif
                                </div>


                                <div class="form-group {{ $errors->has('NumHijos') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Numero de HIjos</label>
                                    <input type="number" class="form-control" name="NumHijos" value="{{$clientes->NumHijos}}" placeholder="Numero de Hijos">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('NumHijos'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('NumHijos') }}</strong>
                                      </span>
                                    @endif
                                </div>



                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Email</label>
                                    <input type="email" class="form-control" name="email" value="{{$clientes->email}}" placeholder="Email Personal">
                                    <span class="fa fa-at form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Precio') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Precio</label>
                                    <input type="number" step="any" class="form-control" name="Precio" value="{{$clientes->Precio}}" placeholder="Precio">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Precio'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Precio') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Descuento') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Descuento</label>
                                    <input type="number" step="any" class="form-control" name="Descuento" value="{{$clientes->Descuento}}" placeholder="Descuento">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Descuento'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Descuento') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Referencia') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Referencia</label>
                                    <input type="text" class="form-control" name="Referencia" value="{{$clientes->Referencia}}" placeholder="Referencia">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Referencia'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Referencia') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('CtaContable') ? ' has-error' : '' }} has-feedback">
                                    <label for="">CtaContable</label>
                                    <input type="text" class="form-control" name="CtaContable" value="{{$clientes->CtaContable}}" placeholder="CtaContable">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('CtaContable'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('CtaContable') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('ValorLimiteCredito') ? ' has-error' : '' }} has-feedback">
                                    <label for="">ValorLimiteCredito</label>
                                    <input type="text" class="form-control" name="ValorLimiteCredito" value="{{$clientes->ValorLimiteCredito}}" placeholder="ValorLimiteCredito">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('ValorLimiteCredito'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('ValorLimiteCredito') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Notas') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Notas</label>
                                    <input type="text" class="form-control" name="Notas" value="{{$clientes->Notas}}" placeholder="Notas">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Notas'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Notas') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('IDpref') ? ' has-error' : '' }} has-feedback">
                                    <label for="IDpref">Empresa</label>
                                    <select class="form-control" name="IDpref" id="IDpref">
                                        <option value="{{$clientes->IDpref}}">Empresa actual: {{$clientes->empresas->NombreEstablecim}}</option>
                                        @foreach($empresas as $emp)
                                            <option value="{{$emp->IDpref}}">{{$emp->NombreEstablecim}}</option>
                                        @endforeach
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('IDpref'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('IDpref') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('IDalmacen') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Almacen</label>
                                    <select class="form-control" name="IDalmacen" id="IDalmacen" readonly>
                                        @if(isset($clientes->almacenes->nombre) )
                                            <option value="{{$clientes->IDalmacen}}">{{$clientes->almacenes->nombre}}</option>
                                        @endif
                                        @foreach($almacen as $alma)
                                                <option value="{{$alma->IDalmacen}}">{{$alma->nombre}}</option>
                                        @endforeach
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('IDalmacen'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('IDalmacen') }}</strong>
                                      </span>
                                    @endif
                                </div>




                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-warning cerrarpermiso"
                                            data-dismiss="modal">Guardar cambios
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="{!! asset('admin-lte/plugins/select2/select2.min.js') !!}"></script>
@endsection