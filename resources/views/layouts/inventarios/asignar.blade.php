@extends('layouts.appmod')


@section('content')
    <section class="content-header">
        <h1>
            Asignar articulo en inventario
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Asignar articulo en inventario</li>
        </ol>
        <div class="x_content">
            @if(Session::has('message'))
                <div class='alert alert-success'>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <p>{!! Session::get('message') !!}</p>
                </div>
            @endif
        </div>
        <div class="x_content">
            @if(Session::has('message2'))
                <div class='alert alert-danger'>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <p>{!! Session::get('message2') !!}</p>
                </div>
            @endif
        </div>
    </section>
    <section class="content">
        <form class="form-horizontal" role="form" method="POST" action="{{route('inventario-asignacionstore-A')}}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <!---------------------------------------------------------------------------------------------------------------->
            <div class="row">
                <div class="col-md-12">
                    <div class="box ">
                        <div class="box-header with-border">
                            <h3 class="box-title">General</h3>

                            <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                                <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                                <!--<div class="btn-group">
                                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-wrench"></i></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>-->
                                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">


                                    <div class="form-group">
                                        <label for="nombre">Almacen</label>
                                        <select class="form-control" name="IDalmacen" id="IDalmacen">
                                            <option value="">Selecciona</option>
                                            @foreach($almacen as $alma)
                                                <option value="{{$alma->IDalmacen}}">{{$alma->nombre}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="apellido">Inventario</label>
                                        <select class="form-control" name="IDinventarios" id="IDinventarios" required>
                                            <option value="">Selecciona</option>

                                        </select>
                                    </div>


                                </div>



                                <!-- /.row -->
                            </div>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="apellido">Articulo</label>
                                        <select class="form-control" name="IDarticulo" id="IDarticulo">
                                            <option value="">Selecciona</option>
                                            @foreach($articulo as $arti)
                                                <option value="{{$arti->IDarticulo}}">{{$arti->Articulo}} {{$arti->Cod}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                </div>
                            </div>

                            <div class=" row">
                                <div style="align-items: center; align-content: center; text-align: center;">
                                <label for="apellido">Información de Articulo Seleccionado</label>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="apellido">Nombre de Articulo</label>
                                        <input type="text" class="form-control" id="Articulo" readonly>
                                    </div>


                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="apellido">Costo</label>
                                        <input type="text" class="form-control" id="Costo" readonly>
                                    </div>


                                </div>
                            </div>
                               <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="apellido">Cantidad Disponible</label>
                                        <input type="text" class="form-control" id="Cantidad" readonly>
                                    </div>


                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="apellido">Codigo</label>
                                        <input type="text" class="form-control" id="Cod" readonly>
                                    </div>


                                </div>

                            </div>

                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
<div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-1">
            <a href="{{route('inventario-asignacion-excel-A')}}" class="btn btn-warning pull-right">Cargar con Excel</a>
            </div>
            <div class="col-md-1">
            <button type="submit" class="btn btn-info pull-right">Guardar</button>
            </div>
            </div>

        </form>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        $('#IDalmacen').change(function(e){
            e.preventDefault();
            var id = $(this).val();
            var APP_URL = {!! json_encode(url('/')) !!};
            var url = APP_URL + '/inventario/consulinventario/'+ id;
            $.get(url,function(resul){
                console.log(resul);
                if(resul.registro == "true"){
                    $("#IDinventarios").empty();
                alert('Ahora selecciona el inventario para este almacen');
                $.each(resul.resultado, function(id,value) {
                    $("#IDinventarios").append('<option value="' + value.IDinventarios + '">' + value.nombre + '</option>');
                });
                    }else{
                    $("#IDinventarios").empty();
                    alert('No hay inventarios para este almacen');
                }

            });
        });
        $('#IDarticulo').change(function(e){
            e.preventDefault();

            var id = $(this).val();
            var APP_URL = {!! json_encode(url('/')) !!};
            var url = APP_URL + '/inventario/consularticulo/'+ id;
            $.get(url,function(resul){
                console.log(resul);
                $('#Articulo').val(resul.Articulo);
                $('#Costo').val(resul.Costo);
                $('#Cantidad').val(resul.Cantidad);
                $('#Cod').val(resul.Cod);

            });
        });
    </script>
@endsection
