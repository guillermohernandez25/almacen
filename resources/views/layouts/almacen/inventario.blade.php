@extends('layouts.appmod')

@section('css')
    <style>
    #inventarios{
    overflow:scroll;
    height:200px;
    width:500px;
    }
    </style>
    @endsection

@section('content')
    <section class="content-header">
        <h1>
            Inventarios de {{$almacen->nombre}}
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Listado de Inventarios</li>
        </ol>
        <div class="x_content">
            @if(Session::has('message'))
                <div class='alert alert-success'>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <p>{!! Session::get('message') !!}</p>
                </div>
            @endif
        </div>
        <div class="x_content">
            @if(Session::has('message2'))
                <div class='alert alert-danger'>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <p>{!! Session::get('message2') !!}</p>
                </div>
            @endif
        </div>

    </section>
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listado de Inventarios de {{$almacen->nombre}}</h3>

                        <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                            <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <!--<div class="btn-group">
                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-wrench"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>-->
                            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                        <table id="inventarios" class="table table-bordered" role="grid" aria-describedby="proveedor_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="inventarios" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Almacen</th>
                                                <th class="sorting" tabindex="0" aria-controls="inventarios" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 149px;">Nombre de Inventario</th>
                                                <th class="sorting" tabindex="0" aria-controls="inventarios" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 149px;">Descripción de Inventario</th>
                                                <th class="sorting" tabindex="0" aria-controls="inventarios" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 200px;">Acciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($inventarios as $inventa)
                                            <tr role="row" class="odd">
                                                <!--<td class="sorting_1">{{$inventa->IDinventarios}}</td>-->
                                                    <td>{{$inventa->almacenes->nombre }}</td>
                                                    <td>{{$inventa->nombre }}</td>
                                                    <td>{{$inventa->descripcion }}</td>

                                                <td><a class="btn btn-warning btn-xs" href="{{ route('inventario-edit-A', $inventa->IDinventarios) }}" data-toggle="tooltip" data-placement="left" title="Editar">
                                                        <i class="fa fa-pencil fa-lg"></i>
                                                    </a>
                                                    <a class="btn btn-danger btn-xs" href="{{ route('inventario-destroy-A', $inventa->IDinventarios) }}" onclick="return confirm('Seguro que desea Eliminar el Articulo de este inventario {{$inventa->IDinventarios}}?')" data-toggle="tooltip" data-placement="left" title="Eliminar">
                                                        <i class="fa fa-trash fa-lg"></i>
                                                    </a>
                                                    <a class="btn btn-success btn-xs" href="{{ route('inventario-ver-A', $inventa->IDinventarios) }}" data-toggle="tooltip" data-placement="left" title="Ver Articulos">
                                                        <i class="fa fa-sitemap fa-lg"></i> Ver Articulos de Inventario
                                                    </a>

                                                       </td>
                                            </tr>
                                                 @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td></td>

                                            </tr>
                                            </tfoot>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <!--<div class="col-md-4">
                                <p class="text-center">
                                    <strong>Configuración Global</strong>
                                </p>
                                <form class="form-horizontal" role="form" method="POST" action="{{route('color_store')}}" enctype="multipart/form-data">
                                    {!! csrf_field() !!}
                                <div class="form-group">
                                    <label>Listado de Colores</label>
                                    <select class="form-control" name="color">

                                        <option value=""></option>

                                    </select>
                                </div>
                                <button type="submit" class="btn btn-info pull-right">Guardar</button>
                                </form>

                            </div>-->
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>

                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>

    </section>
@endsection

@section('script')
    <script>
        $(function () {

            $('#inventarios').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    {
                        extend:'copy',
                        title: '<?php echo __('Almacen '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'csv',
                        title: '<?php echo __('Almacen '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'excel',
                        title: '<?php echo __('Almacen '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'pdf',
                        title: '<?php echo __('Almacen '); echo(date('Y-m-d')); ?>'
                    },
                    ,
                    {
                        extend:'print',
                        title: '<?php echo __('Almacen '); echo(date('Y-m-d')); ?>'
                    }
                ],
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 5,
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>

    @endsection
