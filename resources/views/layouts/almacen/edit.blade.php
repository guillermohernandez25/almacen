@extends('layouts.appmod')


@section('content')
    <section class="content-header">
        <h1>
            Nuevo Proveedor
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Nuevo Proveedor</li>
        </ol>
    </section>

        <section class="content">
            <form class="form-horizontal" role="form" method="POST" action="{{route('almacenes-update-A', $almacen->IDalmacen)}}" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <!---------------------------------------------------------------------------------------------------------------->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box ">
                            <div class="box-header with-border">
                                <h3 class="box-title">General</h3>

                                <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                                    <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <!--<div class="btn-group">
                                        <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-wrench"></i></button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>-->
                                    <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="IDPref">Empresa</label>
                                        <select class="form-control" name="IDpref" id="IDpref" required>
                                            <option value="{{$almacen->IDpref}}">Empresa actual: {{$almacen->empresa->NombreEstablecim}}</option>
                                            @foreach($empresa as $emp)
                                                <option value="{{$emp->IDpref}}">{{$emp->NombreEstablecim}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4">


                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nombre</label>
                                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="{{$almacen->nombre}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Dirección</label>
                                            <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" value="{{$almacen->direccion}}">
                                        </div>

                                    </div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Telefono principal</label>
                                            <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono Principal" value="{{$almacen->telefono}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Descripcion</label>
                                            <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripcion" value="{{$almacen->descripcion}}">
                                        </div>

                                    </div>

                                    <!-- /.row -->
                                </div>

                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box ">
                            <div class="box-header with-border">
                                <h3 class="box-title">Administrador de Almacen</h3>

                                <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                                    <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <!--<div class="btn-group">
                                        <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-wrench"></i></button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>-->
                                    <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4">


                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Administrador</label>
                                            <select class="form-control" name="IDadministradores" id="IDadministradores">
                                                <option value="{{$almacen->IDadministradores}}">Administrador actual: {{$almacen->administradores->nombre}} {{$almacen->administradores->dni}}</option>
                                                @foreach($administradores as $admin)
                                                    <option value="{{$admin->IDadministradores}}">{{$admin->nombre}} {{$admin->dni}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nombre</label>
                                            <input type="text" class="form-control" id="nombreadmin" name="nombreadmin" placeholder="Nombre" value="{{$administrador->nombre}}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Apellido</label>
                                            <input type="text" class="form-control" id="apellidoadmin" name="apellidoadmin" placeholder="Apellido" value="{{$administrador->apellido}}" readonly>
                                        </div>

                                    </div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">DNI</label>
                                            <input type="text" class="form-control" id="dniadmin" name="dniadmin" placeholder="DNI" value="{{$administrador->dni}}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Telefono principal</label>
                                            <input type="text" class="form-control" id="telefonoadmin" name="telefonoadmin" placeholder="Telefono Principal" value="{{$administrador->telefono}}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Dirección</label>
                                            <input type="text" class="form-control" id="direccionadmin" name="direccionadmin" placeholder="Dirección" value="{{$administrador->direccion}}" readonly>
                                        </div>



                                    </div>

                                    <!-- /.row -->
                                </div>

                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>


                <button type="submit" class="btn btn-info pull-right">Guardar</button>

            </form>
        </section>
@endsection
@section('script')
    <script type="text/javascript">
        $('#administrador_id').change(function(e){
            e.preventDefault();
            var id = $(this).val();
            var APP_URL = {!! json_encode(url('/')) !!};
            var url = APP_URL + '/almacenes/administradores/'+ id;
            $.get(url,function(resul){
                console.log(resul);
                $('#nombreadmin').val(resul.nombre);
                $('#apellidoadmin').val(resul.apellido);
                $('#dniadmin').val(resul.dni);
                $('#telefonoadmin').val(resul.telefono);
                $('#direccionadmin').val(resul.direccion);
            })
        })

    </script>
@endsection
