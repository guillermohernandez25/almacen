<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.4.0
    </div>
    <strong>Copyright © 2018 <a href="https://adminlte.io">Factumarket</a>.</strong>
</footer>