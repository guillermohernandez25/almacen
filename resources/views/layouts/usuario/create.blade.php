@extends('layouts.appmod')
@section('script')
    <script type="text/javascript">
        $('.empresa').change(function(e){
            e.preventDefault();
            var id = $(this).val();
            var APP_URL = {!! json_encode(url('/')) !!};
            var url = APP_URL + '/c_almacen/'+ id;
            $.get(url,function(resul){
                console.log(resul);
                if(resul.registro == "true"){
                    $('.almacen').empty();
                    alert('Ahora selecciona el almacen para esta empresa');
                    $(".almacen").append('<option value="">Selecciona un Almacen</option>');
                    $.each(resul.resultado, function(id,value) {
                        $(".almacen").append('<option value="' + value.IDalmacen + '">' + value.nombre + '</option>');
                    });
                }else{
                    alert('No hay almacen para esta empresa');
                    $('.almacen').empty();
                }

            });
        });


    </script>
@endsection

@section('content')
    <div class="box padding_box1">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-user"></i>Crear Usuario</h2>
                        <hr>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="x_content">
                            @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <p>{!! Session::get('message') !!}</p>
                                </div>
                            @endif
                        </div>
                        <div class="x_content">
                            @if(Session::has('message2'))
                                <div class='alert alert-danger'>
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <p>{!! Session::get('message2') !!}</p>
                                </div>
                            @endif
                        </div>
                        <form class="form-horizontal" role="form" method="POST"
                              action="{{route('inhabilitar_store')}}"
                              enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="col-sm-4 col-sm-offset-1">
                                <div class="form-group {{ $errors->has('Nombres') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Nombres</label>
                                    <input type="text" class="form-control" name="Nombres" value="" placeholder="Nombres" required>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Nombres'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Nombres') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Apellidos') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Apellidos</label>
                                    <input type="text" class="form-control" name="Apellidos" value="" placeholder="Apellidos" required>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Apellidos'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Apellidos') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('CedulaRUC') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Cedula</label>
                                    <input type="text" class="form-control" name="CedulaRUC" value="" placeholder="Cedula / RUC" required>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('CedulaRUC'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('CedulaRUC') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('FechaNacimiento') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Fecha de Nacimiento</label>
                                    <input type="date" class="form-control" name="FechaNacimiento" value="" placeholder="FechaNacimiento" required>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('FechaNacimiento'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('FechaNacimiento') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Edad') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Edad</label>
                                    <input type="number" class="form-control" name="Edad" value="" placeholder="Edad" required>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Edad'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Edad') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Genero') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Genero</label>
                                    <select class="form-control" name="Genero" id="Genero" required>
                                         <option value="">Seleccionar Genero</option>
                                        <option value="M">Masculino</option>
                                        <option value="F">Femenino</option>
                                        <option value="I">Indeterminado</option>
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Genero'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Genero') }}</strong>
                                      </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('EstadoCivil') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Estado Civil</label>
                                    <select class="form-control" name="EstadoCivil" id="EstadoCivil" required>
                                        <option value="">Seleccionar Estado Civil</option>
                                        <option value="C">Casado</option>
                                        <option value="S">Soltero</option>
                                        <option value="V">Viudo</option>
                                        <option value="D">Divorciado</option>
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('EstadoCivil'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('EstadoCivil') }}</strong>
                                      </span>
                                    @endif
                                </div>


                                <div class="form-group {{ $errors->has('TelfCasa') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Telefono Casa</label>
                                    <input type="number" class="form-control" name="TelfCasa" value="" placeholder="Telefono Casa" required>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('TelfCasa'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('TelfCasa') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('TelfCelular') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Telefono Celular</label>
                                    <input type="number" class="form-control" name="TelfCelular" value="" placeholder="Telefono Celular" required>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('telefono'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('TelfCelular') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Conyuge') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Conyuge</label>
                                    <input type="text" class="form-control" name="Conyuge" value="" placeholder="Conyuge" required>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Conyuge'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Conyuge') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Direccion') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Dirección</label>
                                    <input type="text" class="form-control" name="Direccion" value="" placeholder="Dirección" required>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Direccion'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Direccion') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('ProfesionOficio') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Profesion/Oficio</label>
                                    <input type="text" class="form-control" name="ProfesionOficio" value="" placeholder="ProfesionOficio" required>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('ProfesionOficio'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('ProfesionOficio') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('NumCargasFamiliares') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Numero de Cargas Familiares</label>
                                    <input type="number" class="form-control" name="NumCargasFamiliares" value="" placeholder="NumCargasFamiliares" required>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('NumCargasFamiliares'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('NumCargasFamiliares') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('NumHijos') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Numero de HIjos</label>
                                    <input type="number" class="form-control" name="NumHijos" value="" placeholder="NumHijos" required>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('NumHijos'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('NumHijos') }}</strong>
                                      </span>
                                    @endif
                                </div>

                            </div>
                            <div class="col-sm-4 col-sm-offset-1">
                                <div class="form-group {{ $errors->has('Comentarios') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Comentarios</label>
                                    <input type="text" class="form-control" name="Comentarios" value="" placeholder="Comentarios">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Comentarios'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Comentarios') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('IDpref') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Empresa</label>
                                    <select class="form-control empresa" name="IDpref" id="IDpref" required>
                                        <option value="">Selecciona Empresa</option>
                                        @foreach($empresas as $empresa)
                                            <option value="{{$empresa->IDpref}}">{{$empresa->NombreEstablecim}}</option>
                                        @endforeach
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('IDpref'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('IDpref') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('IDalmacen') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Almacen</label>
                                    <select class="form-control almacen" name="IDalmacen" id="IDalmacen" required>

                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('IDpref'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('IDpref') }}</strong>
                                      </span>
                                    @endif
                                </div>
                                <!--<div class="form-group {{ $errors->has('IDHorario') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Horario</label>
                                    <select class="form-control" name="IDHorario" id="IDHorario" readonly>
                                    @foreach($horarios as $horario)
                                        <option value="{{$horario->HorarioID}}">{{$horario->HorarioID}}</option>
                                        @endforeach
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('IDHorario'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('IDHorario') }}</strong>
                                      </span>
                                    @endif
                                </div>-->

                                <div class="form-group {{ $errors->has('FechaContratacion') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Fecha de Contratación</label>
                                    <input type="date" class="form-control" name="FechaContratacion" value="" placeholder="FechaContratacion" required>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('FechaContratacion'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('FechaContratacion') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Cargos_IDCargo') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Cargo</label>
                                    <select class="form-control" name="Cargos_IDCargo" id="Cargos_IDCargo">
                                        @foreach($cargos as $cargo)
                                        <option value="{{$cargo->IDcargos}}">{{$cargo->NombreCargo}}</option>
                                            @endforeach
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Cargos_IDCargo'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Cargos_IDCargo') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Salarios_IDSalario') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Salario</label>
                                    <select class="form-control" name="Salarios_IDSalario" id="Salarios_IDSalario">
                                        @foreach($salarios as $salario)
                                        <option value="{{$salario->IDsalarios}}">{{$salario->NombreSalario}}</option>
                                            @endforeach
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Salarios_IDSalario'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Salarios_IDSalario') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Email</label>
                                    <input type="email" class="form-control" name="email" value="" placeholder="Email Personal" required>
                                    <span class="fa fa-at form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('EmailEmpresarial') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Email</label>
                                    <input type="email" class="form-control" name="EmailEmpresarial" value="" placeholder="Email Empresarial" required>
                                    <span class="fa fa-at form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('EmailEmpresarial'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('EmailEmpresarial') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Password</label>
                                    <input type="password" class="form-control" name="password" value=""
                                           placeholder="No usar si no es requerido" required>
                                    <span class="fa fa-key form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('confpassword') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Confirmar Password</label>
                                    <input type="password" class="form-control" name="confpassword" value=""
                                           placeholder="No usar si no es requerido" required>
                                    <span class="fa fa-key form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('confpassword'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('confpassword') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('roles') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Definir ROL</label>
                                    <select class="form-control" name="roles" id="roles" required>
                                    @foreach($roles as $role)
                                            <option value="{{$role->id}}">{{$role->tipo}}</option>
                                        @endforeach
                                    </select>
                                    <span class="fa fa-key form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('roles'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('roles') }}</strong>
                                      </span>
                                    @endif
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-warning cerrarpermiso"
                                            data-dismiss="modal">Guardar
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="{!! asset('admin-lte/plugins/select2/select2.min.js') !!}"></script>
@endsection