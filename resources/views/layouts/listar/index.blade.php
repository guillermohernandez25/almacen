@extends('layouts.appmod')

@section('styles')
@endsection

@section('actual','Listar Kardex')
@section('titulo','Kardex')
@section('detalle','en esta parte se observan los articulos registrados en kardex')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Listado de Productos</h3>
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <div class="table-header">
                Articulos en movimiento (Kardex)
            </div>
            <div>
                <?php $cont = 0; ?>
                <table id="kardex" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Empresa/Almacen</th>
                            <th>Categoria</th>
                            <th>Nro. Factura</th>
                            <th>Codigo de Articulo</th>
                            <th>Entrada</th>
                            <th>Salida</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($kardex as $item)
                        <tr>
                            <td>{{ $item->created_at }}</td>
                            <td>@if(isset($item->almacen->nombre)) {{ $item->empresa->NombreEstablecim }} / {{ $item->almacen->nombre }} @else Aun no se asigno almacen @endif</td>
                            <td>@if(isset($item->categorias->Categoria)) {{$item->categorias->Categoria}} @else No se reconoce la Categoria @endif</td>
                            <td align="center"><b>{{ $item->mov_factura }}</b></td>
                            <td>{{ $item->articulos->Cod.' - '.$item->articulos->Presentacion }}</td>
                            <td style="color:green;text-align:right;"><b>{{ $item->Mov_entrada }}</b></td>
                            <td style="color:red;text-align:right;"><b>{{ $item->Mov_salida }}</b></td>
                            <td style="color:blue;text-align:right;"><b>{{ $item->Mov_total }}</b></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {

            $('#kardex').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    {
                        extend:'copy',
                        title: '<?php echo __('Kardex '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'csv',
                        title: '<?php echo __('Kardex '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'excel',
                        title: '<?php echo __('Kardex '); echo(date('Y-m-d')); ?>'
                    },
                    {
                        extend:'pdf',
                        title: '<?php echo __('Kardex '); echo(date('Y-m-d')); ?>'
                    },
                    ,
                    {
                        extend:'print',
                        title: '<?php echo __('Kardex '); echo(date('Y-m-d')); ?>'
                    }
                ],
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 5,
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });

    </script>

@endsection
