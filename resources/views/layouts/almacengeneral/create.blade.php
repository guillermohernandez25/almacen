@extends('layouts.appmod')


@section('content')
    <section class="content-header">
        <h1>
            Nuevo Producto
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Nuevo Producto</li>
        </ol>
    </section>
    <section class="content">
        <form class="form-horizontal" role="form" method="POST" action="{{route('producto-store-A')}}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <!---------------------------------------------------------------------------------------------------------------->

            <div class="row">
                <div class="col-md-12">
                    <div class="box ">
                        <div class="box-header with-border">
                            <h3 class="box-title">General</h3>

                            <div class="" style="position: absolute;  right: 150px;  top: 5px; align-content: center; align-items: center; text-align: center">
                                <button style="width: 800px;" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                                <!--<div class="btn-group">
                                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-wrench"></i></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>-->
                                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">


                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Categoria">Categoria</label>
                                        <select class="form-control" name="IDCategoria" id="IDCategoria">
                                            <option value="">Seleccionar</option>
                                            @foreach($catego as $cate)
                                                <option value="{{$cate->IDCategoria}}">{{$cate->Categoria}}</option>
                                                @endforeach
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <label for="TipoCuenta">Articulo</label>
                                        <input type="text" class="form-control" id="Articulo" name="Articulo" placeholder="Articulo">
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="TipoTrans">Costo</label>
                                        <input type="text" class="form-control" id="Costo" name="Costo" placeholder="Costo">
                                    </div>
                                    <div class="form-group">
                                        <label for="CtaContable">Cantidad</label>
                                        <input type="text" class="form-control" id="Cantidad" name="Cantidad" placeholder="Cantidad">
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="CategoriaFacturable">TipoTrans</label>
                                        <select class="form-control" name="TipoTrans" id="TipoTrans" required>
                                            <option value="">Selecciona</option>
                                            <option value="7">Servicio</option>
                                            <option value="8">Producto</option>
                                        </select>
                                        <!--<input type="text" class="form-control" id="TipoTrans" name="TipoTrans" placeholder="TipoTrans">-->
                                    </div>
                                    <div class="form-group">
                                        <label for="IDDescuento">Consulta_Espec_Otro</label>
                                        <input type="text" class="form-control" id="Consulta_Espec_Otro" name="Consulta_Espec_Otro" placeholder="Consulta_Espec_Otro">
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ProductoElaborado">AplicaIva</label><br>
                                        <input style="position: relative;" class="minimal" type="radio" name="AplicaIva" value="si">Sí <br>
                                        <input style="position: relative;" class="minimalminimal" type="radio" name="AplicaIva" value="no">No <br>
                                        <!--<input type="text" class="form-control" id="AplicaIva" name="AplicaIva" placeholder="AplicaIva">-->
                                    </div>
                                    <div class="form-group">
                                        <label for="DebitarDelTotal">CtaContable</label>
                                        <input type="text" class="form-control" id="CtaContable" name="CtaContable" placeholder="CtaContable">
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>



                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <!--<div class="form-group">
                                        <label for="CategoriaFacturable">Codigo_Impuestos</label>
                                        <input type="text" class="form-control" id="Codigo_Impuestos" name="Codigo_Impuestos" placeholder="Codigo_Impuestos">
                                    </div>-->
                                    <div class="form-group">
                                        <label for="DebitarDelTotal">CodComponenteProductoElaborado</label>
                                        <input type="text" class="form-control" id="CodComponenteProductoElaborado" name="CodComponenteProductoElaborado" placeholder="CodComponenteProductoElaborado">
                                    </div>
                                    <div class="form-group">
                                        <label for="IDDescuento">FechaDeCompra</label>
                                        <input type="text" class="form-control" id="FechaDeCompra" name="FechaDeCompra" placeholder="FechaDeCompra">
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ProductoElaborado">Margen</label>
                                        <input type="text" class="form-control" id="Margen" name="Margen" placeholder="Margen">
                                    </div>
                                    <div class="form-group">
                                        <label for="DebitarDelTotal">MinIndisp</label>
                                        <input type="text" class="form-control" id="MinIndisp" name="MinIndisp" placeholder="MinIndisp">
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>


                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="CategoriaFacturable">RUC</label>
                                        <input type="text" class="form-control" id="RUC" name="RUC" placeholder="RUC">
                                    </div>
                                    <div class="form-group">
                                        <label for="IDDescuento">Presentacion</label>
                                        <input type="text" class="form-control" id="Presentacion" name="Presentacion" placeholder="Presentacion">
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ProductoElaborado">Contenido</label>
                                        <input type="text" class="form-control" id="Contenido" name="Contenido" placeholder="Contenido">
                                    </div>
                                    <div class="form-group">
                                        <label for="DebitarDelTotal">PVP</label>
                                        <input type="text" class="form-control" id="PVP" name="PVP" placeholder="PVP">
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>


                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="CategoriaFacturable">Cod</label>
                                        <input type="text" class="form-control" id="Cod" name="Cod" placeholder="Cod">
                                    </div>
                                    <div class="form-group">
                                        <label for="IDDescuento">IDEstante</label>
                                        <input type="text" class="form-control" id="IDEstante" name="IDEstante" placeholder="IDEstante">
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ProductoElaborado">AdvertirReposicion</label>
                                        <input type="text" class="form-control" id="AdvertirReposicion" name="AdvertirReposicion" placeholder="AdvertirReposicion">
                                    </div>
                                    <div class="form-group">
                                        <label for="DebitarDelTotal">FechaCaducidad</label>
                                        <input type="text" class="form-control" id="FechaCaducidad" name="FechaCaducidad" placeholder="FechaCaducidad">
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>


                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="CategoriaFacturable">AdvertirCaducidad</label>
                                        <input type="text" class="form-control" id="AdvertirCaducidad" name="AdvertirCaducidad" placeholder="AdvertirCaducidad">
                                    </div>
                                    <div class="form-group">
                                        <label for="IDDescuento">PDMa</label>
                                        <input type="text" class="form-control" id="PDMa" name="PDMa" placeholder="PDMa">
                                    </div>
                                    <!--<div class="form-group">
                                        <label for="IDDescuento">IDUser</label>
                                        <input type="text" class="form-control" id="IDUser" name="IDUser" placeholder="IDUser">
                                    </div>-->
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ProductoElaborado">FechaCierreTransaccion</label>
                                        <input type="text" class="form-control" id="FechaCierreTransaccion" name="FechaCierreTransaccion" placeholder="FechaCierreTransaccion">
                                    </div>
                                    <div class="form-group">
                                        <label for="DebitarDelTotal">TransaccionCerrada</label>
                                        <input type="text" class="form-control" id="TransaccionCerrada" name=TransaccionCerradad" placeholder="TransaccionCerrada">
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>


                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <!--<div class="form-group">
                                        <label for="CategoriaFacturable">IDCaja</label>
                                        <input type="text" class="form-control" id="IDCaja" name="IDCaja" placeholder="IDCaja">
                                    </div>-->
                                    <div class="form-group">
                                        <label for="CategoriaFacturable">Antelacion</label>
                                        <input type="text" class="form-control" id="Antelacion" name="Antelacion" placeholder="Antelacion">
                                    </div>
                                    <div class="form-group">
                                        <label for="IDDescuento">AlfaNumericoPosEstante</label>
                                        <input type="text" class="form-control" id="AlfaNumericoPosEstante" name="AlfaNumericoPosEstante" placeholder="AlfaNumericoPosEstante">
                                    </div>

                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ProductoElaborado">PDMi</label>
                                        <input type="text" class="form-control" id="PDMi" name="PDMi" placeholder="PDMi">
                                    </div>
                                    <div class="form-group">
                                        <label for="DebitarDelTotal">PE1</label>
                                        <input type="text" class="form-control" id="PE1" name="PE1" placeholder="PE1">
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>


                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="CategoriaFacturable">PE2</label>
                                        <input type="text" class="form-control" id="PE2" name="PE2" placeholder="PE2">
                                    </div>
                                    <!--<div class="form-group">
                                        <label for="IDDescuento">ProductoElaborado</label>
                                        <input type="text" class="form-control" id="ProductoElaborado" name="ProductoElaborado" placeholder="ProductoElaborado">
                                    </div>-->

                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">

                                </div>
                                <!-- /.row -->
                            </div>

                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>





            <button type="submit" class="btn btn-info pull-right">Guardar</button>

        </form>
    </section>

@endsection
