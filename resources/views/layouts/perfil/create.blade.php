@extends('layouts.appmod')

@section('content')


    <div class="box padding_box1">

        <div id="wrapper">
            <div id="login" class=" form" style="background-color: #FFF; padding: 0 20px; border-radius: 10px;">
                <section class="login_content">
                    <div class="clearfix"></div>

                    <div class="x_content">
                        @if(Session::has('message'))
                            <div class='alert alert-success'>
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <p>{!! Session::get('message') !!}</p>
                            </div>
                        @endif
                    </div>
                    <div class="x_content">
                        @if(Session::has('message2'))
                            <div class='alert alert-danger'>
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <p>{!! Session::get('message2') !!}</p>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <div class="x_title">
                                <h2><i class="fa fa-user"></i> Registrar Usuario</h2>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="col-sm-8 col-sm-offset-2">
                        <form class="form-horizontal" role="form" method="POST"
                              action="{{ route('manageUsuarioad-store-A') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}

                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
                                <label for="">Nombre Completo</label>
                                <input type="text" class="form-control" name="name" value="" placeholder="Nombre Completo">

                                <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                          <strong>{{ $errors->first('name') }}</strong>
                                      </span>
                                @endif
                            </div>



                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                                <label for="">Email</label>
                                <input type="email" class="form-control" name="email" value="" placeholder="Email">

                                <span class="fa fa-at form-control-feedback right" aria-hidden="true"></span>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                                <label for="">Password</label>
                                <input type="password" class="form-control" name="password" value=""
                                       placeholder="Password" required="">

                                <span class="fa fa-at form-control-feedback right" aria-hidden="true"></span>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('confpassword') ? ' has-error' : '' }} has-feedback">
                                <label for="">Confirmar Password</label>
                                <input type="password" class="form-control" name="confpassword" value=""
                                       placeholder="Confirmar Password" required="">

                                <span class="fa fa-at form-control-feedback right" aria-hidden="true"></span>

                                @if ($errors->has('confpassword'))
                                    <span class="help-block">
                                          <strong>{{ $errors->first('confpassword') }}</strong>
                                      </span>
                                @endif
                            </div>



                            <div class="form-actions">
                                <button type="submit" class="btn btn-success pull-right">
                                    Registrar <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>


                        </form>

                    </div>
                    <div class="clearfix"></div>

                </section>
            </div>


        </div>
    </div>

@endsection




   
