@extends('layouts.appmod')

@section('content')
    <div class="box padding_box1">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-user"></i>Editar Usuario</h2>
                        <hr>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="x_content">
                            @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <p>{!! Session::get('message') !!}</p>
                                </div>
                            @endif
                        </div>
                        <div class="x_content">
                            @if(Session::has('message2'))
                                <div class='alert alert-danger'>
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <p>{!! Session::get('message2') !!}</p>
                                </div>
                            @endif
                        </div>
                        <form class="form-horizontal" role="form" method="POST"
                              action="{{route('perfil-update-A')}}"
                              enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="col-sm-4 col-sm-offset-1">
                                <div class="form-group {{ $errors->has('Nombres') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Nombres</label>
                                    <input type="text" class="form-control" name="Nombres" value="{{$usuario->Nombres}}" placeholder="Nombres">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Nombres'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Nombres') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Apellidos') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Apellidos</label>
                                    <input type="text" class="form-control" name="Apellidos" value="{{$usuario->Apellidos}}" placeholder="Apellidos">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Apellidos'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Apellidos') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('CedulaRUC') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Apellidos</label>
                                    <input type="text" class="form-control" name="CedulaRUC" value="{{$usuario->CedulaRUC}}" placeholder="Cedula / RUC">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('CedulaRUC'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('CedulaRUC') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('FechaNacimiento') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Fecha de Nacimiento</label>
                                    <input type="date" class="form-control" name="FechaNacimiento" value="{{$usuario->FechaNacimiento}}" placeholder="FechaNacimiento">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('FechaNacimiento'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('FechaNacimiento') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Edad') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Edad</label>
                                    <input type="number" class="form-control" name="Edad" value="{{$usuario->Edad}}" placeholder="Edad">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Edad'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Edad') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Genero') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Genero</label>
                                    <select class="form-control" name="Genero" id="Genero">
                                        @if($usuario->Genero == "M") <option value="M">Masculino</option>
                                        @elseif($usuario->Genero == "F") <option value="F">Femenino</option>
                                        @elseif($usuario->Genero == "I") <option value="I">Indeterminado</option>
                                        @else <option value="">Seleccionar Genero</option>  @endif
                                        <option value="M">Masculino</option>
                                        <option value="F">Femenino</option>
                                        <option value="I">Indeterminado</option>
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Genero'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Genero') }}</strong>
                                      </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('EstadoCivil') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Estado Civil</label>
                                    <select class="form-control" name="EstadoCivil" id="EstadoCivil">
                                        @if($usuario->EstadoCivil == "C") <option value="M">Casado</option>
                                        @elseif($usuario->EstadoCivil == "S") <option value="F">Soltero</option>
                                        @elseif($usuario->EstadoCivil == "V") <option value="I">Viudo</option>
                                        @elseif($usuario->EstadoCivil == "D") <option value="I">Divorciado</option>
                                        @else <option value="">Seleccionar Estado Civil</option>  @endif
                                        <option value="C">Casado</option>
                                        <option value="S">Soltero</option>
                                        <option value="V">Viudo</option>
                                        <option value="D">Divorciado</option>
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('EstadoCivil'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('EstadoCivil') }}</strong>
                                      </span>
                                    @endif
                                </div>


                                <div class="form-group {{ $errors->has('TelfCasa') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Telefono Casa</label>
                                    <input type="number" class="form-control" name="TelfCasa" value="{{$usuario->TelfCasa}}" placeholder="Telefono Casa">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('TelfCasa'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('TelfCasa') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('TelfCelular') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Telefono Celular</label>
                                    <input type="number" class="form-control" name="TelfCelular" value="{{$usuario->TelfCelular}}" placeholder="Telefono Celular">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('telefono'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('TelfCelular') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Conyuge') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Conyuge</label>
                                    <input type="text" class="form-control" name="Conyuge" value="{{$usuario->Conyuge}}" placeholder="Conyuge">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Conyuge'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Conyuge') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Direccion') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Direccion</label>
                                    <input type="text" class="form-control" name="Direccion" value="{{$usuario->Direccion}}" placeholder="Direccion">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Direccion'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Direccion') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('ProfesionOficio') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Profesion/Oficio</label>
                                    <input type="text" class="form-control" name="ProfesionOficio" value="{{$usuario->ProfesionOficio}}" placeholder="Direccion">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('ProfesionOficio'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('ProfesionOficio') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('NumCargasFamiliares') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Numero de Cargas Familiares</label>
                                    <input type="number" class="form-control" name="NumCargasFamiliares" value="{{$usuario->NumCargasFamiliares}}" placeholder="Direccion">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('NumCargasFamiliares'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('NumCargasFamiliares') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('NumHijos') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Numero de HIjos</label>
                                    <input type="number" class="form-control" name="NumHijos" value="{{$usuario->NumHijos}}" placeholder="Direccion">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('NumHijos'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('NumHijos') }}</strong>
                                      </span>
                                    @endif
                                </div>

                            </div>
                            <div class="col-sm-4 col-sm-offset-1">
                                <div class="form-group {{ $errors->has('Comentarios') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Comentarios</label>
                                    <input type="text" class="form-control" name="Comentarios" value="{{$usuario->Comentarios}}" placeholder="Direccion">
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Comentarios'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Comentarios') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('IDEmpresa') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Empresa</label>
                                    <select class="form-control" name="IDEmpresa" id="IDEmpresa" readonly>
                                        <option value="{{$usuario->IDEmpresa}}">{{$usuario->empresas->nombre}}</option>
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('IDEmpresa'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('IDEmpresa') }}</strong>
                                      </span>
                                    @endif
                                </div>
                                <!--<div class="form-group {{ $errors->has('IDHorario') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Horario</label>
                                    <select class="form-control" name="IDHorario" id="IDHorario" readonly>
                                        <option value="{{$usuario->IDHorario}}">{{$usuario->IDHorario}}</option>
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('IDHorario'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('IDHorario') }}</strong>
                                      </span>
                                    @endif
                                </div>-->

                                <div class="form-group {{ $errors->has('FechaContratacion') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Fecha de Contratación</label>
                                    <input type="date" class="form-control" name="FechaContratacion" value="{{$usuario->FechaContratacion}}" placeholder="FechaContratacion" readonly>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('FechaContratacion'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('FechaContratacion') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('Cargos_IDCargo') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Cargo</label>
                                    <select class="form-control" name="Cargos_IDCargo" id="Cargos_IDCargo" readonly>
                                        <option value="{{$usuario->Cargos_IDCargo}}">{{$usuario->cargos->NombreCargo}}</option>
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Cargos_IDCargo'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Cargos_IDCargo') }}</strong>
                                      </span>
                                    @endif
                                </div>


                                <div class="form-group {{ $errors->has('Salarios_IDSalario') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Salario</label>
                                    <select class="form-control" name="Salarios_IDSalario" id="Salarios_IDSalario" readonly>
                                        <option value="{{$usuario->Salarios_IDSalario}}">{{$usuario->salarios->NombreSalario}}</option>
                                    </select>
                                    <span class="fa fa-archive form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('Salarios_IDSalario'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('Salarios_IDSalario') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Email</label>
                                    <input type="email" class="form-control" name="email" value="{{$usuario->email}}" placeholder="Email Personal">
                                    <span class="fa fa-at form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('EmailEmpresarial') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Email</label>
                                    <input type="email" class="form-control" name="EmailEmpresarial" value="{{$usuario->EmailEmpresarial}}" placeholder="Email Empresarial">
                                    <span class="fa fa-at form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('EmailEmpresarial'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('EmailEmpresarial') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Password</label>
                                    <input type="password" class="form-control" name="password" value=""
                                           placeholder="No usar si no es requerido" >
                                    <span class="fa fa-key form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('confpassword') ? ' has-error' : '' }} has-feedback">
                                    <label for="">Confirmar Password</label>
                                    <input type="password" class="form-control" name="confpassword" value=""
                                           placeholder="No usar si no es requerido" >
                                    <span class="fa fa-key form-control-feedback right" aria-hidden="true"></span>
                                    @if ($errors->has('confpassword'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('confpassword') }}</strong>
                                      </span>
                                    @endif
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-warning cerrarpermiso"
                                            data-dismiss="modal">Guardar cambios
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="{!! asset('admin-lte/plugins/select2/select2.min.js') !!}"></script>
@endsection