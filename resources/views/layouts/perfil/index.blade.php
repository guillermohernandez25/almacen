@extends('layouts.appmod')



@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box padding_box1">
                <div class="row">
                    <div class="col-md-10">
                        <div class="x_title">
                            <h3><i class="fa fa-user"></i>Mi Perfil</h3>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <br>
                        <a href="{{ route('perfil-edit-A',$usuario->IDusuario) }}" type="submit" class="btn btn-success" style="margin-bottom: 10px; position: relative; z-index: 1;">
                            <i class="fa fa-btn fa-user"></i> Editar Perfil
                        </a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-3">
                        <div style="align-items: center; align-content: center; text-align: center;">
                            <label for="">Nombres</label>
                            <h4 style="color: #656465; text-transform: uppercase;"><strong>{{$usuario->Nombres}}</strong></h4>
                        </div>
                        <div style="align-items: center; align-content: center; text-align: center;">
                            <label for="">Apellidos</label>
                            <h4 style="color: #656465; text-transform: uppercase;"><strong>{{$usuario->Apellidos}}</strong></h4>
                        </div><div style="align-items: center; align-content: center; text-align: center;">
                            <label for="">Cedula/RUC</label>
                            <h4 style="color: #656465; text-transform: uppercase;"><strong>{{$usuario->CedulaRUC}}</strong></h4>
                        </div>
                        <div style="align-items: center; align-content: center; text-align: center;">
                            <label for="">Email Personal</label>
                            <h4 style="color: #656465; text-transform: uppercase;"><strong>{{$usuario->email}}</strong></h4>
                        </div>
                        <div style="align-items: center; align-content: center; text-align: center;">
                            <label for="">Email Empresarial</label>
                            <h4 style="color: #656465; text-transform: uppercase;"><strong>{{$usuario->EmailEmpresarial}}</strong></h4>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div style="align-items: center; align-content: center; text-align: center;">
                            <h5>Click en la imagen para cambiar</h5>
                                <label for="foto_cargada">
                                <img style="
                       border:4px solid #cb9c14;
                       width: 200px; height: 200px; border-radius: 100%;"  src="{{asset("uploads/usuarios/".$usuario->Foto)}}" alt="">
                                <form class="form-horizontal" role="form" method="POST" id="foto"  action="{{route('perfil-update-image-A',Auth::User()->IDusuario)}}"  enctype="multipart/form-data">
                                    {!! csrf_field() !!}

                                    <input class="form-control" onchange="$('#enviar').click();" style="display: none;" type="file" name="foto_cargada" id="foto_cargada">
                                    <button style="display: none;" type="submit" id="enviar"> </button>
                                </form>
                                </label>
                            </div>
                    </div>
                    <div class="col-md-3">
                        <div style="align-items: center; align-content: center; text-align: center;">
                            <label for="">Telefono de Casa</label>
                            <h4 style="color: #656465; text-transform: uppercase;"><strong>{{$usuario->TelfCasa}}</strong></h4>
                        </div>
                        <div style="align-items: center; align-content: center; text-align: center;">
                            <label for="">Telefono Celular</label>
                            <h4 style="color: #656465; text-transform: uppercase;"><strong>{{$usuario->TelfCelular}}</strong></h4>
                        </div>
                        <div style="align-items: center; align-content: center; text-align: center;">
                            <label for="">Dirección</label>
                            <h4 style="color: #656465; text-transform: uppercase;"><strong>{{$usuario->Direccion}}</strong></h4>
                        </div><div style="align-items: center; align-content: center; text-align: center;">
                            <label for="">Fecha de Contratacion</label>
                            <h4 style="color: #656465; text-transform: uppercase;"><strong>{{$usuario->FechaContratacion}}</strong></h4>
                        </div>
                        <div style="align-items: center; align-content: center; text-align: center;">
                            <label for="">Estado Civil</label>
                            <h4 style="color: #656465; text-transform: uppercase;"><strong>{{$usuario->EstadoCivil}}</strong></h4>
                        </div>
                    </div>

                    </div>
                </div>
                <hr>
                <div class="row">
                </div>
            </div>
        </div>
    </div>

@endsection

