@extends('layouts.appmod')


@section('script')
    <script type="text/javascript">

        $(document).ready(function () {
            // Bar chart
            var total = $('#cantidad').val();
            //alert(total);
            var i = 1;

                        //console.log("hola"+informacion);
            new Chart(document.getElementsByName('barchart'), {

                type: 'bar',
                data: {
                    labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                    datasets: [
                        {
                            label: "Niveles de Ventas",
                            backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","#3e95cd", "#8e5ea2",],
                            data: <?php echo(json_encode($mensual_ventas_almacen)); ?>
                        }

                    ]
                },
                options: {
                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'Niveles de Ventas'
                    }
                }

            });

        })
    </script>
    <script>
        $(function () {
            $('.ultimas').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 5,
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
            $('.stock').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 5,
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
            $('.compras').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 5,
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
    <script>


    </script>
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Panel Principal
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-Panel Principal"></i> Home</a></li>
            <li class="active">Panel Principal</li>

                <li class="active">Seleccionar Almacen</li>
            <select class="form-control" name="list_almacen" id="list_almacen" onchange="location.href ='/home/'+$(this).val();">
                <option style="color: #849aff;" value="{{$almacenes->IDalmacen}}">ESTAS AQUI: {{$almacenes->nombre}}</option>
                @foreach($list_almacenes as $list)
                <option value="{{$list->IDalmacen}}">{{$list->nombre}}</option>
                    @endforeach
            </select>

        </ol>





                <div style="align-items: center; align-content: center; text-align: center;">
                    <h4>
                        <strong>
                            Nombre de Almacen: {{$almacenes->nombre}}, Descripción: {{$almacenes->descripcion}}.
                        </strong>
                    </h4>
                </div>
                <div class="row">

                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{$count_factura_proveedor}}</h3>

                                <p>Compras</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            @if($count_factura_proveedor >= 1)
                            <a href="{{route('compras')}}" class="small-box-footer">Ver Compras <i class="fa fa-arrow-circle-right"></i></a>
                                @endif
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>{{$articulos_almacen}}<sup style="font-size: 20px"></sup></h3>

                                <p>Productos</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            @if(isset($almacenes->IDAlmacen))
                                @if($articulos_almacen >= 1)
                            <a href="{{route('almacenes-almacen-A',$almacenes->IDAlmacen)}}" class="small-box-footer">Ir al Almacen <i class="fa fa-arrow-circle-right"></i></a>
                                @endif
                            @endif
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>{{$clientes}}</h3>
                                <p>Clientes</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            @if($clientes >= 1)
                            <a href="{{route('clientes-almacen',$almacenes->IDalmacen)}}" class="small-box-footer">Ver Clientes <i class="fa fa-arrow-circle-right"></i></a>
                            @endif
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3>{{$total_ventas_almacen}}</h3>

                                <p>Ventas</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            @if($total_ventas_almacen >= 1)
                            <a href="{{route('almacenes-ventas-A',$almacenes->IDalmacen)}}" class="small-box-footer">Ver Ventas <i class="fa fa-arrow-circle-right"></i></a>
                                @endif
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <div class="row">
@if(Auth::User()->rol_id == "4") @else
                    <div class="col-md-5">
                        <h4 style="color:#ffffff;  background-color:#309961; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0; border-radius: 7px;">
                            Ventas mensuales
                        </h4>
                        <!-- Bar chart -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <i class="fa fa-bar-chart-o"></i>

                                <h3 class="box-title">Grafico de Ventas Mensuales</h3>

                                <div class="box-tools pull-right">
                                    <!--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                                </div>
                            </div>
                            <div class="box-body">
                                <canvas id="" name="barchart" class="" width="800" height="450" style="height: 300px; padding: 0px; position: relative;">


                                </canvas>
                            </div>
                            <!-- /.box-body-->
                        </div>
                        <!-- /.box -->

                    </div>
                    @endif
                    <div class="col-md-7">
                        <h4 style="color:#ffffff; background-color:#309961; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0; border-radius: 7px;">
                            Últimas Ventas Realizadas
                        </h4>
                        <div class="table-responsive">
                            <table id="" class="table table-bordered ultimas" role="grid" aria-describedby="categoria_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Factura</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Fecha de Venta</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Total</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Cliente</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($ventas_almacen))
                                    @foreach($ventas_almacen as $ventas)
                                <tr role="row" class="odd">

                                    <!--<td class="sorting_1"></td>-->

                                    <td>{{$ventas->IDfacturacion}}</td>
                                    <td>{{$ventas->Fecha}}</td>
                                    <td>{{$ventas->Valor_Total}}</td>

                                    <td>@if(isset($ventas->clientes->Nombres)) {{$ventas->clientes->Nombres}} {{$ventas->clientes->Apellidos}} @else S/R @endif</td>

                                </tr>
                                    @endforeach
                                @endif
                                </tbody>

                                <tfoot>
                                <tr><th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Factura</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Fecha de Venta</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Total</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Cliente</th></tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <div class="row">


                    <div class="col-md-5">
                        <h4 style="color:#ffffff;  background-color:#0b7399; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0; border-radius: 7px;">
                            Productos Sin Stock y con Inventario Bajo
                        </h4>
                        <div class="table-responsive">
                            <table id="" class="table table-bordered stock" role="grid" aria-describedby="categoria_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Codigo</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Producto</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Cantidad</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Estado</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if(isset($producto_stock))
                                    @foreach($producto_stock as $producto)
                                        <tr role="row" class="odd">
                                            <!--<td class="sorting_1"></td>-->
                                            <td>{{$producto->Cod}}</td>
                                            <td>{{$producto->Articulo}}</td>
                                            <td>{{$producto->Cantidad}}</td>
                                            <td>Baja Disponibilidad</td>
                                            <td><a class="btn btn-warning btn-xs" href="{{ route('producto-edit-A', $producto->IDarticulo) }}" data-toggle="tooltip" data-placement="left" title="Reponer Cantidad">
                                                    <i class="fa fa-pencil fa-lg"></i>
                                                </a></td>


                                        </tr>
                                    @endforeach
                                @endif

                                </tbody>

                                <tfoot>
                                <tr>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Codigo</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Producto</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Cantidad</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Estado</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Acciones</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                        <!-- /.box -->

                    </div>
                    @if(Auth::User()->rol_id == "4") @else
                    <div class="col-md-7">
                        <h4 style="color:#ffffff; background-color:#db4020; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0; border-radius: 7px;">
                            Últimas Compras Realizadas
                        </h4>
                        <div class="table-responsive">
                            <table id="" class="table table-bordered compras" role="grid" aria-describedby="categoria_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Nro Doc</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Fecha de Compra</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Nro Factura</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Fecha de Emision</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Total</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Proveedor</th>
                                </tr>
                                </thead>
                                <tbody>



                                @if(isset($factura_proveedor))
                                    @foreach($factura_proveedor as $factura)
                                        <tr role="row" class="odd">
                                            <!--<td class="sorting_1"></td>-->
                                            <td>{{$factura->IDfacturaproveedor}}</td>
                                            <td>{{$factura->FechaEmision}}</td>
                                            <td>{{$factura->NumFactura}}</td>
                                            <td>{{$factura->FechaEmision}}</td>
                                            <td>{{$factura->Total}}</td>
                                            <td>@if(isset($factura->proveedores->Proveedor)){{$factura->proveedores->Proveedor}} @else S/R @endif</td>
                                        </tr>
                                        @endforeach
                                        @endif



                                </tbody>

                                <tfoot>
                                <tr>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Nro Doc</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">FEcha de Compra</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Nro Factura</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Fecha de Emision</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Total</th>
                                    <th class="sorting_asc" tabindex="0" aria-controls="productos" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 174px;">Proveedor</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    @endif
                    <!-- /.box -->
                </div>
            </div>
           
        
    </section>
@endsection


